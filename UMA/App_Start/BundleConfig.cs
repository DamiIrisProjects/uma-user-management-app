﻿using System.Web;
using System.Web.Optimization;

namespace UMA
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/master").Include(
                        "~/Scripts/master.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/home").Include(
                         "~/Scripts/jquery-{version}.js",
                         "~/Scripts/hoverintent.js", 
                         "~/Scripts/jquery.lightbox_me.js"));

            bundles.Add(new ScriptBundle("~/bundles/login").Include(
                        "~/Scripts/login.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                         "~/Scripts/jquery-{version}.js",
                         "~/Scripts/hoverintent.js",
                         "~/Scripts/select2.js", 
                         "~/Scripts/app.js",
                         "~/Scripts/ApplicationManagement.js",
                         "~/Scripts/RoleAndPrivilegeManagement.js",
                         "~/Scripts/ActionManagement.js",
                         "~/Scripts/jquery.lightbox_me.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/general.css"));

            bundles.Add(new StyleBundle("~/Content/home").Include(
                      "~/Content/home.css"));

            bundles.Add(new StyleBundle("~/Content/app").Include(
                     "~/Content/home.css", 
                     "~/Content/select2.css"));

            // Only while in development
            BundleTable.EnableOptimizations = false;
        }
    }
}
