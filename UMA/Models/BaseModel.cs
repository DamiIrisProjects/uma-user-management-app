﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using UMAentities;

namespace UMA.Models
{
    public class BaseModel
    {
        public bool IsFirstTime { get; set; }

        public bool JustRegistered { get; set; }

        public int OperatorType { get; set; }

        public string BranchName { get; set; }

        public string SelectedOperator { get; set; }

        public string SelectedEditPriviApp { get; set; }

        public string SelectedPrivileges { get; set; }

        public string SelectedApplication { get; set; }

        public string SelectedRole { get; set; }

        public string SelectedRoleBranch { get; set; }

        public string  SelectedSharedApp { get; set; }

        public string SelectedSharedUsers { get; set; }

        public string SelectedSharedAppSave { get; set; }

        public string SelectedSharedAppRemove { get; set; }

        public string SelectedAdminBranch { get; set; }

        public string SelectedAdminBranchSu { get; set; }

        public string SelectedRoles { get; set; }

        public string SelectedAdminApp { get; set; }

        public string RemoveShareAppId { get; set; }

        public string CreateAdminAppId { get; set; }

        public string CreateAdminBranchId { get; set; }

        public string SelectedEditPriviGrpApp { get; set; }

        public List<RoleType> RoleTypes { get; set; }
                
        public List<Operator> AllowedStaff { get; set; }

        public List<Action> Actions { get; set; }

        public List<Operator> CreatedAdmins { get; set; }

        public List<Operator> UserRequests { get; set; }

        public List<Operator> SuperUsers { get; set; }
        
        public List<Application> Applications { get; set; }

        public List<Privilege> Privileges { get; set; }

        public List<PrivilegeGroup> PrivilegeGrps { get; set; }

        public List<Role> Roles { get; set; }

        [Required(ErrorMessage="Please enter application name")]
        [StringLength(100, ErrorMessage = "Max length is 100 characters")]
        public string NewAppName { get; set; }

        [StringLength(150, ErrorMessage = "Max length is 150 characters")]
        public string Icon { get; set; }

        [Required(ErrorMessage = "Please enter Role name")]
        [StringLength(100, ErrorMessage = "Max length is 100 characters")]
        public string NewRoleName { get; set; }

        [Required(ErrorMessage = "Please enter privilege name")]
        [StringLength(100, ErrorMessage = "Max length is 150 characters")]
        public string NewPriviName { get; set; }

        [Required(ErrorMessage = "Please enter action definition")]
        [StringLength(250, ErrorMessage = "Max length is 150 characters")]
        public string NewActionName { get; set; }


        [Required(ErrorMessage = "Please enter controller definition")]
        [StringLength(250, ErrorMessage = "Max length is 250 characters")]
        public string NewActionControllerName { get; set; }

        [Required(ErrorMessage = "Please enter privilege group name")]
        [StringLength(100, ErrorMessage = "Max length is 100 characters")]
        public string NewPriviGroupName { get; set; }

        [Required(ErrorMessage = "Please enter a username")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        [StringLength(50, ErrorMessage = "Max length is 50 characters")]
        public string NewAdminUsername { get; set; }

        [Required(ErrorMessage = "Please enter a password")]
        [DataType(DataType.Password)]
        [StringLength(15, ErrorMessage = "Max length is 15 characters")]
        public string NewAdminPassword { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please confirm your password")]
        [Display(Name = "Confirm new password")]
        [Compare("NewAdminPassword", ErrorMessage = "The password and confirmation password do not match.")]
        public string NewAdminPasswordConfirm { get; set; }
    }
}