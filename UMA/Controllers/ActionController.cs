﻿using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Web.Mvc;
using UMA.Models;
using UMA_Proxy;
using UMAentities;
using Helper = UMA.Helpers.Helper;


namespace UMA.Controllers
{
    public class ActionController : Controller
    {
        public ActionResult CreateAction(BaseModel model, string selectedActionsApp)
        {
            if (!string.IsNullOrEmpty(model.NewActionName) && !string.IsNullOrEmpty(model.NewActionControllerName))
            {
                int result = new InternalActionProxy().InternalActionChannel.AddAction(new Action() { Name = model.NewActionName.ToFirstLetterUpper(), Controller = model.NewActionControllerName.ToFirstLetterUpper(), ApplicationId = int.Parse(selectedActionsApp), OwnerId = int.Parse(User.Identity.GetUserId()) });

                if (result == 2)
                {
                    string error = "a controller/action with this name already exists";
                    return Json(new { err = error }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    // Get IActions to update screen
                    List<Action> iactions = new InternalActionProxy().InternalActionChannel.GetApplicationActions(int.Parse(selectedActionsApp));
                    model.OperatorType = int.Parse(GenericPrincipalExtensions.GetClaim(User, "OperatorType"));
                    model.Actions = iactions;
                    return Json(new { status = result, page = Helper.JsonPartialView(this, "ActionManagement/_EditAction", model) }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { status = 0, err = "You must select an application and then enter a controller/action name" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateAction(string id, string name, string controller, string applicationId)
        {
            if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(controller) && !string.IsNullOrEmpty(applicationId))
            {
                var action = new Action
                {
                    EditBy = int.Parse(User.Identity.GetUserId()),
                    Controller = controller,
                    Name = name,
                    ApplicationId = int.Parse(applicationId),
                    Id = int.Parse(id)
                };

                int result = new InternalActionProxy().InternalActionChannel.UpdateAction(action);

                if (result == 3)
                    return Json(new { status = 0, err = "A controller/action with this name already exists in this application" }, JsonRequestBehavior.AllowGet);

                return Json(new { status = result }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { status = 0, err = "You must enter an action name" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteAction(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var action = new Action
                {
                    Id = int.Parse(id),
                    EditBy = int.Parse(User.Identity.GetUserId())
                };

                int result = new InternalActionProxy().InternalActionChannel.DeleteAction(action);
                return Json(new { status = result }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { status = 0, err = "Error deleting action" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPrivilegeActions(string privid, string appid)
        {
            if (!string.IsNullOrEmpty(privid))
            {
                var result = new InternalActionProxy().InternalActionChannel.GetPrivilegeActions(new Action
                {
                    PrivilegeId = int.Parse(privid),
                    ApplicationId = int.Parse(appid)
                });

                result.ApplicationId = int.Parse(appid);

                return Json(new
                {
                    status = 1,
                    page = Helper.JsonPartialView(this, "ActionManagement/_AssignAction", result)
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0, err = "No privilege selected" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetApplicationActions(string appid)
        {
            if (!string.IsNullOrEmpty(appid))
            {
                var result = new InternalActionProxy().InternalActionChannel.GetApplicationActions(int.Parse(appid));
                var model = new BaseModel
                {
                    Actions = result,
                    OperatorType = int.Parse(GenericPrincipalExtensions.GetClaim(User, "OperatorType"))
                };

                if (result == null)
                {
                    string error = "No actions found";
                    return Json(new { err = error }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                    {
                        status = 1,
                        page = Helper.JsonPartialView(this, "ActionManagement/_EditAction", model)
                    }, 
                    JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0, err = "No privilege selected" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AssignActionsToPrivilege(Action iaction)
        {
            if (!string.IsNullOrEmpty(iaction.AddActions) && iaction.Id != 0)
            {
                // disect string
                var actions = new List<Action>();
                foreach (string action in iaction.AddActions.Split(','))
                {
                    var a = new Action
                    {
                        Id = int.Parse(action),
                        EditBy = int.Parse(User.Identity.GetUserId()),
                        PrivilegeId = iaction.Id
                    };

                    actions.Add(a);
                }

                var result = new InternalActionProxy().InternalActionChannel.AddActionsToPrivilege(actions);

                // Get updated iaction details
                iaction.PrivilegeId = iaction.Id;
                var updatediaction = new InternalActionProxy().InternalActionChannel.GetPrivilegeActions(iaction);
                return Json(new
                {
                    status = result,
                    page = Helper.JsonPartialView(this, "ActionManagement/_AssignAction", updatediaction)
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0, err = "Select a privilege and one or more actions" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveActionsFromPrivilege(Action iaction)
        {
            if (!string.IsNullOrEmpty(iaction.RemoveActions) && iaction.Id != 0)
            {
                // disect string
                var actions = new List<Action>();
                foreach (string action in iaction.RemoveActions.Split(','))
                {
                    var a = new Action
                    {
                        Id = int.Parse(action),
                        EditBy = int.Parse(User.Identity.GetUserId()),
                        PrivilegeId = iaction.Id
                    };

                    actions.Add(a);
                }

                var result = new InternalActionProxy().InternalActionChannel.RemoveActionsFromPrivilege(actions);

                // Get updated iaction details
                iaction.PrivilegeId = iaction.Id;
                var updatediaction = new InternalActionProxy().InternalActionChannel.GetPrivilegeActions(iaction);
                return Json(new
                {
                    status = result,
                    page = Helper.JsonPartialView(this, "ActionManagement/_AssignAction", updatediaction)
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0, err = "Select a privilege and one or more actions" }, JsonRequestBehavior.AllowGet);
        }
    }
}