﻿using AspNet.Identity.Oracle;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UMA.Models;
using UMA_Proxy;
using UMAentities;
using Helper = UMA.Helpers.Helper;

namespace UMA.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        #region Variables

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager => _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

        private readonly ApplicationRoleManager _roleManager = null;
        public ApplicationRoleManager RoleManager => _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();

        private SignInHelper _helper;

        private SignInHelper SignInHelper => _helper ?? ( _helper = new SignInHelper(UserManager, AuthenticationManager));

        #endregion

        [AllowAnonymous]
        public ActionResult Login(string returnUrl, string plslogin)
        {
            if (Request.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            if (plslogin == "1")
                ViewBag.JustRegistered = true;

            if (plslogin == "2")
                ViewBag.JustUpdated = true;

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (Request.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doen't count login failures towards lockout only two factor authentication
            // To enable password failures to trigger lockout, change to shouldLockout: true
            var result = await SignInHelper.PasswordSignIn(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    {
                        var user = await UserManager.FindByNameAsync(model.Email);

                        // Check if profile has been filled. If not redirect to fill details
                        if (string.IsNullOrEmpty(user.FirstName) || string.IsNullOrEmpty(user.Surname))
                            return RedirectToAction("Register", new { setprofile = 1 });

                        return RedirectToLocal(returnUrl);
                    }
                case SignInStatus.LockedOut:
                    ModelState.AddModelError("ServerResponse", "Account is locked....for some reason.");
                    return View(model);
                default:
                    ModelState.AddModelError("ServerResponse", "Invalid username or password");
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        [AllowAnonymous]
        public ActionResult Register(string setprofile)
        {
            RegisterViewModel model = new RegisterViewModel();

            if (!string.IsNullOrEmpty(setprofile))
            {
                if (User.Identity.IsAuthenticated)
                {
                    model.Email = GenericPrincipalExtensions.GetClaim(User, "Email").ToLower();
                    model.ProfileUpdate = true;
                    model.OperatorId = int.Parse(User.Identity.GetUserId());
                }
                else
                    return RedirectToAction("Login");
            }

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Add one more check to make sure the person is an Iris/Exaro/Afisteam staff
                if (!model.Email.ToLower().EndsWith("irissmart.com") && !model.Email.ToLower().EndsWith("exarotech.com") && !model.Email.ToLower().EndsWith("afisteam.com"))
                {
                    ModelState.AddModelError("ServerResponse", "Only Iris, Exaro & Code5 email addresses allowed");
                }
                else
                {
                    var user = new ApplicationUser { UserName = model.Email, Email = model.Email, FirstName = model.Firstname, Surname = model.Surname };
                    var result = await UserManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        // Ensure that a default role has been created. Otherwise, create one
                        CreateDefaultRoles();

                        // Create & Assign developer role
                        var role = RoleManager.FindById("1");
                        var rolesForUser = UserManager.GetRoles(user.Id);
                        if (!rolesForUser.Contains(role.Name))
                        {
                            UserManager.AddToRole(user.Id, role.Name);
                        }
                        
                        // Send email and stuff - Deactivated for now
                        var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code }, Request.Url?.Scheme);
                        await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking this link: <a href=\"" + callbackUrl + "\">link</a>");
                        ViewBag.Link = callbackUrl;
                        return View("DisplayEmail");
                    }

                    AddErrors(result);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private void CreateDefaultRoles()
        {
            string sysrole = "System Admin";
            string oprrole = "Operator";
            string branchadminrole = "Branch Admin";

            var sysroleResult = RoleManager.FindByName(sysrole);
            if (sysroleResult == null)
            {
                sysroleResult = new IdentityRole(sysrole);
                RoleManager.Create(sysroleResult);
            }

            var branchadminroleResult = RoleManager.FindByName(branchadminrole);
            if (branchadminroleResult == null)
            {
                branchadminroleResult = new IdentityRole(branchadminrole);
                RoleManager.Create(branchadminroleResult);
            }

            var oprroleResult = RoleManager.FindByName(oprrole);
            if (oprroleResult == null)
            {
                oprroleResult = new IdentityRole(oprrole);
                RoleManager.Create(oprroleResult);
            }               
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                Operator opr = new Operator
                {
                    FirstName = model.Firstname,
                    Surname = model.Surname,
                    EmailAddress = model.Email,
                    Password = model.Password,
                    OperatorId = model.OperatorId
                };

                var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var code = await userManager.GeneratePasswordResetTokenAsync(model.OperatorId.ToString());
                var result = await userManager.ResetPasswordAsync(model.OperatorId.ToString(), code, model.Password);
                if (result.Succeeded)
                {
                    int res = new OperatorProxy().OperatorChannel.UpdateOperator(opr);

                    // Log out so he can login with fresh details
                    AuthenticationManager.SignOut();

                    if (res == 1)
                    {
                        // All ok
                        return RedirectToAction("Login", new { plslogin = 2 });
                    }
                    else
                    {
                        ModelState.AddModelError("ServerResponse", "An error occurred updating your account. Please try again later.");
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return RedirectToAction("Login");
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            if (result.Succeeded)
            {
                return View("ConfirmEmail");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    return Json(new { status = 2 }, JsonRequestBehavior.AllowGet);
                }

                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code }, Request.Url?.Scheme);

                try
                {
                    // Create mail
                    string buttonstyle = "background-color: #336699; font-family: arial; text-align:center; width:180px; height:30px; color: White; margin: 15px 0px 20px 0px; border: none; cursor: pointer; line-height: 30px; font-size: 15px; border-radius: 2px 2px 2px 2px; -moz-border-radius: 2px 2px 2px 2px; -webkit-border-radius: 2px 2px 2px 2px; -khtml-border-radius:  2px 2px 2px 2px;";
                    string button = "<a style='text-decoration: none' href=\"" + callbackUrl + "\"><div Style='" + buttonstyle + "'>Reset My Password</div></a>";

                    StringBuilder sb = new StringBuilder();
                    sb.Append("<div style='color:#333'>Hello, </div><br/>");
                    sb.Append("<div style='color:#333'>Following your request on the U.M.A. portal, here is a link from which you can change your password:</div><br/>");
                    sb.Append(button);
                    sb.Append("<div style='color:#333; margin: 0px 0px 15px 0px'><b>Note:</b> You must be on the Iris network for this to work.</div>");
                    sb.Append("<div style='color:#333; margin: 0px 0px 10px 0px'>Sincerly,</div><br/>");
                    sb.Append("<div style='color:#333'>The UMA Team</div>");

                    await UserManager.SendEmailAsync(user.Id, "Reset Password", sb.ToString());
                    return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { status = 2 }, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code, string userId)
        {
            var user = UserManager.FindById(userId);

            if (user == null)
                return RedirectToAction("Index", "Home");

            var model = new ResetPasswordViewModel
            {
                Email = user.Email
            };

            return code == null ? View("Error") : View(model);
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Error");
            }

            var user = await UserManager.FindByNameAsync(model.Email);

            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }

            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }

            AddErrors(result);
            return View(model);
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("ServerResponse", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
        
        public ActionResult DeactivateAccount(string iscancel, string removeUserOprId, string usertype)
        {
            try
            {
                if (!string.IsNullOrEmpty(removeUserOprId))
                {
                    int result = new OperatorProxy().OperatorChannel.DeactivateUser(new Operator
                    {
                        OperatorId = int.Parse(removeUserOprId),
                        OwnerId = int.Parse(User.Identity.GetUserId()),
                        IsCancel = iscancel == "1" ? 2 : 0
                    });

                    var model = new BaseModel
                    {
                        OperatorType = int.Parse(GenericPrincipalExtensions.GetClaim(User, "OperatorType")),
                        CreatedAdmins = new ApplicationProxy().ApplicationChannel.GetBranchUsers(new Application()
                        {
                            BranchId = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Branch")),
                            OwnerId = int.Parse(User.Identity.GetUserId())
                        })
                    };
                    return Json(new { status = result, page = Helper.JsonPartialView(this, "ApplicationManagement/_ApplicationAdmins", model) }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { status = 0, err = "You must select an operator" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeactivateSuperUser(string iscancel, string removeUserOprId)
        {
            try
            {
                if (!string.IsNullOrEmpty(removeUserOprId))
                {
                    int result = new OperatorProxy().OperatorChannel.DeactivateUser(new Operator
                    {
                        OperatorId = int.Parse(removeUserOprId),
                        OwnerId = int.Parse(User.Identity.GetUserId()),
                        IsCancel = iscancel == "1" ? 2 : 0
                    });

                    var model = new BaseModel
                    {
                        OperatorType = int.Parse(GenericPrincipalExtensions.GetClaim(User, "OperatorType")),
                        SuperUsers = new ApplicationProxy().ApplicationChannel.GetSuperUsers()
                    };

                    return Json(new
                    {
                        status = result,
                        page = Helper.JsonPartialView(this, "ApplicationManagement/ApplicationSuperusers", model)
                    }, 
                    JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = 0, err = "You must select an operator" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        //#region Helpers
        //// Used for XSRF protection when adding external logins
        //private const string XsrfKey = "XsrfId";

        //private void AddErrors(IdentityResult result)
        //{
        //    foreach (var error in result.Errors)
        //    {
        //        ModelState.AddModelError("ServerResponse", error);
        //    }
        //}

        //private ActionResult RedirectToLocal(string returnUrl)
        //{
        //    if (Url.IsLocalUrl(returnUrl))
        //    {
        //        return Redirect(returnUrl);
        //    }
        //    else
        //    {
        //        return RedirectToAction("Index", "Home");
        //    }
        //}

        //private class ChallengeResult : HttpUnauthorizedResult
        //{
        //    public ChallengeResult(string provider, string redirectUri) : this(provider, redirectUri, null)
        //    {
        //    }

        //    public ChallengeResult(string provider, string redirectUri, string userId)
        //    {
        //        LoginProvider = provider;
        //        RedirectUri = redirectUri;
        //        UserId = userId;
        //    }

        //    public string LoginProvider { get; set; }
        //    public string RedirectUri { get; set; }
        //    public string UserId { get; set; }

        //    public override void ExecuteResult(ControllerContext context)
        //    {
        //        var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
        //        if (UserId != null)
        //        {
        //            properties.Dictionary[XsrfKey] = UserId;
        //        }
        //        context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
        //    }
        //}

        //#endregion
    }
}