﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UMA.Models;
using UMA_Proxy;
using UMAentities;
using Helper = UMA.Helpers.Helper;


namespace UMA.Controllers
{
    [AuthorizeCustomAttribute]
    public class PrivilegeController : Controller
    {
        public ActionResult GetApplicationPrivisList(string appid)
        {
            try
            {
                List<Privilege> result = new PrivilegeProxy().PrivilegeChannel.GetApplicationPrivileges(int.Parse(appid));
                result = result.OrderBy(x => x.Name).ToList();
                var jsonList = new List<object>();

                foreach (Privilege app in result)
                {
                    jsonList.Add(new { text = app.Name, id = app.Id });
                }

                return Json(new { result = new JavaScriptSerializer().Serialize(jsonList) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetApplicationPrivis(string appid)
        {
            if (!string.IsNullOrEmpty(appid))
            {
                var result = new PrivilegeProxy().PrivilegeChannel.GetApplicationPrivileges(int.Parse(appid));
                var model = new BaseModel {Privileges = result};

                if (result == null)
                {
                    return Json(new { err = "No privileges found" }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = 1, page = Helper.JsonPartialView(this, "RolePriviManagement/_EditPrivilege", model) }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0, err = "No application selected" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetApplicationPriviGroups(string appid, string roletype)
        {
            if (!string.IsNullOrEmpty(appid))
            {
                var result = new RoleProxy().RoleChannel.GetApplicationRoles(new Application
                {
                    Id = int.Parse(appid),
                    RoleType = string.IsNullOrEmpty(roletype) ? 0 : int.Parse(roletype)
                });
                var model = new BaseModel {Roles = result};

                if (result == null)
                {
                    string error = "No privilege groups found";
                    return Json(new { err = error }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = 1, page = Helper.JsonPartialView(this, "RolePriviManagement/_EditPriviGroup", model) }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0, err = "No application selected" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreatePrivi(BaseModel model)
        {
            if (!string.IsNullOrEmpty(model.NewPriviName))
            {
                int result = new PrivilegeProxy().PrivilegeChannel.AddPrivilege(new Privilege
                {
                    Name = model.NewPriviName,
                    OwnerId = int.Parse(User.Identity.GetUserId()),
                    ApplicationId = int.Parse(model.SelectedEditPriviApp)
                });

                if (result == 2)
                {
                    string error = "a privilege with this name already exists";
                    return Json(new { err = error }, JsonRequestBehavior.AllowGet);
                }

                // Get Privileges to update screen
                var privis = new PrivilegeProxy().PrivilegeChannel.GetApplicationPrivileges(int.Parse(model.SelectedEditPriviApp));
                model.Privileges = privis;

                return Json(new
                {
                    status = result,
                    page = Helper.JsonPartialView(this, "RolePriviManagement/_EditPrivilege", model)
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0, err = "You must enter a privilege name" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdatePrivi(Privilege privi)
        {
            if (!string.IsNullOrEmpty(privi.Name))
            {
                privi.EditBy = int.Parse(User.Identity.GetUserId());

                int result = new PrivilegeProxy().PrivilegeChannel.UpdatePrivilege(privi);

                if (result == 3)
                    return Json(new { status = 0, err = "A privilege already exists with this name" }, JsonRequestBehavior.AllowGet);

                return Json(new { status = result }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { status = 0, err = "You must enter a privilege name" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeletePrivi(Privilege privi)
        {
            if (privi != null)
            {
                privi.EditBy = int.Parse(User.Identity.GetUserId());

                int result = new PrivilegeProxy().PrivilegeChannel.DeletePrivilege(privi);
                return Json(new { status = result }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { status = 0, err = "Error deleting privilege" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AssignGroupPrivileges(Role role)
        {
            if (!string.IsNullOrEmpty(role.AddPrivileges) && role.Id != 0)
            {
                role.Privileges = new List<Privilege>();
                role.EditBy = int.Parse(User.Identity.GetUserId());

                // disect string
                foreach (string privi in role.AddPrivileges.Split(','))
                {
                    Privilege p = new Privilege()
                    {
                        Id = int.Parse(privi)
                    };

                    role.Privileges.Add(p);
                }

                int result = new PrivilegeProxy().PrivilegeChannel.AddPrivilegesToGroup(role);

                // Get updated role details
                Role updatedgrp = new RoleProxy().RoleChannel.GetRoleDetails(role);
                return Json(new { status = result, page = Helper.JsonPartialView(this, "RolePriviManagement/_PriviGrpPartial", updatedgrp) }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { status = 0, err = "Select a role and select 1 or more privileges" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveGroupPrivileges(Role role)
        {
            if (!string.IsNullOrEmpty(role.RemovePrivileges) && role.Id != 0)
            {
                role.Privileges = new List<Privilege>();
                role.EditBy = int.Parse(User.Identity.GetUserId());

                // disect string
                foreach (string privi in role.RemovePrivileges.Split(','))
                {
                    Privilege p = new Privilege()
                    {
                        Id = int.Parse(privi),
                        GroupId = role.Id,
                        EditBy = int.Parse(User.Identity.GetUserId())
                    };

                    role.Privileges.Add(p);
                }

                int result = new PrivilegeProxy().PrivilegeChannel.RemovePrivilegesFromGroup(role);

                // Get updated role details
                Role updatedrole = new RoleProxy().RoleChannel.GetRoleDetails(role);
                return Json(new { status = result, page = Helper.JsonPartialView(this, "RolePriviManagement/_PriviGrpPartial", updatedrole) }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { status = 0, err = "Select a role and select 1 or more privileges" }, JsonRequestBehavior.AllowGet);
        }
    }
}