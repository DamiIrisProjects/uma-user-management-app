﻿using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UMA.Models;
using UMA_Proxy;
using UMAentities;
using Helper = UMA.Helpers.Helper;

namespace UMA.Controllers
{
    [AuthorizeCustomAttribute]
    public class RoleController : Controller
    {
        public ActionResult CreateRole(BaseModel model, string selectedRoleApp, string roletype, string isAppLinked)
        {
            if (!string.IsNullOrEmpty(model.NewRoleName) && (!string.IsNullOrEmpty(selectedRoleApp) || isAppLinked == "0"))
            {
                int result = new RoleProxy().RoleChannel.AddRole(new Role() { Name = model.NewRoleName, RoleType = new RoleType() { RoleTypeId = int.Parse(roletype) }, ApplicationId = string.IsNullOrEmpty(selectedRoleApp) ? 0 : int.Parse(selectedRoleApp), OwnerId = int.Parse(User.Identity.GetUserId()) });

                if (result == 2)
                {
                    string error = "a role with this name already exists";
                    return Json(new { err = error }, JsonRequestBehavior.AllowGet);
                }

                // Get Roles to update screen
                if (isAppLinked == "1")
                {
                    if (selectedRoleApp != null)
                    {
                        var roles = new RoleProxy().RoleChannel.GetApplicationRoles(new Application
                        {
                            Id = int.Parse(selectedRoleApp),
                            RoleType = string.IsNullOrEmpty(roletype) ? 0 : int.Parse(roletype),
                            ExcludeLesserRoles = true
                        });
                        model.OperatorType = int.Parse(GenericPrincipalExtensions.GetClaim(User, "OperatorType"));
                        model.Roles = roles;
                    }
                }
                else
                {
                    var roles = new RoleProxy().RoleChannel.GetSystemRoles();
                    model.OperatorType = int.Parse(GenericPrincipalExtensions.GetClaim(User, "OperatorType"));
                    model.Roles = roles;
                }

                return Json(new { status = result, page = Helper.JsonPartialView(this, "RolePriviManagement/_EditRole", model) }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0, err = "You must select an application and then enter a role name" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AssignRolePrivileges(Role role)
        {
            if ((!string.IsNullOrEmpty(role.AddPrivileges) || !string.IsNullOrEmpty(role.AddRoles)) && role.Id != 0)
            {
                int result;

                // disect string
                if (!string.IsNullOrEmpty(role.AddPrivileges))
                {
                    List<Privilege> privis = new List<Privilege>();
                    foreach (string privi in role.AddPrivileges.Split(','))
                    {
                        Privilege p = new Privilege()
                        {
                            Id = int.Parse(privi),
                            RoleId = role.Id,
                            EditBy = int.Parse(User.Identity.GetUserId())
                        };

                        privis.Add(p);
                    }

                    result = new RoleProxy().RoleChannel.AddPrivilegesToRole(privis);
                }
                else
                {
                    List<Role> grps = new List<Role>();
                    foreach (string grp in role.AddRoles.Split(','))
                    {
                        Role p = new Role()
                        {
                            Id = int.Parse(grp),
                            AddToRole = role.Id,
                            EditBy = int.Parse(User.Identity.GetUserId())
                        };

                        grps.Add(p);
                    }

                    result = new RoleProxy().RoleChannel.AddRolesToRole(grps);
                }

                // Get updated role details
                Role updatedrole = new RoleProxy().RoleChannel.GetRoleDetails(role);
                return Json(new { status = result, page = Helper.JsonPartialView(this, "RolePriviManagement/_RolePriviPartial", updatedrole) }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { status = 0, err = "Select a role and select 1 or more privileges" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveRolePrivileges(Role role)
        {
            if ((!string.IsNullOrEmpty(role.RemovePrivileges) || !string.IsNullOrEmpty(role.RemoveRoles)) && role.Id != 0)
            {
                int result;

                // disect string
                if (!string.IsNullOrEmpty(role.RemovePrivileges))
                {
                    List<Privilege> privis = new List<Privilege>();
                    foreach (string privi in role.RemovePrivileges.Split(','))
                    {
                        Privilege p = new Privilege()
                        {
                            Id = int.Parse(privi),
                            RoleId = role.Id,
                            EditBy = int.Parse(User.Identity.GetUserId())
                        };

                        privis.Add(p);
                    }

                    result = new RoleProxy().RoleChannel.RemovePrivilegesFromRole(privis);
                }
                else
                {
                    List<Role> roles = new List<Role>();
                    foreach (string grp in role.RemoveRoles.Split(','))
                    {
                        Role p = new Role()
                        {
                            Id = int.Parse(grp),
                            AddToRole = role.Id,
                            EditBy = int.Parse(User.Identity.GetUserId())
                        };

                        roles.Add(p);
                    }

                    result = new RoleProxy().RoleChannel.RemoveRolesFromRole(roles);
                }

                // Get updated role details
                Role updatedrole = new RoleProxy().RoleChannel.GetRoleDetails(role);
                return Json(new { status = result, page = Helper.JsonPartialView(this, "RolePriviManagement/_RolePriviPartial", updatedrole) }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { status = 0, err = "Select a role and select 1 or more privileges" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddRoleToRole(Role role)
        {
            if (!string.IsNullOrEmpty(role.AddPrivileges) && role.Id != 0)
            {
                role.Privileges = new List<Privilege>();
                role.EditBy = int.Parse(User.Identity.GetUserId());

                // disect string
                foreach (string privi in role.AddPrivileges.Split(','))
                {
                    Privilege p = new Privilege()
                    {
                        Id = int.Parse(privi)
                    };

                    role.Privileges.Add(p);
                }

                int result = new PrivilegeProxy().PrivilegeChannel.AddPrivilegesToGroup(role);

                // Get updated role details
                Role updatedgrp = new RoleProxy().RoleChannel.GetRoleDetails(role);
                return Json(new { status = result, page = Helper.JsonPartialView(this, "RolePriviManagement/_PriviGrpPartial", updatedgrp) }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { status = 0, err = "Select a role and select 1 or more privileges" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AssignRoles(string addRoles, string selectedOperator, string selectedRoleAppId)
        {
            if (!string.IsNullOrEmpty(addRoles) && !string.IsNullOrEmpty(selectedOperator) && !string.IsNullOrEmpty(selectedRoleAppId))
            {
                // disect string
                var roles = new List<Role>();
                foreach (string s in addRoles.Split(','))
                {
                    var r = new Role
                    {
                        Id = int.Parse(s),
                        OwnerId = int.Parse(selectedOperator),
                        EditBy = int.Parse(User.Identity.GetUserId())
                    };

                    roles.Add(r);
                }

                new RoleProxy().RoleChannel.AddRolesToOperator(roles);

                // Get updated role details
                return GetOperatorRoles(selectedOperator, selectedRoleAppId);
            }

            return Json(new { status = 0, err = "Select a role and select 1 or more privileges" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveRoles(string removeRoles, string selectedOperator, string selectedRoleAppId)
        {
            if (!string.IsNullOrEmpty(removeRoles) && !string.IsNullOrEmpty(selectedOperator) && !string.IsNullOrEmpty(selectedRoleAppId))
            {
                // disect string
                var roles = new List<Role>();
                foreach (string s in removeRoles.Split(','))
                {
                    var r = new Role
                    {
                        Id = int.Parse(s),
                        OwnerId = int.Parse(selectedOperator)
                    };

                    roles.Add(r);
                }

                new RoleProxy().RoleChannel.RemoveRolesFromOperator(roles);

                // Get updated role details
                return GetOperatorRoles(selectedOperator, selectedRoleAppId);
            }

            return Json(new { status = 0, err = "Select a role and select 1 or more privileges" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateRole(Role role)
        {
            if (!string.IsNullOrEmpty(role.Name))
            {
                role.EditBy = int.Parse(User.Identity.GetUserId());

                int result = new RoleProxy().RoleChannel.UpdateRole(role);

                if (result == 3)
                    return Json(new { status = 0, err = "A role already exists with this name" }, JsonRequestBehavior.AllowGet);

                return Json(new { status = result }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { status = 0, err = "You must enter a role name" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteRole(Role role)
        {
            if (role != null)
            {
                role.EditBy = int.Parse(User.Identity.GetUserId());

                int result = new RoleProxy().RoleChannel.DeleteRole(role);
                return Json(new { status = result }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { status = 0, err = "Error deleting role" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetApplicationRoles(string appid, string roletype)
        {
            if (!string.IsNullOrEmpty(appid))
            {
                var result = new RoleProxy().RoleChannel.GetApplicationRoles(new Application
                {
                    Id = int.Parse(appid),
                    RoleType = string.IsNullOrEmpty(roletype) ? 0 : int.Parse(roletype),
                    ExcludeLesserRoles = true
                });

                var model = new BaseModel
                {
                    Roles = result,
                    OperatorType = int.Parse(GenericPrincipalExtensions.GetClaim(User, "OperatorType"))
                };

                if (result == null)
                {
                    string error = "No roles found";
                    return Json(new { err = error }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = 1,
                    page = Helper.JsonPartialView(this, "RolePriviManagement/_EditRole", model)
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0, err = "No application selected" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOperatorRoles(string oprid, string appid)
        {
            if (!string.IsNullOrEmpty(oprid))
            {
                var result = new OperatorProxy().OperatorChannel.GetOperatorRoles(new Operator
                {
                    AppId = appid,
                    OperatorId = int.Parse(oprid)
                });

                if (result == null)
                {
                    return Json(new { err = "No roles not found" }, JsonRequestBehavior.AllowGet);
                }

                var allroles = new RoleProxy().RoleChannel.GetApplicationRoles(new Application { Id = int.Parse(appid) });
                var model = new Operator
                {
                    OperatorId = int.Parse(oprid),
                    AppId = appid,
                    Roles = result,
                    SelectedRoles = result,
                    UnselectedRoles = allroles.Where(x => result.All(z => z.Id != x.Id)).ToList()
                };
                return Json(new
                {
                    status = 1,
                    page = Helper.JsonPartialView(this, "RolePriviManagement/_RoleOprPartial", model)
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0, err = "No operator selected" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetRoleDetails(Role role)
        {
            if (role.Id != 0)
            {
                var result = new RoleProxy().RoleChannel.GetRoleDetails(role);

                if (result == null)
                {
                    return Json(new { err = "Role not found" }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = 1, page = Helper.JsonPartialView(this, "RolePriviManagement/_RolePriviPartial", result) }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0, err = "No role selected" }, JsonRequestBehavior.AllowGet);
        }
    }
}