﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using UMA.Models;
using UMA_Proxy;
using UMAentities;
using Helper = UMA.Helpers.Helper;


namespace UMA.Controllers
{
    [AuthorizeCustom]
    public class ApplicationController : Controller
    {       
        public ActionResult GetApplications()
        {
            try
            {
                List<Application> result = new ApplicationProxy().ApplicationChannel.GetApplications();
                result = result.OrderBy(x => x.Name).ToList();
                var jsonList = new List<object>();

                foreach (Application app in result)
                {
                    jsonList.Add(new { text = app.Name, id = app.Id });
                }

                return Json(new { result = new JavaScriptSerializer().Serialize(jsonList) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetMyApplications()
        {
            try
            {
                List<Application> result = new ApplicationProxy().ApplicationChannel.GetApplications();

                result = result.Where(x => x.OwnerId == int.Parse(User.Identity.GetUserId())).OrderBy(x => x.Name).ToList();
                var jsonList = new List<object>();

                foreach (Application app in result)
                {
                    jsonList.Add(new { text = app.Name, id = app.Id });
                }

                return Json(new { result = new JavaScriptSerializer().Serialize(jsonList) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CreateApp(BaseModel model)
        {
            try
            {
                if (!string.IsNullOrEmpty(model.NewAppName))
                {
                    int result = new ApplicationProxy().ApplicationChannel.AddApplication(new Application() { Name = model.NewAppName, OwnerId = int.Parse(User.Identity.GetUserId()) });

                    if (result == 2)
                    {
                        string error = "An application with this name already exists";
                        return Json(new { err = error }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        // Get Applications to update screen
                        List<Application> apps = new ApplicationProxy().ApplicationChannel.GetApplications();
                        return Json(new { status = result, page = Helper.JsonPartialView(this, "ApplicationManagement/_EditApplication", apps) }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                    return Json(new { status = 0, err = "You must enter an application name" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpdateApp(Application app)
        {
            try
            {
                if (!string.IsNullOrEmpty(app.Name))
                {
                    app.EditBy = int.Parse(User.Identity.GetUserId());

                    int result = new ApplicationProxy().ApplicationChannel.UpdateApplication(app);

                    if (result == 3)
                        return Json(new { status = 0, err = "An application already exists with this name" }, JsonRequestBehavior.AllowGet);

                    return Json(new { status = result }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { status = 0, err = "You must enter an application name" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteApp(Application app)
        {
            try
            {
                if (app != null)
                {
                    app.EditBy = int.Parse(User.Identity.GetUserId());

                    int result = new ApplicationProxy().ApplicationChannel.DeleteApplication(app);
                    return Json(new { status = result }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { status = 0, err = "Error deleting application" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetUmaUsers(string appid)
        {
            try
            {
                if (!string.IsNullOrEmpty(appid))
                {
                    List<Operator> result = new ApplicationProxy().ApplicationChannel.GetUmaUsers(new Application() { OwnerId = int.Parse(User.Identity.GetUserId()), Id = int.Parse(appid) });

                    result = result.OrderBy(x => x.FirstName).ToList();
                    var jsonList = new List<object>();

                    foreach (Operator op in result)
                    {
                        jsonList.Add(new { text = op.FullName, id = op.OperatorId });
                    }

                    return Json(new { result = new JavaScriptSerializer().Serialize(jsonList) }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { result = -1 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetSharedAppDetails(BaseModel model)
        {
            try
            {
                if (!string.IsNullOrEmpty(model.SelectedSharedApp))
                {
                    model.AllowedStaff = new ApplicationProxy().ApplicationChannel.GetSharedAppUsers(new Application() { Id = int.Parse(model.SelectedSharedApp), OwnerId = int.Parse(User.Identity.GetUserId()) });
                    return Json(new { status = 1, page = Helper.JsonPartialView(this, "ApplicationManagement/_ShareApplication", model) }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { status = 0, err = "You must enter an application name" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetBranchAdmins(string branchid)
        {
            try
            {
                if (!string.IsNullOrEmpty(branchid))
                {
                    var model = new BaseModel
                    {
                        OperatorType = int.Parse(GenericPrincipalExtensions.GetClaim(User, "OperatorType")),
                        CreatedAdmins = new ApplicationProxy().ApplicationChannel.GetBranchAdmins(new Application()
                        {
                            OwnerId = int.Parse(User.Identity.GetUserId()),
                            BranchId = int.Parse(branchid)
                        })
                    };

                    return Json(new { status = 1, page = Helper.JsonPartialView(this, "ApplicationManagement/_ApplicationAdmins", model) }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = 0, err = "You must select a branch" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetBranchUsers(string branchid)
        {
            try
            {
                if (!string.IsNullOrEmpty(branchid))
                {
                    var model = new BaseModel
                    {
                        OperatorType = int.Parse(GenericPrincipalExtensions.GetClaim(User, "OperatorType")),
                        BranchName = new BranchProxy().BranchChannel.GetBranchName(int.Parse(branchid)),
                        CreatedAdmins = new ApplicationProxy().ApplicationChannel.GetBranchUsers(new Application()
                        {
                            BranchId = int.Parse(branchid),
                            OwnerId = int.Parse(User.Identity.GetUserId())
                        })
                    };

                    return Json(new { status = 1, page = Helper.JsonPartialView(this, "ApplicationManagement/_ApplicationAdmins", model) }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = 0, err = "You must select a branch" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveSharedAppUsers(BaseModel model)
        {
            try
            {
                if (!string.IsNullOrEmpty(model.SelectedSharedUsers) && !string.IsNullOrEmpty(model.SelectedSharedAppSave))
                {
                    model.AllowedStaff = new ApplicationProxy().ApplicationChannel.SaveSharedAppUsers(new Application()
                    {
                        Id = int.Parse(model.SelectedSharedAppSave),
                        OwnerId = int.Parse(User.Identity.GetUserId()),
                        SelectedUsers = model.SelectedSharedUsers
                    });

                    if (model.AllowedStaff == null)
                        return Json(new { status = 0, err = "You are not the owner of this application" }, JsonRequestBehavior.AllowGet);

                    return Json(new { status = 1, page = Helper.JsonPartialView(this, "ApplicationManagement/_ShareApplication", model) }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { status = 0, err = "You must enter an application name" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RemoveShareApp(BaseModel model)
        {
            try
            {
                if (!string.IsNullOrEmpty(model.SelectedSharedAppRemove) && !string.IsNullOrEmpty(model.RemoveShareAppId))
                {
                    model.AllowedStaff = new ApplicationProxy().ApplicationChannel.RemoveShareApp(new Application()
                    {
                        Id = int.Parse(model.SelectedSharedAppRemove),
                        OwnerId = int.Parse(User.Identity.GetUserId()),
                        EditBy = int.Parse(model.RemoveShareAppId)
                    });

                    return Json(new { status = 1, page = Helper.JsonPartialView(this, "ApplicationManagement/_ShareApplication", model) }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { status = 0, err = "You must enter an application name" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<ActionResult> CreateBranchAdmin(BaseModel model)
        {
            try
            {
                if (!string.IsNullOrEmpty(model.CreateAdminBranchId))
                {
                    var user = new ApplicationUser
                    {
                        UserName = model.NewAdminUsername,
                        Email = model.NewAdminUsername,
                        FirstName = string.Empty,
                        Surname = string.Empty,
                        CreatedBy = int.Parse(User.Identity.GetUserId())
                    };

                    var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var result = await userManager.CreateAsync(user, model.NewAdminPassword);
                    if (result.Succeeded)
                    {
                        // Create & Assign developer role
                        var role = HttpContext.GetOwinContext().GetUserManager<ApplicationRoleManager>().FindById("2");
                        var rolesForUser = userManager.GetRoles(user.Id);
                        if (!rolesForUser.Contains(role.Name))
                        {
                            userManager.AddToRole(user.Id, role.Name);
                        }

                        // Send email and stuff - Deactivated for now
                        var code = await userManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        var callbackUrl = Url.Action("ConfirmEmail", "Account", new
                        {
                            userId = user.Id,
                            code
                        }, protocol: Request.Url?.Scheme);
                        await userManager.SendEmailAsync(user.Id, "Confirm your account", 
                            "Please confirm your account by clicking this link: <a href=\"" + callbackUrl + "\">link</a>");

                        model.OperatorType = int.Parse(GenericPrincipalExtensions.GetClaim(User, "OperatorType"));
                        model.CreatedAdmins = new ApplicationProxy().ApplicationChannel.GetBranchUsers(new Application
                        {
                            BranchId = int.Parse(user.Branch),
                            OwnerId = int.Parse(user.Id)
                        });

                        model.NewAdminUsername = string.Empty;

                        return Json(new
                        {
                            status = 1,
                            page = Helper.JsonPartialView(this, "ApplicationManagement/_ApplicationAdmins", model)
                        }, JsonRequestBehavior.AllowGet);
                    }

                    return Json(new { status = 0, err = result.Errors.FirstOrDefault() }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = 0, err = "You must select a branch" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<ActionResult> CreateSuperUser(BaseModel model)
        {
            try
            {
                // Ensure that the user is an Iris Staff
                if (model.NewAdminUsername.EndsWith("@irissmart.com"))
                {
                    var user = new ApplicationUser
                    {
                        UserName = model.NewAdminUsername,
                        Email = model.NewAdminUsername,
                        FirstName = string.Empty,
                        Surname = string.Empty,
                        CreatedBy = int.Parse(User.Identity.GetUserId())
                    };
                    var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

                    // Create a default password
                    model.NewAdminPassword = Membership.GeneratePassword(6, 0);

                    var result = await userManager.CreateAsync(user, model.NewAdminPassword);
                    if (result.Succeeded)
                    {
                        // Create & Assign superuser role
                        var role = HttpContext.GetOwinContext().GetUserManager<ApplicationRoleManager>().FindById("4");
                        var rolesForUser = userManager.GetRoles(user.Id);
                        if (!rolesForUser.Contains(role.Name))
                        {
                            userManager.AddToRole(user.Id, role.Name);
                        }

                        // Send email and stuff
                        var code = await userManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        var callbackUrl = Url.Action("ConfirmEmail", "Account", new
                        {
                            userId = user.Id,
                            code
                        }, protocol: Request.Url?.Scheme);

                        await userManager.SendEmailAsync(user.Id, "Confirm your account", 
                            "Hello, <br style='margin-bottom: 5px' />Please confirm your account by clicking this link: <a href=\"" + callbackUrl
                            + "\">link</a><br style='margin-bottom: 5px' /> Your default password is " + model.NewAdminPassword);

                        model = new BaseModel
                        {
                            OperatorType = int.Parse(GenericPrincipalExtensions.GetClaim(User, "OperatorType")),
                            SuperUsers = new ApplicationProxy().ApplicationChannel.GetSuperUsers()
                        };

                        return Json(new
                        {
                            status = 1,
                            page = Helper.JsonPartialView(this, "ApplicationManagement/ApplicationSuperUsers", model)
                        }, JsonRequestBehavior.AllowGet);
                    }

                    return Json(new { status = 0, err = result.Errors.FirstOrDefault() }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = -1, err="Invalid email: Only Iris staff are allowed to be superusers."
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<ActionResult> CreateBranchUser(BaseModel model)
        {
            try
            {
                var user = new ApplicationUser
                {
                    UserName = model.NewAdminUsername,
                    Email = model.NewAdminUsername,
                    FirstName = string.Empty,
                    Surname = string.Empty,
                    Branch = GenericPrincipalExtensions.GetClaim(User, "Branch"),
                    CreatedBy = int.Parse(User.Identity.GetUserId())
                };
                var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var result = await userManager.CreateAsync(user, model.NewAdminPassword);

                if (result.Succeeded)
                {
                    // Create & Assign developer role
                    var role = HttpContext.GetOwinContext().GetUserManager<ApplicationRoleManager>().FindById("3");
                    var rolesForUser = userManager.GetRoles(user.Id);
                    if (!rolesForUser.Contains(role.Name))
                    {
                        userManager.AddToRole(user.Id, role.Name);
                    }

                    // Send email and stuff - Deactivated for now
                    var code = await userManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new
                    {
                        userId = user.Id,
                        code
                    }, protocol: Request.Url?.Scheme);
                    await userManager.SendEmailAsync(user.Id, "Confirm your account", 
                        "Please confirm your account by clicking this link: <a href=\"" + callbackUrl + "\">link</a>");

                    model.OperatorType = int.Parse(GenericPrincipalExtensions.GetClaim(User, "OperatorType"));
                    var op = await userManager.FindByIdAsync(User.Identity.GetUserId());
                    model.CreatedAdmins = new ApplicationProxy().ApplicationChannel.GetBranchUsers(new Application()
                    {
                        BranchId = int.Parse(op.Branch),
                        OwnerId = int.Parse(op.Id)
                    });

                    return Json(new
                    {
                        status = 1,
                        page = Helper.JsonPartialView(this, "ApplicationManagement/_ApplicationAdmins", model)
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = 0, err = result.Errors.FirstOrDefault() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}