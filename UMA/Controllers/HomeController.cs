﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UMA.Models;
using UMA_Proxy;
using UMAentities;


namespace UMA.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public HomeController()
        {

        }

        public HomeController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        public async Task<ActionResult> Index()
        {
            BaseModel model = new BaseModel();
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (user.FirstTimeLog)
            {
                ViewBag.IsFirstTime = true;
                user.FirstTimeLog = false;

                // update it to show its not first time anymore
                await UserManager.UpdateAsync(user);
            }

            // Get Lists
            model.Applications = new ApplicationProxy().ApplicationChannel.GetApplications();
            model.Privileges = new PrivilegeProxy().PrivilegeChannel.GetPrivileges();
            model.RoleTypes = new RoleProxy().RoleChannel.GetRoleTypes();
            model.Roles = new RoleProxy().RoleChannel.GetSystemRoles();

            // set operator type
            string result = GenericPrincipalExtensions.GetClaim(User, "OperatorType");

            // if you cannot find the claim, sign him in again
            if (string.IsNullOrEmpty(result))
            {
                AuthenticationManager.SignOut();
                return RedirectToAction("Login", "Account");
            }
            else
            {
                model.OperatorType = int.Parse(result);

                if (model.OperatorType == 1)
                {
                    model.SuperUsers = new ApplicationProxy().ApplicationChannel.GetSuperUsers();
                }

                if (model.OperatorType == 2 || model.OperatorType == 4)
                {
                    // Get user requests
                    model.UserRequests = new List<Operator>();
                }

                if (model.OperatorType == 2)
                {
                    // Get the branch name
                    if (!string.IsNullOrEmpty(user.Branch))
                    {
                        model.BranchName = new BranchProxy().BranchChannel.GetBranchName(int.Parse(user.Branch));
                        model.CreatedAdmins = new ApplicationProxy().ApplicationChannel.GetBranchUsers(new Application() { BranchId = int.Parse(user.Branch), OwnerId = int.Parse(user.Id) });
                    }
                    else
                    {
                        AuthenticationManager.SignOut();
                        return RedirectToAction("Login", "Account", new { msg = "NoBranchFound" });
                    }
                }
            }

            return View(model);
        } 
        
        #region Misc Json

        public ActionResult GetBranches()
        {
            try
            {
                List<Branch> result = new BranchProxy().BranchChannel.GetBranches();

                var jsonList = new List<object>();

                foreach (Branch branch in result)
                {
                    jsonList.Add(new { text = branch.BranchName, id = branch.BranchId });
                }

                return Json(new { result = new JavaScriptSerializer().Serialize(jsonList) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetBranchUsers()
        {
            try
            {
                Application app = new Application()
                {
                    BranchId = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Branch"))
                };

                List<Operator> result = new ApplicationProxy().ApplicationChannel.GetBranchUsers(app);

                var jsonList = new List<object>();

                foreach (Operator op in result)
                {
                    jsonList.Add(new { id = op.OperatorId, text = op.FullName });
                }

                return Json(new { result = new JavaScriptSerializer().Serialize(jsonList) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetBranchUsersById(string branchId)
        {
            try
            {
                Application app = new Application()
                {
                    BranchId = int.Parse(branchId)
                };

                List<Operator> result = new ApplicationProxy().ApplicationChannel.GetBranchUsers(app);

                var jsonList = new List<object>();

                foreach (Operator op in result)
                {
                    jsonList.Add(new { id = op.OperatorId, text = op.FullName });
                }

                return Json(new { result = new JavaScriptSerializer().Serialize(jsonList) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = -1 }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}