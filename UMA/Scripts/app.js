﻿$(function () {
    // Effects
    $(document).on('click', '.groupboxtitle', function () {
        var res = $(this).parent().find('.groupbox');
        res.slideToggle();

        // Scroll to element
        $('html, body').animate({
            scrollTop: res.offset().top
        }, 700);
    });

    $(document).on('click', '.groupboxinnertitle', function () {
        var res = $(this).parent().find('.groupboxinner');
        res.slideToggle();

        // Scroll to element
        $('html, body').animate({
            scrollTop: res.offset().top
        }, 700);
    });

    // Since the delete button is in another form, I am clicking on it externally to access correct ajax/form
    $(document).on('click', '.delbutton', function () {
        $(this).parent().parent().find(".hiddenclick").click();
        $(this).parent().parent().find(".hiddenclickdelrole").click();
    });
    
    // Default Functions
    var ajaxUpdate = function (form) {
        var $form = form;

        if ($form.valid()) {
            var options = {
                url: $form.attr("action"),
                type: $form.attr("method"),
                data: $form.serialize()
            };

            $.ajax(options).done(function (data) {
                var $target = $('#' + $form.attr("data-uma-target"));
                var successmsg = $form.attr("data-uma-success");

                if (data.err !== undefined)
                    $form.find('.apperror').text(data.err).slideDown();
                else
                {
                    if (successmsg !== undefined) {
                        // If its a delete, remove that row
                        if (successmsg.indexOf("removed") > -1)
                            $form.parent().fadeOut();
                        else
                            showPopup(successmsg);
                    }
                }

                $('.loadingdiv').removeClass("loading");
            }).error(function (data) {
                showPopup('Could not connect to server');
                $('.loadingdiv').removeClass("loading");
            });
        }
        else
            $('.loadingdiv').removeClass("loading");

        return false;
    };

    var ajaxPartialUpdate = function (form) {
        var $form = form;

        if ($form.valid()) {
            var options = {
                url: $form.attr("action"),
                type: $form.attr("method"),
                data: $form.serialize()
            };
            
            $.ajax(options).done(function (data) {
                var $target = $('#' + $form.attr("data-uma-target"));

                if (data.err !== undefined)
                    $form.find('.apperror').text(data.err).slideDown();
                else
                {
                    // Cleanup
                    $(this).trigger("reset");
                    $form.find('.apperror').text('').slideUp();
                    $form.find('input[type="text"]').each(function () {
                        $(this).val('');
                    });

                    $target.fadeOut(function () {
                        $(this).html(data.page);

                        // Some special cases
                        if ($(this).attr('id') == 'shareappdiv')
                        {
                            $('#SelectedSharedAppRemove').val($('#SelectedSharedApp').val());

                            $("#cmbSelectStaff").select2({
                                placeholder: "Select a user",
                                minimumResultsForSearch: 8,
                                multiple: true,
                                ajax: {
                                    url: "/Application/GetUmaUsers",
                                    dataType: 'json',
                                    quietMillis: 250,
                                    data: function (term, page) {
                                        return {
                                            q: term, appid : $('#SelectedSharedApp').val()
                                        };
                                    },
                                    results: function (data, page) {
                                        return { results: $.parseJSON(data.result) };
                                    },
                                    cache: true
                                }
                            }).change(function () {
                                $('#SelectedSharedUsers').val($(this).val());
                                $('#SelectedSharedAppSave').val($('#SelectedSharedApp').val());
                            });
                        }

                        if ($(this).attr('id').indexOf('roleprividiv') > -1) {
                            // For filtering purposes on system roles per application
                            $('.privisort').each(function (i, obj) {
                                $(this).select2({
                                    placeholder: "Filter by application",
                                    minimumResultsForSearch: 8,
                                    ajax: {
                                        url: "/Application/GetApplications",
                                        dataType: 'json',
                                        quietMillis: 250,
                                        data: function (term, page) {
                                            return {
                                                q: term,
                                            };
                                        },
                                        results: function (data, page) {
                                            return { results: $.parseJSON(data.result) };
                                        },
                                        cache: true
                                    }
                                }).change(function () {
                                    var appid = $(this).val();
                                    $(this).closest('.privisortparent').find('.leftsidebox .prividiv, .leftsidebox .privigrp').each(function () {
                                        // seperate the id
                                        var priviappid = $.grep(this.className.split(" "), function (v, i) {
                                            return v.indexOf('app') === 0;
                                        }).join().replace('app', '');

                                        if (priviappid != appid) {
                                            $(this).fadeOut().addClass('filterout');
                                        }
                                        else {
                                            $(this).fadeIn().removeClass('filterout');
                                        }
                                    });
                                });
                            });
                        }

                        // Clear data on role updates
                        if ($(this).attr('id').indexOf('roleprividiv') > -1) {
                            var id = $(this).attr('id').replace('roleprividiv', '');
                            $('#AddPrivileges' + id).val('');
                            $('#AddPrivilegeGroups' + id).val('');
                            $('#RemovePrivileges' + id).val('');
                            $('#RemovePrivilegeGroups' + id).val('');
                            selectedprivisadd = {};
                            selectedprivisrem = {};
                            selectedprivigrpsadd = {};
                            selectedprivigrpsrem = {};
                        }
                        
                        $(this).fadeIn(function () {
                            // Scroll to element
                            //$('html, body').animate({
                            //    scrollTop: $form.offset().top
                            //}, 500);
                        });
                    });
                }

                $('.loadingdiv').removeClass("loading");
            }).error(function (e, xhr, settings) {
                showPopup('An error occurred processing your request. It has been logged.');
                $('.loadingdiv').removeClass("loading");
            })
        }
        else
            $('.loadingdiv').removeClass("loading");
    };

    var ajaxInsert = function (form) {
        var $form = form;

        if ($form.valid()) {
            var options = {
                url: $form.attr("action"),
                type: $form.attr("method"),
                data: $form.serialize()
            };

            $.ajax(options).done(function (data) {
                if (data.err !== undefined)
                    $form.find('.apperror').text(data.err).slideDown();
                else
                {
                    var successmsg = $($form.attr("data-uma-success"));
                    $form.find('input[type="text"]').each(function () {
                        $(this).val('');
                    });
                    $form.find('.apperror').text('').slideUp();
                }

                $('.loadingdiv').removeClass("loading");
            });

            $.ajax(options).error(function (data) {
                $('.loadingdiv').removeClass("loading");
            });
        }

        return false;
    };

    // encountered the weirdest error where data wasn't passed properly back to server. This is a patch as i can't figure out whats going on
    $(document).on('click', ".deactivateopr", function (e) {
        var id = $(this).attr('id').replace('deactivateopr', '');
        $('#RemoveUserOprId' + id).val(id);
    });

    $(document).on('submit', "form[data-uma-partial='true']", function (e) {
        e.preventDefault();
        ajaxPartialUpdate($(this));
    });

    $(document).on('submit', "form[data-uma-insert='true']", function (e) {
        e.preventDefault();
        ajaxInsert($(this));
    });

    $(document).on('submit', "form[data-uma-update='true'] , form[data-uma-del='true']", function (e) {
        e.preventDefault();
        ajaxUpdate($(this));
    });

    // Open up openables
    $('.open').slideDown();
});