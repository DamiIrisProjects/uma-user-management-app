﻿$(function () {
    // application wide functions
    $(document).on('click', '.closeme, .closebtn, .closeButton', function () {
        $('.PopupWindow').fadeOut(function () { $(this).trigger('close') });
    });

    $(document).on('click', 'input[type=submit]', function () {
        if($(this).parents('form:first').valid())
            $(this).siblings('.loadingdiv').addClass("loading");
    });

    // Login functions & menu
    $(document).on('DOMSubtreeModified', '.validationMessage, .validation-summary-errors', function () {
        var span = $('#' + this.id + ' span span');
        if ($(this).css('display') == 'none' && (span.text() != '' || $(this).hasClass("isDiv"))) {
            $(this).slideDown();
            $('.loadingdiv').removeClass("loading");
        }
    });

    $('.accountoptions').click(function () {
        $("#menu2").fadeIn();
    });

    $(document).ajaxError(function (e, xhr, settings) {
        if (xhr.status == 403) {
            location = '/Login';
        }
    });

    $('.outersub').css("min-width", $('#divLoggedIn').width() + "px").css("left", "-" + ($('#divLoggedIn').width() - 107 + 77) + "px");

    $("#menu li.menuitem").hoverIntent(function () {
        if (!$(this).is(':animated'))
            $(this).find("div.outersub").hide().fadeIn();
    },
     function () {
         $(this).find("div.outersub").fadeOut(100);
     });

    $("#divLoggedIn").hoverIntent(function () {
        if (!$(this).is(':animated'))
            $(this).find("div.outersub").hide().slideDown();
    },
    function () {
        if (!$(this).is(':animated'))
            $(this).find("div.outersub").slideUp(100);
    });


    $("#menu li.menuitem div.submenu div.ssmenu").hoverIntent(function () {
        if (!$(this).is(':animated'))
            $(this).find("div.submenu").hide().fadeIn();
    },
    function () {
        if (!$(this).is(':animated'))
            $(this).find("div.submenu").fadeOut(100);
    });
});

function showPopup(msg) {
    $('#popupmsgInner').text(msg);
    $('#PopupMsg').lightbox_me({
        centered: true
    });
}