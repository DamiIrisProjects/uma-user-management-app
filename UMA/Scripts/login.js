﻿$(function () {
    $('#forgottenpwdiv').click(function () {
        $('#forgotpassword').lightbox_me({
            centered: true
        });
    });

    // If the user just presses enter instead
    $('#txtresetemail').keydown(function (event) {
        if (event.keyCode == 13) {
            SendReset();
        }
    });

});

function SendReset() {
    $('#forgotpwval').slideUp().text('');
    var email = $('#txtresetemail').val();
    if (email == '') {
        $('#forgotpwval').text('Please enter your email address').slideDown();
    }
    else {
        $('#forgotpwloadingdiv').addClass('loading');
        $.ajax({
            type: "POST",
            url: "/Account/ForgotPassword",
            data: { email: email },
            dataType: "json",
            error: function (result) {
                $('#forgotpwloadingdiv').removeClass('loading');
                $('#forgotpwval').text('Unable to connect to server').slideDown();
            },
            success: function (result) {
                $('#forgotpwloadingdiv').removeClass('loading');
                if (result.status == "0")
                    $('#forgotpwval').addClass('success').text("An email has been sent to you Iris mailbox giving details on how to reset your password.").slideDown();

                if (result.status == "1")
                    $('#forgotpwval').text("An email has already been sent to your mailbox giving details on how to reset your password.").slideDown();

                if (result.status == "2")
                    $('#forgotpwval').text("This email did not match any of our registered users.").slideDown();

                if (result.status == "5")
                    $('#forgotpwval').text(result.message);
            }
        });
    }
}