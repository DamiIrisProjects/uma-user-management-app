﻿$(function () {
    $("#cmbDefineActionApps, #cmbAssignActionApps").select2({
        placeholder: "Select an application",
        minimumResultsForSearch: 8,
        ajax: {
            url: "/Application/GetApplications",
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    q: term,
                };
            },
            results: function (data, page) {
                return { results: $.parseJSON(data.result) };
            },
            cache: true
        }
    }).change(function () {
        if ($(this).attr('id') == 'cmbDefineActionApps') {
            $('#SelectedActionsApp').val($(this).val());
            $('#defineactionswrap').fadeOut(function () {
                $('#actionAppPrivisload').addClass('loading');
                $.ajax({
                    type: "POST",
                    url: "/Action/GetApplicationActions",
                    data: { appid: $("#cmbDefineActionApps").val() },
                    dataType: "json",
                    error: function (result) {
                        showPopup("Unable to access server");
                        $('#actionAppPrivisload').removeClass('loading');
                    },
                    success: function (result) {
                        $('#SelectedActionPrivilege').val($("#cmbGetAppPrivis").val());
                        $('#actionAppPrivisload').removeClass('loading');
                        $('#defineactionswrap').fadeIn(function () {
                            $('#defineactionsdiv').html(result.page).fadeIn();
                        });
                    }
                });
            });
        }
        else
        {
            $('#actionprividiv').slideDown();
            $('#assignactionswrap').fadeOut();
        }
    });

    $("#cmbGetAppPrivis").select2({
        placeholder: "Select a privilege",
        minimumResultsForSearch: 8,
        ajax: {
            url: "/Privilege/GetApplicationPrivisList",
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    q: term,
                    appid: $("#cmbAssignActionApps").val()
                };
            },
            results: function (data, page) {
                return { results: $.parseJSON(data.result) };
            },
            cache: true
        }
    }).change(function () {
        var privid = $(this).val();
        $('#assignactionswrap').fadeOut(function () {
            $('#assignactionAppPrivisload').addClass('loading');
            $.ajax({
                type: "POST",
                url: "/Action/GetPrivilegeActions",
                data: { privid: privid, appid: $('#cmbAssignActionApps').val() },
                dataType: "json",
                error: function (result) {
                    showPopup("Unable to access server");
                    $('#assignactionAppPrivisload').removeClass('loading');
                },
                success: function (result) {
                    $('#SelectedAssignActionPrivilege').val($("#cmbGetAppPrivis").val());
                    $('#assignactionswrap').html(result.page).fadeIn(function () {
                        $('#assignactionAppPrivisload').removeClass('loading');
                    });
                }
            });
        });
    });
});