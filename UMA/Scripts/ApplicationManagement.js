﻿$(function () {
    // Select branch
    $("#cmbBranches, #cmbSelectBranchCreateUser").select2({
        placeholder: "Select a branch",
        minimumResultsForSearch: 8,
        ajax: {
            url: "/Home/GetBranches",
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    q: term,
                };
            },
            results: function (data, page) {
                return { results: $.parseJSON(data.result) };
            },
            cache: true
        }
    }).change(function () {
        if ($(this).attr('id') == 'cmbSelectBranchCreateUser') {
            $('#branchsuperuserload').addClass('loading');
            $('#createappuseriv').fadeOut(function () {
                $.ajax({
                    type: "POST",
                    url: "/Application/GetBranchUsers",
                    data: { branchid: $("#cmbSelectBranchCreateUser").val() },
                    dataType: "json",
                    error: function (result) {
                        showPopup("Unable to access server");
                        $('#branchsuperuserload').removeClass('loading');
                    },
                    success: function (result) {
                        $('#createappuseriv').html(result.page).fadeIn(function () {
                            $('#branchsuperuserload').removeClass('loading');
                        });
                        $('#CreateAdminBranchId').val($("#cmbSelectBranchCreateUser").val());
                    }
                });
            });
        }
        else {
            $('#SelectedAdminBranch').val($(this).val());
            $('#adminsappload').addClass('loading');
            $('#createappadmindiv').fadeOut(function () {
                $.ajax({
                    type: "POST",
                    url: "/Application/GetBranchAdmins",
                    data: { branchid: $("#cmbBranches").val() },
                    dataType: "json",
                    error: function (result) {
                        showPopup("Unable to access server");
                        $('#adminsappload').removeClass('loading');
                    },
                    success: function (result) {
                        $('#createappadmindiv').html(result.page).fadeIn(function () {
                            $('#adminsappload').removeClass('loading');
                        });
                        $('#CreateAdminBranchId').val($("#cmbBranches").val());
                    }
                });
            });
        }
    });

    $("#cmbShareApps, #cmbAdminApps, #cmbSelectApp, #cmbSelectApp2").select2({
    placeholder: "Select an application",
    minimumResultsForSearch: 8,
    ajax: {
        url: "/Application/GetApplications",
        dataType: 'json',
        quietMillis: 250,
        data: function (term, page) {
            return {
                q: term,
            };
        },
        results: function (data, page) {
            return { results: $.parseJSON(data.result) };
        },
        cache: true
    }
    }).change(function () {
        // Share application
        if ($(this).attr('id') == 'cmbShareApps') {
            $('#SelectedSharedApp').val($(this).val());
            $('#getsharedappdetailsbtn').click();
        }

        // Create Administrator
        else if ($(this).attr('id') == 'cmbAdminApps') {
            if ($('.branchesdiv').css('display') == 'none') {
                $('.branchesdiv').slideDown();
            }
            else {
                // reset
                $("#cmbBranches").select2('val', '');
                $('#createappadmindiv').fadeOut();
            }
        }

        // Edit Actions
        else if ($(this).attr('id') == 'cmbSelectApp') {
            $('#cmbselectappload').addClass('loading');
            $('#assignrolediv').fadeOut(function () {
                $.ajax({
                    type: "POST",
                    url: "/Role/GetOperatorRoles",
                    data: { oprid: $("#cmbSelectUser").val(), appid: $("#cmbSelectApp").val() },
                    dataType: "json",
                    error: function (result) {
                        showPopup("Unable to access server");
                        $('#cmbselectappload').removeClass('loading');
                    },
                    success: function (result) {
                        $('#assignrolediv').html(result.page).fadeIn(function () {
                            $('#cmbselectappload').removeClass('loading');
                        });
                    }
                });
            });
        }

        else if ($(this).attr('id') == 'cmbSelectApp2') {
            $('#cmbselectappload').addClass('loading');
            $('#assignrolediv').fadeOut(function () {
                $.ajax({
                    type: "POST",
                    url: "/Role/GetOperatorRoles",
                    data: { oprid: $("#cmbSelectUser2").val(), appid: $("#cmbSelectApp2").val() },
                    dataType: "json",
                    error: function (result) {
                        showPopup("Unable to access server");
                        $('#cmbselectappload').removeClass('loading');
                    },
                    success: function (result) {
                        $('#assignrolediv').html(result.page).fadeIn(function () {
                            $('#cmbselectappload').removeClass('loading');
                        });
                    }
                });
            });
        }
    });
})