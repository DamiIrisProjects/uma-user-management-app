﻿// Variables
var selectedprivisadd = {};
var selectedprivisrem = {};
var selectedprivigrpsadd = {};
var selectedprivigrpsrem = {};

$(function () {
    // Allow searching of privileges by removing items as u type
    if (typeof String.prototype.startsWith != 'function') {
        String.prototype.startsWith = function (str) {
            return this.slice(0, str.length) == str;
        };
    }

    $(document).on('input propertychange paste', '.searchbox', function () {
        var txt = $(this).val();        
        $(this).siblings('.box').children('.prividiv, .privigrp').each(function () {
            if (!$(this).hasClass('filterout')) {
                if ($(this).text().toLowerCase().indexOf(txt.toLowerCase()) < 0)
                    $(this).fadeOut();
                else
                    $(this).fadeIn();
            }
        });
    });

    // Assigning a role to a user
    $("#cmbSuperuserRoleBranch").select2({
        placeholder: "Select a branch",
        minimumResultsForSearch: 8,
        ajax: {
            url: "/Home/GetBranches",
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    q: term
                };
            },
            results: function (data, page) {
                return { results: $.parseJSON(data.result) };
            },
            cache: true
        }
    }).change(function () {
        $('#SelectedSuperuserBranchId').val($(this).val());
        $('.selectuser').slideDown();
    });

    $("#cmbSelectUser").select2({
        placeholder: "Select a user",
        minimumResultsForSearch: 8,
        ajax: {
            url: "/Home/GetBranchUsers",
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    q: term
                };
            },
            results: function (data, page) {
                return { results: $.parseJSON(data.result) };
            },
            cache: true
        }
    }).change(function () {
        $('#SelectedUserId').val($(this).val());
        $('#assignrolediv').fadeOut();
        $('#cmbSelectApp2').select2('val', '');
        $('.selectapp').slideDown();
    });

    $("#cmbSelectUser2").select2({
        placeholder: "Select a user",
        minimumResultsForSearch: 8,
        ajax: {
            url: "/Home/GetBranchUsersById",
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    branchid: $('#SelectedSuperuserBranchId').val(),
                    q: term
                };
            },
            results: function (data, page) {
                return { results: $.parseJSON(data.result) };
            },
            cache: true
        }
    }).change(function () {
        $('#SelectedUserId').val($(this).val());
        $('#assignrolediv').fadeOut();
        $('#cmbSelectApp2').select2('val', '');
        $('.selectapp').slideDown();
    });

    // Different application list comboboxes relating the role/privilege management
    $("#cmbEditPriviApps, #cmbEditPriviGrpApps, #cmbEditRoleApps").select2({
        placeholder: "Select an application",
        minimumResultsForSearch: 8,
        ajax: {
            url: "/Application/GetApplications",
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    q: term,
                };
            },
            results: function (data, page) {
                return { results: $.parseJSON(data.result) };
            },
            cache: true
        }
    }).change(function () {
        // Edit Privileges
        if ($(this).attr('id') == 'cmbEditPriviApps') {
            $('#privieditdivwrap').find('.loadingdiv').addClass('loading');
            var appid = $(this).val();
            var getAppPrivis = function () {
                var res;
                $.ajax({
                    type: "POST",
                    url: "/Privilege/GetApplicationPrivis",
                    data: { appid: appid },
                    dataType: "json",
                    error: function (result) {
                        showPopup("Unable to access server");
                        $('#privieditdivwrap .loadingdiv').removeClass('loading');
                    },
                    success: function (result) {
                        $('#privieditdiv').html(result.page).fadeIn(function () {
                            $('#privieditdivwrap .loadingdiv').removeClass('loading');
                        });
                        $('#SelectedEditPriviApp').val(appid);
                    }
                });
                
                return res;
            };

            if ($('#privieditdivwrap').css('display') == 'none') {
                $('#privieditdivwrap').slideDown(function () {
                    getAppPrivis();
                });
            }
            else {
                $('#privieditdiv').fadeOut(function () {
                    getAppPrivis();
                });
            }
        }

        // Edit Roles
        else if ($(this).attr('id').indexOf('cmbEditRoleApps') > -1) {
            var appid = $(this).val();
            var parent = $(this).closest('.groupboxinner');
            parent.find('#editroleload').addClass('loading');
            parent.find('#SelectedRoleApp').val(appid);
            var roletype = parent.find('#RoleType').val();
            var getAppGrps = function () {
                var res;
                $.ajax({
                    type: "POST",
                    url: "/Role/GetApplicationRoles",
                    data: { appid: appid, roletype: roletype },
                    dataType: "json",
                    error: function (result) {
                        parent.find('#editroleload').removeClass('loading');
                        showPopup("Unable to access server");
                    },
                    success: function (result) {
                        parent.find('.groupboxedit').html(result.page).fadeIn(function () {
                            parent.find('#editroleload').removeClass('loading');
                        });
                    }
                });

                return res;
            };

            if (parent.find('#roleeditdivwrap').css('display') == 'none') {
                parent.find('#roleeditdivwrap').slideDown(function () {
                    getAppGrps();
                });
            }
            else {
                parent.find('.groupboxedit').fadeOut(function () {
                    getAppGrps();
                });
            }
        }

        // Edit privilege groups
        else if ($(this).attr('id') == 'cmbEditPriviGrpApps') {
            var appid = $(this).val();
            $('#privigrpeditdivwrap').find('.loadingdiv').addClass('loading');
            var getAppPrivis = function () {
                var res;
                $.ajax({
                    type: "POST",
                    url: "/Application/GetApplicationsPriviGroups",
                    data: { appid: appid },
                    dataType: "json",
                    error: function (result) {
                        $('#privigrpeditdivwrap .loadingdiv').removeClass('loading');
                        showPopup("Unable to access server");
                    },
                    success: function (result) {
                        $('#privigrpeditdiv').html(result.page).fadeIn(function () {
                            $('#privigrpeditdivwrap .loadingdiv').removeClass('loading');
                        });
                        $('#SelectedEditPriviGrpApp').val(appid);
                    }
                });

                return res;
            };

            if ($('#privigrpeditdivwrap').css('display') == 'none') {
                $('#privigrpeditdivwrap').slideDown(function () {
                    getAppPrivis();
                });
            }
            else {
                $('#privigrpeditdiv').fadeOut(function () {
                    getAppPrivis();
                });
            }
        }

        // Assign role to user
        else {
            // Get user roles
            var oprid = $("#cmbSelectUser").val();
            var appid = $(this).val();
            $.ajax({
                type: "POST",
                url: "/Home/GetOperatorRoles",
                data: { oprid: oprid, appid: appid },
                dataType: "json",
                error: function (result) {
                    $('#cmbselectusersload').removeClass("loading");
                    showPopup("Unable to access server");
                },
                success: function (result) {
                    $('#cmbselectusersload').removeClass("loading");
                    $('#assignrolediv').html(result.page).slideDown();

                    // Also set the current user id we are working with
                    $('#SelectedUserId').val(oprid);
                }
            });
        }
    });

    // Show privileges div and scroll to it on click
    $(document).on('click', '.privibtn', function () {
        var div = $(this).parent().parent().find(".roleprividiv");
        if (div.css('display') == 'none') {
            $(this).siblings('.loadingdiv').addClass("loading");
            $(this).parent().parent().find(".hiddenclickgetrole").click();

            // Scroll to element
            $('html, body').animate({
                scrollTop: $(this).offset().top
            }, 700);
        }
        else
            div.slideUp();
    });

    // Clear array on execution/call to server
    $(document).on('click', '.transferbtn, .transferremovebtn', function (e) {
        if ($(this).hasClass('unfocused')) {
            e.preventDefault();
        }
        else {
            selectedprivisadd = {};
            selectedprivisrem = {};

            // Show loading icon
            $(this).closest('.spacetop').find('.loadingdiv:first').addClass('loading');
        }
    });

    // Code to handle privilege movement data 
    $(document).on('click', '.prividiv, .privigrp', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');

            // Left side moving to right
            if ($(this).attr('id').indexOf('addprivis') > -1) {
                // remove from selected array
                var roleid = $(this).parent().attr('id').replace('unselpriviwrap', '');
                if (selectedprivisadd[roleid] === undefined)
                    selectedprivisadd[roleid] = [];
               
                var index = selectedprivisadd[roleid].indexOf($(this).attr('id').replace('addprivis', ''));
                if (index > -1) {
                    selectedprivisadd[roleid].splice(index, 1);

                    // if no more selected, disable button
                    var parent = $(this).closest('.transferboxes');
                    if (selectedprivisadd[roleid].length == 0)
                        parent.find('.transferbtn').addClass('unfocused');

                    // update the variable we are using for posting
                    parent.find('#AddPrivileges' + roleid).val(selectedprivisadd[roleid].toString());
                    parent.find('#AddPrivilegeGroups' + roleid).val('');
                }
            }

            if ($(this).attr('id').indexOf('addprivigrp') > -1) {
                // remove from selected array
                var roleid = $(this).parent().attr('id').replace('unselprivigrpwrap', '');
                if (selectedprivigrpsadd[roleid] === undefined)
                    selectedprivigrpsadd[roleid] = [];

                var index = selectedprivigrpsadd[roleid].indexOf($(this).attr('id').replace('addprivigrp', ''));
                if (index > -1) {
                    selectedprivigrpsadd[roleid].splice(index, 1);

                    // if no more selected, disable button
                    var parent = $(this).closest('.transferboxes');
                    if (selectedprivigrpsadd[roleid].length == 0)
                        parent.find('.transferbtn').addClass('unfocused');

                    // update the variable we are using for posting
                    parent.find('#AddPrivilegeGroups' + roleid).val(selectedprivigrpsadd[roleid].toString());
                    parent.find('#AddPrivileges' + roleid).val('');
                }
            }

            // Right side moving to left
            if ($(this).attr('id').indexOf('removeprivis') > -1) {
                // remove from selected array
                var roleid = $(this).parent().attr('id').replace('priviwrap', '');
                if (selectedprivisrem[roleid] === undefined)
                    selectedprivisrem[roleid] = [];

                var index = selectedprivisrem[roleid].indexOf($(this).attr('id').replace('removeprivis', ''));
                if (index > -1) {
                    selectedprivisrem[roleid].splice(index, 1);

                    // if no more selected, disable button
                    var parent = $(this).closest('.transferboxes');
                    if (selectedprivisrem[roleid].length == 0)
                        parent.find('.transferremovebtn').addClass('unfocused');

                    // update the variable we are using for posting
                    parent.find('#RemovePrivileges' + roleid).val(selectedprivisrem[roleid].toString());
                    parent.find('#RemovePrivilegeGroups' + roleid).val('');
                }
            }

            if ($(this).attr('id').indexOf('removeprivigrp') > -1) {
                // remove from selected array
                var roleid = $(this).parent().attr('id').replace('privigrpwrap', '');
                if (selectedprivigrpsrem[roleid] === undefined)
                    selectedprivigrpsrem[roleid] = [];

                var index = selectedprivigrpsrem[roleid].indexOf($(this).attr('id').replace('removeprivigrp', ''));
                if (index > -1) {
                    selectedprivigrpsrem[roleid].splice(index, 1);

                    // if no more selected, disable button
                    var parent = $(this).closest('.transferboxes');
                    if (selectedprivigrpsrem[roleid].length == 0)
                        parent.find('.transferremovebtn').addClass('unfocused');

                    // update the variable we are using for posting
                    parent.find('#RemovePrivilegeGroups' + roleid).val(selectedprivigrpsrem[roleid].toString());
                    parent.find('#RemovePrivileges' + roleid).val('');
                }
            }
        }
        else {
            $(this).addClass('selected');

            // Left side moving to right
            if ($(this).attr('id').indexOf('addprivis') > -1) {
                // Add to selected values array
                var roleid = $(this).parent().attr('id').replace('unselpriviwrap', '');
                if (selectedprivisadd[roleid] === undefined)
                    selectedprivisadd[roleid] = [];
               
                selectedprivisadd[roleid].push($(this).attr('id').replace('addprivis', ''));
                var parent = $(this).closest('.transferboxes');
                parent.find('.transferbtn').removeClass('unfocused');

                // update the variable we are using for posting
                parent.find('#AddPrivileges' + roleid).val(selectedprivisadd[roleid].toString());
                parent.find('#AddPrivilegeGroups' + roleid).val('');
            }

            if ($(this).attr('id').indexOf('addprivigrp') > -1) {
                // Add to selected values array
                var roleid = $(this).parent().attr('id').replace('unselprivigrpwrap', '');
                if (selectedprivigrpsadd[roleid] === undefined)
                    selectedprivigrpsadd[roleid] = [];             

                selectedprivigrpsadd[roleid].push($(this).attr('id').replace('addprivigrp', ''));
                var parent = $(this).closest('.transferboxes');
                parent.find('.transferbtn').removeClass('unfocused');

                // update the variable we are using for posting
                parent.find('#AddPrivilegeGroups' + roleid).val(selectedprivigrpsadd[roleid].toString());
                parent.find('#AddPrivileges' + roleid).val('');
            }

            // Right side moving to left
            if ($(this).attr('id').indexOf('removeprivis') > -1) {
                // Add to selected values array
                var roleid = $(this).parent().attr('id').replace('priviwrap', '');
                if (selectedprivisrem[roleid] === undefined)
                    selectedprivisrem[roleid] = [];

                selectedprivisrem[roleid].push($(this).attr('id').replace('removeprivis', ''));
                var parent = $(this).closest('.transferboxes');
                parent.find('.transferremovebtn').removeClass('unfocused');

                // update the variable we are using for posting
                parent.find('#RemovePrivileges' + roleid).val(selectedprivisrem[roleid].toString());
                parent.find('#RemovePrivilegeGroups' + roleid).val('');
            }

            if ($(this).attr('id').indexOf('removeprivigrp') > -1) {
                // Add to selected values array
                var roleid = $(this).parent().attr('id').replace('privigrpwrap', '');
                if (selectedprivigrpsrem[roleid] === undefined)
                    selectedprivigrpsrem[roleid] = [];

                selectedprivigrpsrem[roleid].push($(this).attr('id').replace('removeprivigrp', ''));
                var parent = $(this).closest('.transferboxes');
                parent.find('.transferremovebtn').removeClass('unfocused');

                // update the variable we are using for posting
                parent.find('#RemovePrivilegeGroups' + roleid).val(selectedprivigrpsrem[roleid].toString());
                parent.find('#RemovePrivileges' + roleid).val('');
            }
        }
    });

    // If you click on one side, deactivate the other
    $(document).on('click', '.leftsidebox .prividiv', function () {
        $(this).closest('.transferboxes').find('.rightsidebox .prividiv').removeClass('selected');
        $(this).closest('.transferboxes').find('.rightsidebox .privigrp').removeClass('selected');
        $(this).closest('.transferboxes').find('.leftsidebox .privigrp').removeClass('selected');

        // reset array of selected values on right side
        selectedprivisrem = {};
        selectedprivigrpsrem = {};
        selectedprivigrpsadd = {};
        // disable remove button and activate add button
        $(this).closest('.transferboxes').find('.transferremovebtn').addClass('unfocused');
        $(this).closest('.transferboxes').find('.transferbtn').removeClass('unfocused');
    });

    $(document).on('click', '.leftsidebox .privigrp', function () {
        $(this).closest('.transferboxes').find('.rightsidebox .prividiv').removeClass('selected');
        $(this).closest('.transferboxes').find('.rightsidebox .privigrp').removeClass('selected');
        $(this).closest('.transferboxes').find('.leftsidebox .prividiv').removeClass('selected');
        selectedprivisrem = {};
        selectedprivigrpsrem = {};
        selectedprivisadd = {};
        $(this).closest('.transferboxes').find('.transferremovebtn').addClass('unfocused');
        $(this).closest('.transferboxes').find('.transferbtn').removeClass('unfocused');
    });

    $(document).on('click', '.rightsidebox .prividiv', function () {
        $(this).closest('.transferboxes').find('.leftsidebox .prividiv').removeClass('selected');
        $(this).closest('.transferboxes').find('.leftsidebox .privigrp').removeClass('selected');
        $(this).closest('.transferboxes').find('.rightsidebox .privigrp').removeClass('selected');
        selectedprivisadd = {};
        selectedprivigrpsadd = {};
        selectedprivigrpsrem = {};
        $(this).closest('.transferboxes').find('.transferremovebtn').removeClass('unfocused');
        $(this).closest('.transferboxes').find('.transferbtn').addClass('unfocused');
    });

    $(document).on('click', '.rightsidebox .privigrp', function () {
        $(this).closest('.transferboxes').find('.rightsidebox .prividiv').removeClass('selected');
        $(this).closest('.transferboxes').find('.leftsidebox .privigrp').removeClass('selected');
        $(this).closest('.transferboxes').find('.leftsidebox .prividiv').removeClass('selected');
        selectedprivisadd = {};
        selectedprivigrpsadd = {};
        selectedprivisrem = {};
        $(this).closest('.transferboxes').find('.transferremovebtn').removeClass('unfocused');
        $(this).closest('.transferboxes').find('.transferbtn').addClass('unfocused');
    });
})