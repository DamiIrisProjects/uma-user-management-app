﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UMA.Startup))]
namespace UMA
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
