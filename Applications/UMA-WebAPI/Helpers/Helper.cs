﻿using Microsoft.Owin.Security;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using UMAWebAPI.Models;

namespace UMAWebAPI.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToFirstLetterUpper(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var words = input.ToLower().Split(' ');
                var result = string.Empty;

                foreach (var word in words)
                {
                    if (word != string.Empty && word != " ")
                    {
                        var a = word.ToCharArray();
                        a[0] = char.ToUpper(a[0]);

                        if (result == string.Empty)
                            result = result + new string(a);
                        else
                            result = result + " " + new string(a);
                    }
                }

                return result;
            }

            return string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        public static Dictionary<UserValidator, AuthenticationTicket> Tickets { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rolelist"></param>
        /// <returns></returns>
        public static List<Role> ConvertToApiRoles(List<UMAentities.Role> rolelist)
        {
            var result = new List<Role>();

            foreach (var role in rolelist)
            {
                var r = new Role()
                {
                    ApplicationId = role.ApplicationId,
                    Id = role.Id,
                    DateCreated = role.DateCreated.ToString("dd-MM-yyyy hh:mm tt"),
                    Name = role.Name,
                    OwnerId = role.OwnerId,
                    OwnerName = role.OwnerName,
                    Privileges = ConvertToApiPrivileges(role.Privileges)
                };

                result.Add(r);
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="privilist"></param>
        /// <returns></returns>
        public static List<Privilege> ConvertToApiPrivileges(List<UMAentities.Privilege> privilist)
        {
            var result = new List<Privilege>();

            if (privilist != null)
            {
                foreach (var privi in privilist)
                {
                    var p = new Privilege()
                    {
                        ApplicationId = privi.ApplicationId,
                        Id = privi.Id,
                        DateCreated = privi.DateCreated,
                        Name = privi.Name,
                        OwnerId = privi.OwnerId,
                        OwnerName = privi.OwnerName,
                        RoleId = privi.RoleId,
                        Actions = privi.Actions
                    };

                    result.Add(p);
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rolelist"></param>
        /// <returns></returns>
        public static List<JRole> ConvertToJRoles(List<UMAentities.Role> rolelist)
        {
            var result = new List<JRole>();

            foreach (var role in rolelist)
            {
                var r = new JRole()
                {
                    ApplicationId = role.ApplicationId,
                    Id = role.Id,
                    Name = role.Name,
                    Privileges = ConvertToJPrivileges(role.Privileges)
                };

                result.Add(r);
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="privilist"></param>
        /// <returns></returns>
        public static List<JPrivilege> ConvertToJPrivileges(List<UMAentities.Privilege> privilist)
        {
            var result = new List<JPrivilege>();

            if (privilist != null)
            {
                foreach (var privi in privilist)
                {
                    var p = new JPrivilege()
                    {
                        Id = privi.Id,
                        Name = privi.Name,
                    };

                    result.Add(p);
                }
            }

            return result;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class GenericPrincipalExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string FullName(this IPrincipal user)
        {
            return Helper.ToFirstLetterUpper(GetClaim(user, "Fullname"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="claimsearch"></param>
        /// <returns></returns>
        public static string GetClaim(IPrincipal user, string claimsearch)
        {
            var result = "";

            if (user.Identity.IsAuthenticated)
            {
                var claimsIdentity = user.Identity as ClaimsIdentity;
                if (claimsIdentity != null)
                    foreach (var claim in claimsIdentity.Claims)
                    {
                        if (claim.Type == claimsearch)
                            result = claim.Value;
                    }
            }

            return result;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ApiAuthorizeAttribute : System.Web.Http.AuthorizeAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        public string AccessLevel { get; set; }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var identity = Thread.CurrentPrincipal.Identity;

            if (identity == null && HttpContext.Current != null)
            {
                identity = HttpContext.Current.User.Identity;
            }

            if (identity != null && identity.IsAuthenticated)
            {
                var id = (ClaimsPrincipal)Thread.CurrentPrincipal;
                var optype = id.Claims.FirstOrDefault(c => c.Type == "OperatorType");
                if (optype != null)
                {
                    var result = optype.Value;
                    if (!AccessLevel.Equals(result))
                        return false;
                }

                return true;
            }

            return false;
        }
    }

}
