﻿using AspNet.Identity.Oracle;
// Add to use Identity:
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
// Add Usings:
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using UMA_Proxy;
using UMAentities;
using Helper = UMAWebAPI.Helpers.Helper;

namespace UMAWebAPI.OAuthServerProvider
{
    /// <summary>
    /// 
    /// </summary>
    public class ApplicationOAuthServerProvider
        : OAuthAuthorizationServerProvider
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task ValidateClientAuthentication(
            OAuthValidateClientAuthenticationContext context)
        {
            // This call is required...
            // but we're not using client authentication, so validate and move on...
            await Task.FromResult(context.Validated());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task GrantResourceOwnerCredentials(
            OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                // ** Use extension method to get a reference to the user manager from the Owin Context:
                var manager = context.OwinContext.GetUserManager<ApplicationUserManager>();
                var rolemanager = new ApplicationRoleManager(new RoleStore<IdentityRole>(context.OwinContext.Get<Models.ApplicationDbContext>()));

                // UserManager allows us to retrieve use with name/password combo:
                var user = await manager.FindAsync(context.UserName, context.Password);
                if (user == null)
                {
                    context.SetError(
                        "invalid_grant", "The user name or password is incorrect.");
                    context.Rejected();
                    return;
                }

                // Add claims associated with this user to the ClaimsIdentity object:
                var userIdentity = await user.GenerateUserIdentityAsync(manager);
                userIdentity.AddClaim(new Claim("Fullname", user.FullName));
                userIdentity.AddClaim(new Claim("Email", user.Email));
                //userIdentity.AddClaim(new Claim("Token", ));
                userIdentity.AddClaim(new Claim("Id", user.Id));
                if (!string.IsNullOrEmpty(user.Branch))
                    userIdentity.AddClaim(new Claim("Branch", user.Branch));

                // set operator type as well
                var oprtype = 0; var oprtypestr = "";
                var rolesForUser = manager.GetRoles(user.Id);
                var role1 = rolemanager.FindById("1");
                var role2 = rolemanager.FindById("2");
                var role3 = rolemanager.FindById("3");

                for (int i = 0; i < rolesForUser.Count; i++)
                {
                    if (rolesForUser.Contains(role1.Name))
                    {
                        userIdentity.AddClaim(new Claim("OperatorType", "1"));
                        oprtype = 1; oprtypestr = Helper.ToFirstLetterUpper(role1.Name);
                    }
                    if (rolesForUser.Contains(role2.Name))
                    {
                        userIdentity.AddClaim(new Claim("OperatorType", "2"));
                        oprtype = 2; oprtypestr = Helper.ToFirstLetterUpper(role2.Name);
                    }
                    if (rolesForUser.Contains(role3.Name))
                    {
                        userIdentity.AddClaim(new Claim("OperatorType", "3"));
                        oprtype = 3; oprtypestr = Helper.ToFirstLetterUpper(role3.Name);
                    }
                }

                // Send login information back to client
                var props = new AuthenticationProperties(new Dictionary<string, string>
                {
                    { "UserId", user.Id }, 
                    { "Firstname", user.FirstName.ToFirstLetterUpper() }, 
                    { "Surname", user.Surname.ToFirstLetterUpper() }, 
                    { "Name", user.FullName }, 
                    { "Email", user.Email }, 
                    { "RoleTypeId" , oprtype.ToString() },
                    { "RoleTypeName" , oprtypestr },
                    { "Branch" , user.Branch }
                });

                var roles = new OperatorProxy().OperatorChannel.GetOperatorRoles(new Operator() { OperatorId = int.Parse(user.Id) });

                // Convert role to api form to remove clutter
                var apiroles = Helper.ConvertToJRoles(roles);

                // Convert to json format
                var jsonroles = new JavaScriptSerializer().Serialize(apiroles);

                // Add it to properties
                props.Dictionary.Add("Roles", jsonroles);

                var ticket = new AuthenticationTicket(userIdentity, props);

                // Add ticket to dictionary for future validation
                if (Helper.Tickets == null)
                    Helper.Tickets = new Dictionary<Models.UserValidator, AuthenticationTicket>();

                Helper.Tickets.Add(new Models.UserValidator() { Guid = Guid.NewGuid().ToString(), UserId = user.Id }, ticket);

                context.Validated(ticket);
            }
            catch (Exception)
            {
                context.Rejected();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task TokenEndpointResponse(OAuthTokenEndpointResponseContext context)
        {
            var accessToken = context.AccessToken;
            var ticket = Helper.Tickets.FirstOrDefault(x => x.Key.UserId == context.Identity.GetUserId());
            ticket.Key.Token = accessToken;

            return Task.FromResult<object>(null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (var property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
    }
}
