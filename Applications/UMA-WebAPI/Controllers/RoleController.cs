﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UMA_Proxy;
using UMAentities;
using UMAWebAPI.Helpers;
using Helper = UMAWebAPI.Helpers.Helper;

namespace UMAWebAPI.Controllers
{
    /// <summary>
    /// Creation, Removal and Updates of Roles
    /// </summary>
    public class RoleController : ApiController
    {
        #region Get

        /// <summary>
        /// Get all roles
        /// </summary>
        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                var result = new RoleProxy().RoleChannel.GetRoles();

                // Sort it by application
                result = result.OrderBy(o => o.ApplicationId).ToList();

                var returnObjects = result.Select(x => new
                {
                    x.Id,
                    x.Name,
                    Owner = x.OwnerName,
                    x.OwnerId,
                    AppId = x.ApplicationId
                });

                var httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK, new { Roles = returnObjects });
                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Get application roles
        /// </summary>
        /// <param name="id">The application's Id</param>
        [HttpGet]
        public HttpResponseMessage GetApplicationRoles(int id)
        {
            try
            {
                var result = new RoleProxy().RoleChannel.GetApplicationRoles(new Application() { Id = id });

                // Sort it by name
                result = result.OrderBy(o => o.Name).ToList();

                var returnObjects = result.Select(x => new
                {
                    x.Id,
                    x.Name,
                    Owner = x.OwnerName,
                    x.OwnerId,
                    AppId = x.ApplicationId
                });

                var httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK, new { Roles = returnObjects });
                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Get roles linked to a particular operator
        /// </summary>
        /// <param name="id">The operator's Id</param>
        /// <param name="id2">The application Id of where the roles should come from (if any)</param>
        [HttpGet]
        public HttpResponseMessage GetOperatorRoles(int id, int id2)
        {
            try
            {
                var result = new OperatorProxy().OperatorChannel.GetOperatorRoles(new Operator() { OperatorId = id, AppId = id2.ToString() });

                // Sort it by name
                result = result.OrderBy(o => o.Name).ToList();

                var returnObjects = result.Select(x => new
                {
                    x.Id,
                    x.Name,
                    Owner = x.OwnerName,
                    x.OwnerId,
                    AppId = x.ApplicationId
                });

                var httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK, new { Roles = returnObjects });
                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Get the types of roles
        /// </summary>
        [HttpGet]
        public HttpResponseMessage GetRoleTypes()
        {
            try
            {
                var result = new RoleProxy().RoleChannel.GetRoleTypes();

                var returnObjects = result.Select(x => new
                {
                    Id = x.RoleTypeId,
                    x.Name,
                    IsLinkedToApplication = x.IsApplicationLinked
                });

                var httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK, new { RoleTypes = returnObjects });
                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Get application roles by RoleType
        /// </summary>
        /// <param name="id">The roletype Id</param>
        [HttpGet]
        public HttpResponseMessage GetRolesByType(int id)
        {
            try
            {
                var result = new RoleProxy().RoleChannel.GetRolesByType(id);

                // Sort it by name
                result = result.OrderBy(o => o.Name).ToList();

                var returnObjects = result.Select(x => new
                {
                    x.Id,
                    x.Name,
                    Owner = x.OwnerName,
                    x.OwnerId,
                    AppId = x.ApplicationId
                });

                var httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK, new { Roles = returnObjects });
                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Get System roles - Not attached to any application
        /// </summary>
        [HttpGet]
        public HttpResponseMessage GetSystemRoles()
        {
            try
            {
                var result = new RoleProxy().RoleChannel.GetSystemRoles();

                // Sort it by name
                result = result.OrderBy(o => o.Name).ToList();

                var returnObjects = result.Select(x => new
                {
                    x.Id,
                    x.Name,
                    Owner = x.OwnerName,
                    x.OwnerId,
                    AppId = x.ApplicationId
                });

                var httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK, new { Roles = returnObjects });
                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Get role details
        /// </summary>
        /// <param name="id">The role's Id</param>
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var result = new RoleProxy().RoleChannel.GetRoleDetailsById(id);

                dynamic returnObject = new {result.Id, result.Name, Owner = result.OwnerName, result.OwnerId, AppId = result.ApplicationId };

                // Add our own version of roles
                var roles = Helper.ConvertToApiRoles(new List<Role>(){ result });

                returnObject.Roles = roles;

                var httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK, new { Role = returnObject });
                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        } 

        #endregion

        #region Post

        /// <summary>
        /// Add a new role
        /// </summary>
        /// <param name="role">The role to add</param>
        [HttpPost]
        public HttpResponseMessage Post(Models.Role role)
        {
            if (role == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Please give a role name" });
            }

            if (role.ApplicationId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Adding a role requires an application Id" });
            }

            try
            {
                var result = new RoleProxy().RoleChannel.AddRole(new Role() { Name = role.Name, ApplicationId = role.ApplicationId, OwnerId = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Id")) });

                if (result == 2)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "A role with this name already exists" });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Add a role to another role
        /// </summary>
        /// <param name="role">The role to add. Ensure the role Id of the other one is set before posting.</param>
        [HttpPost]
        public HttpResponseMessage AddRoleToRole(Models.Role role)
        {
            if (role == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Please give an role name" });

            if (role.ApplicationId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Adding a role requires an application Id" });

            if (role.RoleId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Invalid role Id" });

            try
            {
                new RoleProxy().RoleChannel.AddRolesToRole(new List<Role>
                {
                    new Role
                    {
                        Name = role.Name,
                        ApplicationId = role.ApplicationId,
                        AddToRole = role.RoleId,
                        OwnerId = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Id"))
                    }
                });

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Add a role operator
        /// </summary>
        /// <param name="role">The role to add - Ensure user id and role id properties are filled</param>
        [HttpPost]
        public HttpResponseMessage AddRoleToOperator(Models.Role role)
        {
            if (role == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Role is required" });

            if (role.UserId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Invalid user id" });

            if (role.RoleId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Invalid role id" });

            try
            {
                new RoleProxy().RoleChannel.AddRolesToOperator(new List<Role> {
                    new Role()
                    {
                        Id = role.Id,
                        OwnerId = role.UserId,
                        EditBy = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Id"))
                    } });
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Remove a role to a role
        /// </summary>
        /// <param name="role">The role to remove. Ensure the role Id is set before posting.</param>
        [HttpPost]
        public HttpResponseMessage RemoveRolesFromRole(Models.Role role)
        {
            if (role == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Please give an role name" });

            if (role.ApplicationId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Adding a role requires an application Id" });

            if (role.RoleId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Invalid role Id" });

            try
            {
                var result = new RoleProxy().RoleChannel.RemoveRolesFromRole(new List<Role>() { new Role() { Name = role.Name, ApplicationId = role.ApplicationId, AddToRole = role.RoleId, OwnerId = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Id")) } });

                if (result == 2)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "A role with this name already exists" });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }


        #endregion

        #region Put

        /// <summary>
        /// Update a role
        /// </summary>
        [HttpPut]
        public HttpResponseMessage Put(Models.Role role)
        {
            if (role == null || role.Id == 0 || role.ApplicationId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "invalid role" });
            }

            try
            {
                var ap = new Role() { EditBy = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Id")), Id = role.Id, Name = role.Name, ApplicationId = role.ApplicationId };

                var result = new RoleProxy().RoleChannel.UpdateRole(ap);

                if (result == 2)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "You do not have access to this role" });

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Could not find any role matching your request" });

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        
        #endregion

        #region Delete

        /// <summary>
        /// Delete a role
        /// </summary>
        /// <param name="id">The roles's Id</param>
        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            if (id == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Please give role id" });
            }

            try
            {
                var role = new Role() { Id = id, EditBy = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Id")) };
                var result = new RoleProxy().RoleChannel.DeleteRole(role);

                if (result == 2)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "You do not have access to this role" });

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Could not find any role matching your request" });

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }
        
        #endregion
    }
}
