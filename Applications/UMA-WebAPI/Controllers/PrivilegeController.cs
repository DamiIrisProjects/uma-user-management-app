﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UMA_Proxy;
using UMAentities;
using UMAWebAPI.Helpers;

namespace UMAWebAPI.Controllers
{
    /// <summary>
    /// Creation, Removal and Updates of Privileges
    /// </summary>
    [ApiAuthorize(AccessLevel = "1")]
    public class PrivilegeController : ApiController
    {
        /// <summary>
        /// Get all privileges
        /// </summary>
        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                var result = new PrivilegeProxy().PrivilegeChannel.GetPrivileges();
                
                // Sort it by application
                result = result.OrderBy(o => o.ApplicationId).ToList();

                var returnObjects = result.Select(x => new
                {
                    x.Id,
                    x.Name,
                    Owner = x.OwnerName,
                    x.OwnerId,
                    AppId = x.ApplicationId
                });

                var httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK, new { Privileges = returnObjects });
                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Get application privileges
        /// </summary>
        /// <param name="id">The application's Id</param>
        [HttpGet]
        public HttpResponseMessage GetByApplication(int id)
        {
            try
            {
                var result = new PrivilegeProxy().PrivilegeChannel.GetApplicationPrivileges(id);

                // Sort it by name
                result = result.OrderBy(o => o.Name).ToList();

                var returnObjects = result.Select(x => new
                {
                    x.Id,
                    x.Name,
                    Owner = x.OwnerName,
                    x.OwnerId,
                    AppId = x.ApplicationId
                });

                var httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK, new { Privileges = returnObjects });
                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Get role privileges
        /// </summary>
        /// <param name="id">The role's Id</param>
        [HttpGet]
        public HttpResponseMessage GetByRole(int id)
        {
            try
            {
                var result = new RoleProxy().RoleChannel.GetRolePrivileges(id);

                // Sort it by name
                result = result.OrderBy(o => o.Name).ToList();

                var returnObjects = result.Select(x => new
                {
                    x.Id,
                    x.Name,
                    Owner = x.OwnerName,
                    x.OwnerId,
                    AppId = x.ApplicationId
                });

                var httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK, new { Privileges = returnObjects });
                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Add a new privilege
        /// </summary>
        /// <param name="privi">The privilege to add</param>
        [HttpPost]
        public HttpResponseMessage Post(Models.Privilege privi)
        {
            if (privi == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Please give an privilege name" });
            }

            if (privi.ApplicationId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Adding a privilege requires an application Id" });
            }

            try
            {
                var result = new PrivilegeProxy().PrivilegeChannel.AddPrivilege(new Privilege() { Name = privi.Name, ApplicationId = privi.ApplicationId, OwnerId = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Id")) });

                if (result == 2)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "A privilege with this name already exists" });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Add a privilege to a role
        /// </summary>
        /// <param name="privi">The privilege to add. Ensure the role Id is set before posting.</param>
        [HttpPost]
        public HttpResponseMessage AddPrivilegeToRole(Models.Privilege privi)
        {
            if (privi == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Please give an privilege name" });

            if (privi.ApplicationId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Adding a privilege requires an application Id" });

            if (privi.RoleId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Invalid role Id" });

            try
            {
                var result = new PrivilegeProxy().PrivilegeChannel.AddPrivilegesToRole(new List<Privilege>() { new Privilege(){ Name = privi.Name, ApplicationId = privi.ApplicationId, RoleId = privi.RoleId, OwnerId = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Id"))} });

                if (result == 2)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "A privilege with this name already exists" });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Remove a privilege to a role
        /// </summary>
        /// <param name="privi">The privilege to remove. Ensure the role Id is set before posting.</param>
        [HttpPost]
        public HttpResponseMessage RemovePrivilegesFromRole(Models.Privilege privi)
        {
            if (privi == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Please give an privilege name" });

            if (privi.ApplicationId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Adding a privilege requires an application Id" });

            if (privi.RoleId == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Invalid role Id" });

            try
            {
                var result = new PrivilegeProxy().PrivilegeChannel.RemovePrivilegesFromRole(new List<Privilege>() { new Privilege() { Name = privi.Name, ApplicationId = privi.ApplicationId, RoleId = privi.RoleId, OwnerId = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Id")) } });

                if (result == 2)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "A privilege with this name already exists" });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Update a privilege
        /// </summary>
        [HttpPut]
        public HttpResponseMessage Put(Models.Privilege privi)
        {
            if (privi == null || privi.Id == 0 || privi.ApplicationId == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "invalid privilege" });
            }

            try
            {
                var ap = new Privilege() { EditBy = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Id")), Id = privi.Id, Name = privi.Name, ApplicationId = privi.ApplicationId };

                var result = new PrivilegeProxy().PrivilegeChannel.UpdatePrivilege(ap);

                if (result == 2)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "You do not have access to this privilege" });

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Could not find any privilege matching your request" });

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Delete a privilege
        /// </summary>
        /// <param name="id">The privileges's Id</param>
        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            if (id == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Please give privilege id" });
            }

            try
            {
                var privi = new Privilege() { Id = id, EditBy = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Id")) };
                var result = new PrivilegeProxy().PrivilegeChannel.DeletePrivilege(privi);

                if (result == 2)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "You do not have access to this privilege" });

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Could not find any privilege matching your request" });

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

    }
}
