﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using UMA_Proxy;
using UMAentities;
using UMAWebAPI.Helpers;
using Helper = UMAWebAPI.Helpers.Helper;

namespace UMAWebAPI.Controllers
{
    /// <summary>
    /// Creation, Removal and Updates of Operators/Users
    /// </summary>
    [ApiAuthorizeAttribute(AccessLevel = "1")]
    public class OperatorController : ApiController
    {
        /// <summary>
        /// Get all operators
        /// </summary>
        [ActionName("Get")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                var result = new OperatorProxy().OperatorChannel.GetAllUsers();

                foreach (var opr in result)
                {
                    if (opr.Roles != null)
                    {
                        // Convert role to api form to remove clutter
                        var apiroles = Helper.ConvertToApiRoles(opr.Roles);

                        // Convert to json format
                        opr.RolesJson = new JavaScriptSerializer().Serialize(apiroles);
                    }
                }

                var returnObjects = result.Select(x => new
                {
                    UserId = x.OperatorId,
                    Name = x.FullName,
                    UserTypeId = x.OperatorType,
                    UserType = x.OperatorTypeName,
                    Roles = x.RolesJson
                });

                var httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK, new { Operators = returnObjects });
                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Get a particular operator
        /// </summary>
        /// <param name="id">The operators's Id</param>
        [ActionName("Get")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var result = new OperatorProxy().OperatorChannel.GetOperatorDetails(id);

                if (result.Roles != null)
                {
                    // Convert role to api form to remove clutter
                    var apiroles = Helper.ConvertToApiRoles(result.Roles);

                    // Convert to json format
                    result.RolesJson = new JavaScriptSerializer().Serialize(apiroles);
                }

                dynamic returnObject = new
                {
                    UserId = result.OperatorId,
                    Name = result.FullName,
                    UserTypeId = result.OperatorType,
                    UserType = result.OperatorTypeName,
                    Roles = result.RolesJson
                };

                return Request.CreateResponse(HttpStatusCode.OK, new { Operator = returnObject });
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Create an operator external login token
        /// </summary>
        [ActionName("ExtToken")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage CreateOperatorToken()
        {
            try
            {
                var id = GenericPrincipalExtensions.GetClaim(User, "Id");
                var val = Helper.Tickets.Keys.FirstOrDefault(x => x.UserId == id);

                if (val == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Invalid code" });
                
                // Otherwise, return the original ticket for the application
                var response = Request.CreateResponse(HttpStatusCode.OK, new { token = val.Guid });
                                
                return response;
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = "Invalid credentials" });
            }
        }

        /// <summary>
        /// Get a logged in operator via token
        /// </summary>
        /// <param name="id">Token</param>
        [AllowAnonymous]
        [ActionName("ExtToken")]
        [HttpGet]
        public HttpResponseMessage GetOperatorToken(string id)
        {
            try
            {
                var val = Helper.Tickets.Keys.FirstOrDefault(x => x.Guid == id.ToString());

                // If it doesn't exist, send invalid code message
                if (val == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Invalid code" });

                // Change the code for next time
                val.Guid = Guid.NewGuid().ToString();
                
                // Return the original ticket for the application after setting access token
                if (Helper.Tickets[val].Properties.Dictionary.ContainsKey("access_token"))
                    Helper.Tickets[val].Properties.Dictionary["access_token"] = val.Token;
                else
                    Helper.Tickets[val].Properties.Dictionary.Add("access_token", val.Token);

                return Request.CreateResponse(HttpStatusCode.OK, new { ticket = new JavaScriptSerializer().Serialize(Helper.Tickets[val].Properties.Dictionary) });
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = "Invalid code" });
            }
        }


        /// <summary>
        /// Add a new operator
        /// </summary>
        [HttpPost]
        public HttpResponseMessage Post(Models.Operator app)
        {
            if (app == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Please give an operator name" });
            }

            try
            {
                var result = 1;// new OperatorProxy().OperatorChannel.AddOperator(new Operator() { Name = app.Name, OwnerId = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Id")) });

                if (result == 2)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "An operator with this name already exists" });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Update an operator
        /// </summary>
        [HttpPut]
        public HttpResponseMessage Put(Models.Operator app)
        {
            if (app == null || app.Id == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "invalid operator" });
            }

            try
            {
                var ap = new Operator() { EditBy = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Id")), OperatorId = app.Id, FirstName = app.Name };
                var result = new OperatorProxy().OperatorChannel.UpdateOperator(ap);

                if (result == 2)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "You do not have access to this operator" });

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Could not find any operator matching your request" });

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Delete an operator
        /// </summary>
        /// <param name="id">The operators's Id</param>
        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            if (id == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Please give operator id" });
            }

            try
            {
                //var app = new Operator() { OperatorId = id, EditBy = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Id")) };
                //int result = new OperatorProxy().OperatorChannel.DeleteOperator(app);

                //if (result == 2)
                //    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "You do not have access to this operator" });

                //if (result == 0)
                //    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Could not find any operator matching your request" });

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        } 
    }
}
