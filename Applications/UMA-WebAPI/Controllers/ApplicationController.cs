﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UMA_Proxy;
using UMAentities;
using UMAWebAPI.Helpers;

namespace UMAWebAPI.Controllers
{
    /// <summary>
    /// Creation, Removal and Updates of Applications
    /// </summary>
    //[ApiAuthorizeAttribute(AccessLevel = "1")]
    public class ApplicationController : ApiController
    {
        /// <summary>
        /// Get all applications
        /// </summary>
        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {

                var result = new ApplicationProxy().ApplicationChannel.GetApplications();
                var returnObjects = result.Select(x => new
                {
                    x.Id,
                    x.Name,
                    Owner = x.OwnerName,
                    x.Icon,
                    x.OwnerId,
                    x.Port
                });

                var httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK, new { Applications = returnObjects });
                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Get a particular application
        /// </summary>
        /// <param name="id">The applications's Id</param>
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var result = new ApplicationProxy().ApplicationChannel.GetApplication(id);
                dynamic returnObject = new {result.Id, result.Name, Owner = result.OwnerName, result.OwnerId };

                return Request.CreateResponse(HttpStatusCode.OK, new { Applications = returnObject });
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Get a particular application by name
        /// </summary>
        /// <param name="id">The applications's Name</param>
        [HttpGet]
        public HttpResponseMessage GetByName(string id)
        {
            try
            {
                var result = new ApplicationProxy().ApplicationChannel.GetApplicationByName(id);
                dynamic returnObject = new {result.Id, result.Name, Owner = result.OwnerName, result.OwnerId };

                return Request.CreateResponse(HttpStatusCode.OK, new { Applications = returnObject });
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Add a new application
        /// </summary>
        [HttpPost]
        public HttpResponseMessage Post(Models.Application app)
        {
            if (app == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Please give an application name" });
            }

            try
            {
                var result = new ApplicationProxy().ApplicationChannel.AddApplication(new Application() { Name = app.Name, OwnerId = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Id")) });

                if (result == 2)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "An application with this name already exists" });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Update an application
        /// </summary>
        [HttpPut]
        public HttpResponseMessage Put(Models.Application app)
        {
            if (app == null || app.Id == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "invalid application" });
            }

            try
            {
                var ap = new Application() { EditBy = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Id")), Id = app.Id, Name = app.Name };
                var result = new ApplicationProxy().ApplicationChannel.UpdateApplication(ap);

                if (result == 2)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "You do not have access to this application" });

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Could not find any application matching your request" });

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }

        /// <summary>
        /// Delete an application
        /// </summary>
        /// <param name="id">The applications's Id</param>
        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            if (id == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Please give application id" });
            }

            try
            {
                var app = new Application() { Id = id, EditBy = int.Parse(GenericPrincipalExtensions.GetClaim(User, "Id")) };
                var result = new ApplicationProxy().ApplicationChannel.DeleteApplication(app);

                if (result == 2)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "You do not have access to this application" });

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "Could not find any application matching your request" });

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = ex.Message });
            }
        }
    }
}