﻿using System;
using System.Collections.Generic;

namespace UMAWebAPI.Models
{
    /// <summary>
    /// A privilege/permission
    /// </summary>
    public class Privilege
    {
        /// <summary>
        /// Privilege Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Privilege name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Date it was created
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Id of who created it
        /// </summary>
        public int OwnerId { get; set; }

        /// <summary>
        /// Name of who created it
        /// </summary>
        public string OwnerName { get; set; }

        /// <summary>
        /// Id of application privilege is linked to
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Id of role linked to privilege
        /// </summary>
        public int RoleId { get; set; }

        /// <summary>
        /// List of actions linked to privilege
        /// </summary>
        public List<UMAentities.Action> Actions { get; set; }
    }
}