﻿using System;
using System.Collections.Generic;

namespace UMAWebAPI.Models
{
    /// <summary>
    /// A User
    /// </summary>
    public class Operator
    {
        /// <summary>
        /// User Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Operator name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Date it was created
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Type of user
        /// </summary>
        public int UserType { get; set; }

        /// <summary>
        /// Name of who created it
        /// </summary>
        public string OwnerName { get; set; }

        /// <summary>
        /// Id of application operator is linked to
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Id of role linked to operator
        /// </summary>
        public int RoleId { get; set; }

        /// <summary>
        /// List of actions linked to operator
        /// </summary>
        public List<UMAentities.Action> Actions { get; set; }
    }
}