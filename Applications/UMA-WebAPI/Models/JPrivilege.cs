﻿namespace UMAWebAPI.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class JPrivilege
    {
        /// <summary>
        /// Id of role
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of role
        /// </summary>
        public string Name { get; set; }
    }
}