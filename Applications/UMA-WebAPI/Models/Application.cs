﻿using System;

namespace UMAWebAPI.Models
{
    /// <summary>
    /// </summary>
    public class Application
    {
        /// <summary>
        /// Id of application
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of application
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Date application was created
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Id of who created this application
        /// </summary>
        public int OwnerId { get; set; }

        /// <summary>
        /// Namem of who created this application
        /// </summary>
        public string OwnerName { get; set; }
    }
}