﻿using System;

namespace UMAWebAPI.Models
{
    /// <summary>
    /// MVC Actions
    /// </summary>
    public class Action
    {
        /// <summary>
        /// Action Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Action name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Date it was created
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Id of who created it
        /// </summary>
        public int OwnerId { get; set; }

        /// <summary>
        /// Name of who created it
        /// </summary>
        public string OwnerName { get; set; }

        /// <summary>
        /// Id of privilege action is linked to
        /// </summary>
        public int PrivilegeId { get; set; }
    }
}