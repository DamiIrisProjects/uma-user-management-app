﻿using System.Collections.Generic;

namespace UMAWebAPI.Models
{
    /// <summary>
    /// </summary>
    public class JRole
    {
        /// <summary>
        /// Id of role
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of role
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Application linked to role
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// List of privileges linked to role
        /// </summary>
        public List<JPrivilege> Privileges { get; set; }
    }
}