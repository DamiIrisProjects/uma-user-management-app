﻿using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
// Add the following usings:
using Owin;
using System;
using System.Web.Http;
using UMAWebAPI.Models;
using UMAWebAPI.OAuthServerProvider;

[assembly: OwinStartup(typeof(UMAWebAPI.Startup))]
namespace UMAWebAPI
{
    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {
        // This method is required by Katana:
        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            var webApiConfiguration = ConfigureWebApi();
            app.UseWebApi(webApiConfiguration);
        }


        private void ConfigureAuth(IAppBuilder app)
        {
            // Create per OWIN Context:
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            var oAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthServerProvider(),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(15),

                // Only do this for demo!!
                AllowInsecureHttp = true
            };
            app.UseOAuthAuthorizationServer(oAuthOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }

        private HttpConfiguration ConfigureWebApi()
        {
            var config = new HttpConfiguration();

            config.Routes.MapHttpRoute(
                name: "ControllerOnly",
                routeTemplate: "api/{controller}"
            );

            config.Routes.MapHttpRoute(
                name: "ControllerAndId",
                routeTemplate: "api/{controller}/{id}",
                defaults: null,
                constraints: new { id = @"^\d+$" } // Only integers 
            );

            config.Routes.MapHttpRoute(
                name: "ControllerAndAction",
                routeTemplate: "api/{controller}/{action}"
            );

            config.Routes.MapHttpRoute(
                name: "ControllerActionAndId",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: null
            );

            return config;
        }
    }
}
