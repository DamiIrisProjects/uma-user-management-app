﻿using System.Collections.Generic;
using System.ServiceModel;
using UMAentities;

namespace UMA_Contract
{
    [ServiceContract]
    public interface IPrivilege
    {
        [OperationContract]
        List<Privilege> GetPrivileges();
        
        [OperationContract]
        List<Privilege> GetApplicationPrivileges(int appid);

        [OperationContract]
        int AddPrivilege(Privilege privi);

        [OperationContract]
        int AddPrivilegesToRole(List<Privilege> privileges);

        [OperationContract]
        int RemovePrivilegesFromRole(List<Privilege> privileges);
       
        [OperationContract]
        int AddPrivilegesToGroup(Role grp);

        [OperationContract]
        int DeletePrivilege(Privilege privi);

        [OperationContract]
        int RemovePrivilegesFromGroup(Role grp);
       
        [OperationContract]
        int UpdatePrivilege(Privilege privi);
    }
}
