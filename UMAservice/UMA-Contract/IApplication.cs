﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using UMAentities;

namespace UMA_Contract
{
    [ServiceContract]
    public interface IApplication
    {
        [OperationContract]
        [WebGet]
        List<Application> GetApplications();

        [OperationContract]
        [WebGet(UriTemplate = "Applications/{id}")]
        Application GetApplicationById(string id);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string GetApplicationsJson();

        [OperationContract]
        Application GetApplication(int appid);

        [OperationContract]
        Application GetApplicationByName(string id);

        [OperationContract]
        int AddApplication(Application app);

        [OperationContract]
        int DeleteApplication(Application app);

        [OperationContract]
        int UpdateApplication(Application appname);

        [OperationContract]
        List<Operator> GetSharedAppUsers(Application app);

        [OperationContract]
        List<Operator> GetUmaUsers(Application app);

        [OperationContract]
        List<Operator> SaveSharedAppUsers(Application app);

        [OperationContract]
        List<Operator> RemoveShareApp(Application app);

        [OperationContract]
        List<Operator> GetBranchAdmins(Application app);

        [OperationContract]
        List<Operator> GetBranchUsers(Application app);

        [OperationContract]
        List<Operator> GetSuperUsers();
    }
}
