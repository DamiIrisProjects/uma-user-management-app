﻿using System.Collections.Generic;
using System.ServiceModel;
using UMAentities;

namespace UMA_Contract
{
    [ServiceContract]
    public interface IBranch
    {
        [OperationContract]
        List<Branch> GetBranches();

        [OperationContract]
        string GetBranchName(int branchid);
    }
}
