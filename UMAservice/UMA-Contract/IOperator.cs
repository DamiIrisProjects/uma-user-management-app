﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using UMAentities;

namespace UMA_Contract
{
    [ServiceContract]
    public interface IOperator
    {
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "Login/{username}/{password}")]
        string OperatorLogin(string username, string password); 

        [OperationContract]
        int RegisterOperator(Operator opr);

        [OperationContract]
        Operator GetOperatorDetails(int oprid);

        [OperationContract]
        List<Operator> GetAllUsers();

        [OperationContract]
        int UpdateOperator(Operator opr);

        [OperationContract]
        Operator LoginOperator(Operator opr);

        [OperationContract]
        int DeactivateUser(Operator opr);

        [OperationContract]
        List<Role> GetOperatorRoles(Operator opr);
    }
}
