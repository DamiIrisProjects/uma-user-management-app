﻿using System.Collections.Generic;
using System.ServiceModel;
using UMAentities;

namespace UMA_Contract
{
    [ServiceContract]
    public interface IInternalAction
    {
        [OperationContract]
        int AddAction(Action action);

        [OperationContract]
        int DeleteAction(Action action);

        [OperationContract]
        int UpdateAction(Action action);

        [OperationContract]
        Privilege GetPrivilegeActions(Action action);

        [OperationContract]
        List<Action> GetApplicationActions(int appid);

        [OperationContract]
        int AddActionsToPrivilege(List<Action> actions);

        [OperationContract]
        int RemoveActionsFromPrivilege(List<Action> actions);
    }
}
