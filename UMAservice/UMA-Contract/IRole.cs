﻿using System.Collections.Generic;
using System.ServiceModel;
using UMAentities;

namespace UMA_Contract
{
    [ServiceContract]
    public interface IRole
    {

        [OperationContract]
        List<Role> GetRoles();

        [OperationContract]
        List<Role> GetRolesByType(int id);

        [OperationContract]
        List<Role> GetApplicationRoles(Application app);

        [OperationContract]
        List<Role> GetSystemRoles();

        [OperationContract]
        List<RoleType> GetRoleTypes();

        [OperationContract]
        List<Privilege> GetRolePrivileges(int id);     

        [OperationContract]
        Role GetRoleDetails(Role role);

        [OperationContract]
        Role GetRoleDetailsById(int roleid);

        [OperationContract]
        int AddRolesToRole(List<Role> grps);

        [OperationContract]
        int AddRole(Role privi);

        [OperationContract]
        int AddRolesToOperator(List<Role> roles);

        [OperationContract]
        int RemoveRolesFromOperator(List<Role> roles);

        [OperationContract]
        int AddPrivilegesToRole(List<Privilege> privileges);

        [OperationContract]
        int RemovePrivilegesFromRole(List<Privilege> privileges);

        [OperationContract]
        int RemoveRolesFromRole(List<Role> grps);

        [OperationContract]
        int DeleteRole(Role privi);

        [OperationContract]
        int UpdateRole(Role privi);
    }
}
