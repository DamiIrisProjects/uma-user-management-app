﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using UMA_DataManager.Misc;
using UMAdata;
using UMAentities;
using Action = UMAentities.Action;

namespace UMA_DataManager
{
    public class DataManager : OracleDataProv
    {
        #region Registration and logins

        public int VerifyUsernameDoesNotExists(string email)
        {
            var dset = new DataSet();

            var query = "select email, is_active, registration_guid from operator where lower(email) = :email";

            OracleParameter[] param =
                    {   
                        new OracleParameter("email",  email.ToLower()),
                    };

            RunProcedure(query, param, dset);
            if (dset.Tables[0].Rows.Count != 0)
            {
                // User already exists. Now if the user hasn't been activated already, send them an activation email
                if (dset.Tables[0].Rows[0]["is_active"].ToString() == "0")
                {
                    //SendUserActivationMail(email, dset.Tables[0].Rows[0]["registration_guid"].ToString(), 1);
                    //return 2;
                    return 3;
                }
                else
                    return 3;

            }
            else
                return 0;
        }

        public int RegisterOperator(Operator opr)
        {
            // First verify user doesnt already exist
            var res = VerifyUsernameDoesNotExists(opr.EmailAddress);

            if (res == 0)
            {
                // Hash password
                var sh = SaltedHash.Create(opr.Password);

                var salt = sh.Salt;
                var hash = sh.Hash;
                var regGuid = Guid.NewGuid().ToString();

                var query = @"insert into aspnetuser (firstname, surname, email, password_hash, password_salt, registration_date, registration_guid, branch_id) values (:firstname, :surname, :email, :operator_type, :password_hash, :password_salt, :registration_date, :registration_guid, :branch_id)";
                OracleParameter[] param =
                {   
                    new OracleParameter("firstname",  opr.FirstName.ToUpper()),
                    new OracleParameter("surname",  opr.Surname.ToUpper()),
                    new OracleParameter("email",  opr.EmailAddress.ToUpper()),
                    new OracleParameter("operator_type",  opr.OperatorType),
                    new OracleParameter("password_hash",  hash),
                    new OracleParameter("password_salt",  salt),
                    new OracleParameter("registration_date",  DateTime.Now),
                    new OracleParameter("registration_guid",  regGuid),
                    new OracleParameter("branch_id",  opr.BranchId == 0 ? (object)DBNull.Value : opr.BranchId)
                };

                var result = ExecuteQuery(query, param);

                if (result != 0)
                {
                    AddAudit(new Audit() { UserId = 0, Description = "Registration email sent for " + opr.EmailAddress, EventType = (int)AuditEnum.RegistrationEmailSent });
                    //SendUserActivationMail(enrollee.EmailAddress, regGuid, 1);
                }

                return result;
            }

            return res;
        }

        public int ActivateUser(string guid)
        {
            
            var query = "update operator set is_active = 1 where registration_guid = :registration_guid";
            OracleParameter[] param =
                {   
                    new OracleParameter("registration_guid",  guid)
                };

            return ExecuteQuery(query, param);
        }

        public int DeactivateUser(Operator opr)
        {
            
            var query = "update aspnetusers set is_active = :iscancel where id = :id";
            OracleParameter[] param =
                {   
                    new OracleParameter("iscancel",  opr.IsCancel),
                    new OracleParameter("id",  opr.OperatorId),
                };

            ExecuteQuery(query, param);

            //Audit
            var audit = new Audit()
            {
                EventType = (int)AuditEnum.DeactivateUser,
                UserId = opr.OwnerId,
                Description = opr.IsCancel == 2 ? "Cancel invite" : "Deactivate"
            };

            AddAudit(audit);

            return 1;
        }

        public Operator LoginUser(Operator opr)
        {
            // Get hash and salt from database
            
            var query = @"select id, operator_type, firstname, surname, email, registration_date, password_salt, password_hash, o.is_active, o.emailconfirmed, last_activity, first_time_log, b.branch_id, b.branch_name from aspnetusers o
                            left join branch b on b.branch_id = o.branch 
                            where lower(email) = :email and o.is_active = 1";

            OracleParameter[] param = { new OracleParameter("email", opr.EmailAddress.ToLower()) };
            var dset = new DataSet();

            RunProcedure(query, param, dset);

            if (dset.Tables[0].Rows.Count != 0)
            {
                var row = dset.Tables[0].Rows[0];

                // Normal operators must say what application they are logging onto
                var oprType = int.Parse(row["operator_type"].ToString());
                
                // Otherwise carry on
                var salt = row["password_salt"].ToString();
                var hash = row["password_hash"].ToString();

                // Verify
                var sh = SaltedHash.Create(salt, hash);
                var value = sh.Verify(opr.Password);

                // Add audit trail
                var audit = new Audit()
                {
                    TimeStamp = DateTime.Now,
                    UserId = int.Parse(row["id"].ToString())
                };

                if (value)
                {
                    //set details
                    var user = new Operator
                    {
                        OperatorId = int.Parse(row["id"].ToString()),
                        OperatorType = oprType,
                        FirstTimeLog = int.Parse(row["first_time_log"].ToString()),
                        FirstName = row["firstname"].ToString(),
                        Surname = row["surname"].ToString()
                    };

                    if (!string.IsNullOrEmpty(row["branch"].ToString()))
                        user.BranchId = int.Parse(row["branch"].ToString());
                    user.BranchName = row["branch_name"].ToString();
                    user.EmailAddress = row["email"].ToString();
                    if (!string.IsNullOrEmpty(row["registration_date"].ToString()))
                        user.DateOfRegistration = DateTime.Parse(row["registration_date"].ToString());
                    if (!string.IsNullOrEmpty(row["last_activity"].ToString()))
                        user.LastActivityDate = DateTime.Parse(row["last_activity"].ToString());
                    user.IsActive = row["is_active"].ToString() == "1";
                    user.IsActivated = row["emailconfirmed"].ToString() == "1";
                    user.Salt = row["password_salt"].ToString();
                    user.Hash = row["password_hash"].ToString();

                    // If normal operator get roles
                    if (user.OperatorType == 3 || user.OperatorType == 2)
                    {
                        user.Roles = GetOperatorRoleDetails(user);
                    }

                    //Audit
                    audit.EventType = (int)AuditEnum.LoginSuccess;
                    audit.Description = user.EmailAddress;
                    AddAudit(audit);

                    // If its first time, update it so user doesn't have to see message again
                    if (user.FirstTimeLog == 1)
                    {
                        query = "update operator set first_time_log = 0 where userid = :userid";
                        OracleParameter[] param2 = { new OracleParameter("userid", user.OperatorId) };
                        ExecuteQuery(query, param2);
                    }

                    return user;
                }
                else
                {
                    //Audit
                    audit.EventType = (int)AuditEnum.LoginFailed;
                    audit.Description = opr.EmailAddress;
                    AddAudit(audit);
                    return null;
                }
            }
            else
            {
                //Audit
                var audit = new Audit()
                {
                    EventType = (int)AuditEnum.LoginFailed,
                    Description = opr.EmailAddress,
                    TimeStamp = DateTime.Now
                };

                AddAudit(audit);
                return null;
            }
        }
      
        public int ResetUserPassword(string email)
        {
            var res = VerifyUsernameDoesNotExists(email);

            // If the user exists
            if (res == 2)
            {
                SendPasswordByMail(email);
                return 0;
            }
            else
                return 2;
        }

        public int VerifyPassword(string email, string password)
        {
            // Get hash and salt from database
            
            var query = @"select email, password_salt, password_hash from operator where email = :email";

            OracleParameter[] param = { new OracleParameter("email", email) };
            var dset = new DataSet();

            RunProcedure(query, param, dset);

            if (dset.Tables[0].Rows.Count != 0)
            {
                var row = dset.Tables[0].Rows[0];
                var salt = row["password_salt"].ToString();
                var hash = row["password_hash"].ToString();

                // Verify
                var sh = SaltedHash.Create(salt, hash);
                var value = sh.Verify(password);

                if (value) return 1;

                return 2;
            }
            else
                return 0;
        }

        public int ChangePassword(string password, int userid)
        {
            

            var query = "update operator set password_hash = :password_hash, password_salt = :password_salt, last_edit = :last_edit where userid = :userid";

            // Hash password

            var sh = SaltedHash.Create(password);
            var salt = sh.Salt;
            var hash = sh.Hash;

            OracleParameter[] param =
            {   
                new OracleParameter("userid",  userid),
                new OracleParameter("password_hash",  hash),
                new OracleParameter("password_salt",  salt),
                new OracleParameter("last_edit",  DateTime.Now)
            };

            return ExecuteQuery(query, param);
        }

        public int UpdateOperator(Operator opr)
        {
            

            // Add audit
            AddAudit(new Audit { EventType = (int)AuditEnum.EditLoginDetails, UserId = opr.OperatorId, TimeStamp = DateTime.Now, Description = "Updated Account" });

            var query = "update aspnetusers set firstname = :firstname, surname = :surname, emailconfirmed = 1 where id = :userid";

            OracleParameter[] param =
            {   
                new OracleParameter("firstname",  opr.FirstName.ToUpper()),
                new OracleParameter("surname",  opr.Surname.ToUpper()),
                new OracleParameter("userid",  opr.OperatorId),
            };

            //Audit
            var audit = new Audit
            {
                EventType = (int) AuditEnum.EditLoginDetails,
                Description = opr.EmailAddress
            };

            AddAudit(audit);

            return ExecuteQuery(query, param);
        }

        private Person GetUserFromId(int userid)
        {
            
            var dset = new DataSet();
            Person user = null;

            var query = @"select id, firstname, surname, email, registration_date, userid, password_salt, password_hash, is_active, last_edit
                            from operator where userid = :userid";
            OracleParameter[] param =
                {   
                    new OracleParameter("userid",  userid)
                };

            RunProcedure(query, param, dset);

            if (dset.Tables[0].Rows.Count != 0)
            {
                var row = dset.Tables[0].Rows[0];
                user = new Person
                {
                    PersonId = int.Parse(row["id"].ToString()),
                    FirstName = row["firstname"].ToString(),
                    Surname = row["surname"].ToString(),
                    EmailAddress = row["email"].ToString()
                };

                if (!string.IsNullOrEmpty(row["registration_date"].ToString()))
                    user.DateOfRegistration = DateTime.Parse(row["registration_date"].ToString());
                if (!string.IsNullOrEmpty(row["last_edit"].ToString()))
                    user.LastActivityDate = DateTime.Parse(row["last_edit"].ToString());
                user.IsActive = row["is_active"].ToString() == "1";
                user.Salt = row["password_salt"].ToString();
                user.Hash = row["password_hash"].ToString();
            }

            return user;
        }

        public int VerifyPasswordChange(string email, string guid)
        {
            var query = "select is_changing_pw, new_key, last_pwchange_expiry from operator where email = :email";

            
            var dset = new DataSet();

            OracleParameter[] param =
                        {   
                            new OracleParameter("email",  email)
                        };

            RunProcedure(query, param, dset);

            // Verify if its correct
            if (dset.Tables[0].Rows.Count != 0)
            {
                var pwstatus = int.Parse(dset.Tables[0].Rows[0]["is_changing_pw"].ToString());
                var userkey = Guid.Parse(dset.Tables[0].Rows[0]["new_key"].ToString());
                var expiry = DateTime.Parse((dset.Tables[0].Rows[0]["last_pwchange_expiry"].ToString()));

                // Check password status is set to changing
                if (pwstatus == 1)
                {
                    // Check Guid correct
                    var result = userkey.CompareTo(Guid.Parse(guid));

                    if (result != 0)
                        return 1;

                    // Check Expiration
                    if (DateTime.Now.CompareTo(expiry) == 1)
                        return 2;

                    return 0;
                }
                
                return 1;
            }

            return 1;
        }

        public int UpdateForgottenPassword(string email, string password)
        {
            
            // Hash password
            var sh = SaltedHash.Create(password);

            var salt = sh.Salt;
            var hash = sh.Hash;

            // Update Password
            var query = "update operator set password_hash = :password_hash, password_salt = :password_salt where email = :email";

            OracleParameter[] param =
                    {   
                        new OracleParameter("password_hash", hash),
                        new OracleParameter("email",  email.ToLower()),
                        new OracleParameter("password_salt",  salt)
                    };

            ExecuteQuery(query, param);

            // Update member is_changingpw
            query = "update operator set is_changing_pw = 0 where email = :email";

            OracleParameter[] param2 =
                    {   
                        new OracleParameter("email",  email)
                    };

            ExecuteQuery(query, param2);

            return 0;
        }

        public string SendPasswordByMail(string email)
        {
            
            var newKey = Guid.NewGuid();

            //Set guid and pw status
            var query = "update operator set is_changing_pw = 1, new_key = :new_key, last_pwchange_expiry = :last_pwchange_expiry where email = :email";

            OracleParameter[] param2 =
                    {   
                        new OracleParameter("new_key",  newKey.ToString()),
                        new OracleParameter("email",  email),
                        new OracleParameter("last_pwchange_expiry",  DateTime.Now.AddHours(1))
                    };
            ExecuteQuery(query, param2);

            SendPasswordMail(email, newKey.ToString());
            return string.Empty;

        }

        private void SendPasswordMail(string email, string guid)
        {
            var mail = new MailMessage();
            const string iris = "NoReply:irissmart.com.ng";
            const string friendlyName = "Iris Smart Technologies";
            var server = ConfigurationManager.AppSettings["serverurl"] == null ? "http://10.10.10.50:451" 
                : ConfigurationManager.AppSettings["serverurl"];

            const string buttonstyle = "background-color: #336699; font-family: arial; text-align:center; width:180px; height:30px; " +
                                    "color: White; margin: 15px 0px 20px 0px; border: none; cursor: pointer; line-height: 30px; font-size: 15px;" +
                                    "border-radius: 2px 2px 2px 2px; -moz-border-radius: 2px 2px 2px 2px; -webkit-border-radius: 2px 2px 2px 2px;" +
                                    " -khtml-border-radius:  2px 2px 2px 2px;";
            var button = "<a style='text-decoration: none' href=\"" + server + "/Home?guid=" + guid + "&usr=" + email + 
                "\"><div Style='" + buttonstyle + "'>Reset My Password</div></a>";

            var sb = new StringBuilder();
            sb.Append("<div style='color:#333'>Hello, </div><br/>");
            sb.Append("<div style='color:#333'>Following your request on the Iris website, here is a link from which you can change your password:</div><br/>");
            sb.Append(button);
            sb.Append("<div style='color:#333; margin: 0px 0px 15px 0px'><b>Note:</b> You must be on the Iris network for this to work</div><br/>");
            sb.Append("<div style='color:#333; margin: 0px 0px 10px 0px'>Sincerly,</div><br/>");
            sb.Append("<div style='color:#333'>Iris Software Team</div>");

            mail.From = new MailAddress(iris, friendlyName);
            mail.To.Add(email);

            //set the content
            mail.Subject = "Password Recovery";
            mail.Body = sb.ToString();
            mail.IsBodyHtml = true;

            SendMail(mail);
        }

        public void SendUserActivationMail(string email, string guid, int type)
        {
            var mail = new MailMessage();
            var server = ConfigurationManager.AppSettings["serviceurl"];
            var iris = ConfigurationManager.AppSettings["CompanyEmail"];
            var friendlyName = ConfigurationManager.AppSettings["FriendlyName"];
            const string buttonstyle = "background-color: #336699; font-family: arial; text-align:center; width:150px; height:30px; " +
                                       "color: White; margin: 15px 0px 20px 0px; border: none; cursor: pointer; line-height: 30px; " +
                                       "font-size: 16px; border-radius: 2px 2px 2px 2px; -moz-border-radius: 2px 2px 2px 2px; " +
                                       "-webkit-border-radius: 2px 2px 2px 2px; -khtml-border-radius:  2px 2px 2px 2px;";
            var button = "<a href='" + server + "/Activation?Email=" + email + "&ID=" + guid + "'><div Style='" + buttonstyle + 
                "'>Activate</div></a>";

            var sb = new StringBuilder();
            sb.Append("<div style='color:#333'>Hello, </div><br/>");
            sb.Append("<div style='color:#333'>To complete your registration, please click on the button below:</div><br/>");
            sb.Append(button);
            sb.Append("<div style='color:#333'>Sincerly,</div><br/>");
            sb.Append("<div style='color:#333'>Iris Software Team</div>");

            mail.From = new MailAddress(iris, friendlyName);
            mail.To.Add(email);

            //set the content
            mail.Subject = "Iris Registration Completion";
            mail.Body = sb.ToString();
            mail.IsBodyHtml = true;

            SendMail(mail);

            //SendMeEmail(email, 2);
        }

        public void SendFeedback(string message, string email)
        {
            var mail = new MailMessage();
            var rjl = "NoReply:ajobiswaiting.com";
            var friendlyName = "AJobIsWaiting.com";

            mail.From = new MailAddress(rjl, friendlyName);
            mail.To.Add("ucheogbu:gmail.com");
            mail.To.Add("dami.b.lawal:gmail.com");

            //set the content
            mail.Subject = "Feedback at AJobIsWaiting.com";
            mail.IsBodyHtml = true;
            mail.Body = message + "<br /><br />Reply To: " + email + "<br /><br />";

            
            var query = "insert into irisuser_feedback (feedback, user_email) values (:feedback, :user_email)";
            OracleParameter[] param =
            {   
                new OracleParameter("feedback",  message),
                new OracleParameter("user_email",  email)
            };

            ExecuteQuery(query, param);
            SendMail(mail);
        }

        public void SendMeEmail(string username, int type)
        {
            var mail = new MailMessage();
            var wangsEmail = ConfigurationManager.AppSettings["CompanyEmail"];
            var friendlyName = ConfigurationManager.AppSettings["FriendlyName"];
            var sb = new StringBuilder();

            if (type == 1)
            {
                sb.Append("Someone just registered on AJW to post a single job" + Environment.NewLine + Environment.NewLine);
                sb.Append("Email address: " + username + Environment.NewLine + Environment.NewLine);
                mail.Subject = "Single Job Post";
            }

            if (type == 2)
            {
                sb.Append("A User just reistered and was sent an activation mail" + Environment.NewLine + Environment.NewLine);
                sb.Append("Email address: " + username + Environment.NewLine + Environment.NewLine);
                mail.Subject = "User Registration (Activation mail)";
            }

            if (type == 3)
            {
                sb.Append("A User requrested for their password to be changed" + Environment.NewLine + Environment.NewLine);
                sb.Append("Email address: " + username + Environment.NewLine + Environment.NewLine);
                mail.Subject = "User Password recovery";
            }

            if (type == 4)
            {
                sb.Append("A joobseeker just applied for a job" + Environment.NewLine + Environment.NewLine);
                sb.Append("Email address: " + username + Environment.NewLine + Environment.NewLine);
                mail.Subject = "User Application";
            }

            if (type == 5)
            {
                sb.Append("Application sent via 'Email My CV'" + Environment.NewLine + Environment.NewLine);
                sb.Append("Email address: " + username + Environment.NewLine + Environment.NewLine);
                mail.Subject = "Email My CV application sent";
            }

            if (type == 6)
            {
                sb.Append("Messages were sent all a certain number of applicants" + Environment.NewLine + Environment.NewLine);
                sb.Append("Email address: " + username + Environment.NewLine + Environment.NewLine);
                mail.Subject = "Employer emails applicants";
            }

            if (type == 7)
            {
                sb.Append("List of all job applicants requested" + Environment.NewLine + Environment.NewLine);
                sb.Append("Email address: " + username + Environment.NewLine + Environment.NewLine);
                mail.Subject = "Job applicants List requested";
            }

            mail.From = new MailAddress(wangsEmail, friendlyName);
            mail.To.Add("dami.b.lawal:gmail.com");
            mail.Body = sb.ToString();

            SendMail(mail);
        }

        #endregion

        #region Operators

        public List<Operator> GetAllUsers()
        {
            var result = new List<Operator>();
            

            var query = @"select id, firstname, surname, branch from aspnetusers";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var op = new Operator()
                    {
                        FirstName = row["firstname"].ToString(),
                        Surname = row["surname"].ToString(),
                        OperatorId = int.Parse(row["id"].ToString())
                    };

                    result.Add(op);
                }
            }

            return result;
        }

        public Operator GetOperatorDetails(int oprid)
        {
            var result = new Operator();
            

            var query = @"select id, firstname, surname, email, phonenumber, datecreated, branch, ar.id as roleid, ar.name as rolename from aspnetusers o
                            left join aspnetuserroles r on r.userid = o.id left join aspnetroles ar on ar.id = r.roleid where o.id = :id";

            OracleParameter[] param = 
            { 
                new OracleParameter("id", oprid)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    result = new Operator()
                    {
                        FirstName = row["firstname"].ToString(),
                        Surname = row["surname"].ToString(),
                        EmailAddress = row["email"].ToString(),
                        OperatorId = int.Parse(row["id"].ToString()),
                        DateOfRegistration = DateTime.Parse(row["datecreated"].ToString()),
                        OperatorType = int.Parse(row["roleid"].ToString()),
                        OperatorTypeName = row["rolename"].ToString()
                    };

                    if (!string.IsNullOrEmpty(row["branch"].ToString()))
                        result.BranchId = int.Parse(row["branch"].ToString());

                    // Add roles
                    result.Roles = GetOperatorRoleDetails(result);
                }
            }

            return result;
        }

        #endregion

        #region UMA Application

        public List<Application> GetApplications()
        {
            var result = new List<Application>();
            

            var query = @"select u.app_id, app_name, u.registration_date, owner_id, u.is_active, firstname, surname, icon from apps u 
                            left join aspnetusers o on o.id = u.owner_id where u.is_active = 1 order by app_id";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var app = new Application()
                    {
                        Id = int.Parse(row["app_id"].ToString()),
                        Name = row["app_name"].ToString(),
                        Icon = row["icon"].ToString(),
                        DateCreated = DateTime.Parse(row["registration_date"].ToString()),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        OwnerId = int.Parse(row["owner_id"].ToString()),
                        OwnerName = (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper()
                    };

                    // Add people shared in this application
                    query = @"select userid from APP_SHARE where is_active = 1 and app_id = :app_id";

                    using (var dset2 = new DataSet())
                    {
                        OracleParameter[] param = { new OracleParameter("app_id", app.Id) };
                        RunProcedure(query, param, dset2);
                        app.SharedWith = new List<string>();

                        foreach (DataRow row2 in dset2.Tables[0].Rows)
                        {
                            app.SharedWith.Add(row2["userid"].ToString());
                        }
                    }

                    result.Add(app);
                }
            }

            return result;
        }

        public Application GetApplication(int id)
        {
            

            var query = "select app_name, registration_date, owner_id, is_active, icon from apps where app_id = :app_id";
            OracleParameter[] param = { new OracleParameter("app_id", id) };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var app = new Application()
                    {
                        Id = id,
                        Name = row["app_name"].ToString(),
                        Icon = row["icon"].ToString(),
                        DateCreated = DateTime.Parse(row["registration_date"].ToString()),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        OwnerId = int.Parse(row["owner_id"].ToString())
                    };

                    return app;
                }
            }

            return null;
        }

        public Application GetApplicationByName(string name)
        {
            

            var query = "select app_id, app_name, registration_date, owner_id, is_active, icon from apps where lower(app_name) = :app_name";
            OracleParameter[] param = { new OracleParameter("app_name", name.ToLower()) };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var app = new Application()
                    {
                        Id = int.Parse(row["app_id"].ToString()),
                        Name = row["app_name"].ToString(),
                        Icon = row["icon"].ToString(),
                        DateCreated = DateTime.Parse(row["registration_date"].ToString()),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        OwnerId = int.Parse(row["owner_id"].ToString())
                    };

                    return app;
                }
            }

            return null;
        }

        public List<Operator> GetSharedAppUsers(Application app)
        {
            var result = new List<Operator>();
            

            var query = @"select o.id, firstname, surname from aspnetusers o 
                            left join app_share s on s.userid = o.id
                            where s.app_id = :app_id and o.is_active = 1 and s.is_active = 1";
            OracleParameter[] param = { new OracleParameter("app_id", app.Id) };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var op = new Operator()
                    {
                        FirstName = row["firstname"].ToString(),
                        Surname = row["surname"].ToString(),
                        OperatorId = int.Parse(row["id"].ToString())
                    };

                    result.Add(op);
                }
            }

            return result;
        }

        public List<Operator> GetUmaUsers(Application app)
        {
            var result = new List<Operator>();
            

            var query = @"select id, firstname, surname from aspnetusers o left join aspnetuserroles ur on ur.userid = o.id where ur.roleid = 1 and o.is_active = 1 and id != :id and id not in
                            (select userid from app_share where app_id = :app_id and is_active = 1)";

            OracleParameter[] param = 
            { 
                new OracleParameter("id", app.OwnerId), 
                new OracleParameter("app_id", app.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var op = new Operator()
                    {
                        FirstName = row["firstname"].ToString(),
                        Surname = row["surname"].ToString(),
                        OperatorId = int.Parse(row["id"].ToString())
                    };

                    result.Add(op);
                }
            }

            return result;
        }     

        public List<Operator> SaveSharedAppUsers(Application app)
        {
            // First ensure this is the owner of the application
            var query = "select owner_id from application where app_id = :app_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("app_id", app.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["owner_id"].Equals(app.OwnerId))
                        return null;
                }
            }

            // Check if it has already been shared
            foreach (var user in app.SelectedUsers.Split(','))
            {
                query = "select app_id, userid from app_share where app_id = :app_id and userid = :userid";
                OracleParameter[] param = { new OracleParameter("app_id", app.Id), new OracleParameter("userid", user) };

                using (var dset = new DataSet())
                {
                    RunProcedure(query, param, dset);

                    // insert
                    if (dset.Tables[0].Rows.Count == 0)
                    {
                        query = @"insert into app_share (app_id, userid, date_shared) values (:app_id, :userid, :date_shared)";
                        OracleParameter[] param2 = 
                        { 
                            new OracleParameter("app_id", app.Id), 
                            new OracleParameter("userid", user),
                            new OracleParameter("date_shared", DateTime.Now)
                        };

                        ExecuteQuery(query, param2);
                    }

                    // otherwise update
                    else
                    {
                        query = @"update app_share set is_active = 1, date_shared = systimestamp where app_id = :app_id and userid = :userid";
                        OracleParameter[] param3 = 
                        { 
                            new OracleParameter("app_id", app.Id), 
                            new OracleParameter("userid", user)
                        };

                        ExecuteQuery(query, param3);
                    }
                }
            }

            // Return new list
            return GetSharedAppUsers(app);
        }

        public List<Operator> RemoveShareApp(Application app)
        {
            var query = "update app_share set is_active = 0 where app_id = :app_id and userid = :userid";
            OracleParameter[] param = { new OracleParameter("app_id", app.Id), new OracleParameter("userid", app.EditBy) };

            ExecuteQuery(query, param);

            // Return new list
            return GetSharedAppUsers(app);
        }

        public List<Operator> GetBranchAdmins(Application app)
        {
            var result = new List<Operator>();
            

            var query = @"select id, firstname, surname, email, o.branch, branch_name, emailconfirmed from aspnetusers o left join branch b on b.branch_id = o.branch
                            left join aspnetuserroles ur on ur.userid = id 
                            where roleid = 2 and o.is_active = 1 and o.branch " + (app.BranchId == 0 ? "is null" : "= " + app.BranchId);

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    // Don't add the operator who requested this
                    if (int.Parse(row["id"].ToString()) != app.OwnerId)
                    {
                        var op = new Operator()
                        {
                            FirstName = row["firstname"].ToString(),
                            Surname = row["surname"].ToString(),
                            EmailAddress = row["email"].ToString().ToLower(),
                            BranchId = !string.IsNullOrEmpty(row["branch"].ToString()) ? int.Parse(row["branch"].ToString()) : 0,
                            BranchName = !string.IsNullOrEmpty(row["branch_name"].ToString()) ? row["branch_name"].ToString() : "General",
                            OperatorId = int.Parse(row["id"].ToString()),
                            IsActivated = row["emailconfirmed"].ToString() == "1",
                        };

                        result.Add(op);
                    }
                }
            }

            return result;
        }

        public List<Operator> GetBranchUsers(Application app)
        {
            var result = new List<Operator>();
            

            var query = @"select id, firstname, surname, email, o.branch, branch_name, emailconfirmed from aspnetusers o 
                            left join branch b on b.branch_id = o.branch
                            left join aspnetuserroles ar on ar.USERID = o.id
                            where ar.roleid = 3 and o.is_active = 1 and o.branch = :branch_id order by firstname";

            using (var dset = new DataSet())
            {
                OracleParameter[] param = 
                {
                    new OracleParameter("branch_id", app.BranchId)
                };

                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    // Don't add the operator who requested this
                    if (int.Parse(row["id"].ToString()) != app.OwnerId)
                    {
                        var op = new Operator()
                        {
                            FirstName = row["firstname"].ToString(),
                            Surname = row["surname"].ToString(),
                            EmailAddress = row["email"].ToString().ToLower(),
                            BranchId = !string.IsNullOrEmpty(row["branch"].ToString()) ? int.Parse(row["branch"].ToString()) : 0,
                            BranchName = !string.IsNullOrEmpty(row["branch_name"].ToString()) ? row["branch_name"].ToString() : "General",
                            OperatorId = int.Parse(row["id"].ToString()),
                            IsActivated = row["emailconfirmed"].ToString() == "1",
                        };

                        result.Add(op);
                    }
                }
            }

            return result;
        }

        public List<Operator> GetSuperUsers()
        {
            var result = new List<Operator>();
            

            var query = @"select id, firstname, surname, email, o.branch, branch_name, emailconfirmed from aspnetusers o 
                            left join branch b on b.branch_id = o.branch
                            left join aspnetuserroles ar on ar.USERID = o.id
                            where ar.roleid = 4 and o.is_active = 1 order by firstname";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var op = new Operator()
                    {
                        FirstName = row["firstname"].ToString(),
                        Surname = row["surname"].ToString(),
                        EmailAddress = row["email"].ToString().ToLower(),
                        BranchId = !string.IsNullOrEmpty(row["branch"].ToString()) ? int.Parse(row["branch"].ToString()) : 0,
                        BranchName = !string.IsNullOrEmpty(row["branch_name"].ToString()) ? row["branch_name"].ToString() : "General",
                        OperatorId = int.Parse(row["id"].ToString()),
                        IsActivated = row["emailconfirmed"].ToString() == "1",
                    };

                    result.Add(op);
                }
            }

            return result;
        }
        
        public int AddApplication(Application app)
        {
            

            // Ensure there is no other application with the same name
            var query = @"select app_id from apps where lower(app_name) = :app_name and is_active = 1";
            OracleParameter[] param = 
            {
                new OracleParameter("app_name", app.Name.ToLower())
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);
                if (dset.Tables[0].Rows.Count != 0)
                    return 2;
            }

            // Add new application
            query = @"insert into apps (app_name, registration_date, owner_id) values
                             (:app_name, :registration_date, :owner_id)";

            OracleParameter[] param2 = 
            {
                new OracleParameter("app_name", app.Name),        
                new OracleParameter("registration_date", DateTime.Now),
                new OracleParameter("owner_id", app.OwnerId),
            };

            ExecuteQuery(query, param2);

            // Add audit
            AddAudit(new Audit { EventType = (int)AuditEnum.CreateApplication, UserId = app.OwnerId, TimeStamp = DateTime.Now });

            return 1;
        }

        public int UpdateApplication(Application app)
        {
            

            // First ensure the user has access to the application
            var query = "select owner_id from apps where app_id = :app_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("app_id", app.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["owner_id"].Equals(app.OwnerId))
                        return 2;
                }
                else
                    return 0;
            }

            // Ensure the name is also still unique
            query = "select app_name from apps where app_name = :app_name";
            OracleParameter[] param2 = 
            {
                new OracleParameter("app_name", app.Name)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param2, dset);

                if (dset.Tables[0].Rows.Count != 0)
                    return 3;
            }

            // Do update
            query = @"update apps set app_name = :app_name where app_id = :app_id";

            OracleParameter[] param = 
            {
                new OracleParameter("app_name", app.Name),        
                new OracleParameter("app_id", app.Id)
            };

            ExecuteQuery(query, param);

            // Add audit
            AddAudit(new Audit { EventType = (int)AuditEnum.EditApplication, UserId = app.EditBy, TimeStamp = DateTime.Now });

            return 1;
        }

        public int DeleteApplication(Application app)
        {
            

            // First ensure the user has access to the application
            var query = "select owner_id from apps where app_id = :app_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("app_id", app.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["owner_id"].Equals(app.OwnerId))
                        return 2;
                }
                else
                    return 0;
            }

            query = @"update apps set is_active = 0 where app_id = :app_id";

            OracleParameter[] param = 
            {
                new OracleParameter("app_id", app.Id)
            };

            ExecuteQuery(query, param);

            // Add audit
            AddAudit(new Audit { EventType = (int)AuditEnum.EditApplication, UserId = app.EditBy, TimeStamp = DateTime.Now });

            return 1;
        }

        #endregion

        #region UMA Roles

        public List<Role> GetApplicationRoles(Application app)
        {
            var result = new List<Role>();
            

            var query = @"select role_id, app_id, role_name, creation_date, created_by, p.is_active, firstname, surname, is_linked_to_app, l.role_type_id, l.role_type from roles p 
                            left join aspnetusers o on o.id = p.created_by left join list_role_type l on l.role_type_id = p.role_type where p.is_active = 1 and p.app_id = :app_id order by role_name";

            using (var dset = new DataSet())
            {
                OracleParameter[] param = { new OracleParameter("app_id", app.Id) };
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var rtype = int.Parse(row["role_type_id"].ToString());

                    // If the role type is set, only get those that are of the same role type or below it
                    if (app.RoleType == 0 || (app.RoleType != 0 && ((rtype <= app.RoleType && !app.ExcludeLesserRoles) || (rtype == app.RoleType && app.ExcludeLesserRoles))))
                    {
                        var role = new Role()
                        {
                            Id = int.Parse(row["role_id"].ToString()),
                            RoleType = new RoleType() { IsApplicationLinked = int.Parse(row["is_linked_to_app"].ToString()), RoleTypeId = rtype, Name = row["role_type"].ToString() },
                            ApplicationId = int.Parse(row["app_id"].ToString()),
                            Name = row["role_name"].ToString(),
                            IsActive = int.Parse(row["is_active"].ToString()),
                            DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                            OwnerId = int.Parse(row["created_by"].ToString()),
                            OwnerName = "(" + (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper() + ")"
                        };

                        result.Add(role);
                    }
                }
            }

            return result;
        }

        public List<Role> GetSystemRoles()
        {
            var result = new List<Role>();
            

            var query = @"select role_id, app_id, role_name, creation_date, created_by, p.is_active, firstname, surname, is_linked_to_app, l.role_type_id, l.role_type from roles p 
                            left join aspnetusers o on o.id = p.created_by left join list_role_type l on l.role_type_id = p.role_type where p.is_active = 1 and l.is_linked_to_app = 0 order by role_name";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var rtype = int.Parse(row["role_type_id"].ToString());

                    // If the role type is set, only get those that are of the same role type or below it
                    var role = new Role()
                    {
                        Id = int.Parse(row["role_id"].ToString()),
                        RoleType = new RoleType() { IsApplicationLinked = int.Parse(row["is_linked_to_app"].ToString()), RoleTypeId = rtype, Name = row["role_type"].ToString() },
                        Name = row["role_name"].ToString(),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = "(" + (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper() + ")"
                    };

                    if (!string.IsNullOrEmpty(row["app_id"].ToString())) role.ApplicationId = int.Parse(row["app_id"].ToString());

                    result.Add(role);
                }
            }

            return result;
        }

        public List<Role> GetOperatorRoles(Operator opr)
        {
            var result = new List<Role>();
            

            if (string.IsNullOrEmpty(opr.AppId))
            {
                var query = @"select r.role_id, role_name, r.app_id, r.created_by, r.creation_date, u.id, u.firstname, u.surname from roles r
                            left join user_role_link l on r.role_id = l.role_id
                            left join aspnetusers u on u.ID = r.CREATED_BY 
                            where l.userid = :userid and l.is_active = 1 and r.is_active = 1 
                            order by role_name";

                using (var dset = new DataSet())
                {
                    OracleParameter[] param = { new OracleParameter("userid", opr.OperatorId) };
                    RunProcedure(query, param, dset);

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        var role = new Role()
                        {
                            Id = int.Parse(row["role_id"].ToString()),
                            Name = row["role_name"].ToString(),
                            ApplicationId = int.Parse(row["app_id"].ToString()),
                            DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                            OwnerName = row["firstname"].ToString().ToFirstLetterUpper() + " " + row["surname"].ToString().ToFirstLetterUpper(),
                            OwnerId = int.Parse(row["id"].ToString())
                        };

                        result.Add(role);
                    }
                }
            }
            else
            {
                var query = @"select r.role_id, role_name from roles r
                            left join user_role_link l on r.role_id = l.role_id 
                            where l.userid = :userid and r.app_id = :app_id and l.is_active = 1 and r.is_active = 1 
                            order by role_name";

                using (var dset = new DataSet())
                {
                    OracleParameter[] param = { new OracleParameter("userid", opr.OperatorId), new OracleParameter("app_id", opr.AppId) };
                    RunProcedure(query, param, dset);

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        var role = new Role()
                        {
                            Id = int.Parse(row["role_id"].ToString()),
                            Name = row["role_name"].ToString()
                        };

                        result.Add(role);
                    }
                }
            }

            return result;
        }

        public List<Role> GetRoles()
        {
            var result = new List<Role>();
            

            var query = @"select firstname, app_id, surname, role_id, grp_name, created_by, creation_date from roles p 
                            left join aspnetusers o on o.id = p.created_by where p.is_active = 1 order by grp_name";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var role = new Role()
                    {
                        Id = int.Parse(row["role_id"].ToString()),
                        Name = row["grp_name"].ToString(),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper()
                    };

                    if (!string.IsNullOrEmpty(row["app_id"].ToString())) role.ApplicationId = int.Parse(row["app_id"].ToString());

                    result.Add(role);
                }
            }

            return result;
        }

        public List<RoleType> GetRoleTypes()
        {
            var result = new List<RoleType>();
            

            var query = @"select role_type_id, role_type, is_linked_to_app from list_role_type";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var type = new RoleType()
                    {
                        RoleTypeId = int.Parse(row["role_type_id"].ToString()),
                        IsApplicationLinked = int.Parse(row["is_linked_to_app"].ToString()),
                        Name = row["role_type"].ToString().ToFirstLetterUpper()
                    };

                    result.Add(type);
                }
            }

            return result;
        }

        public List<Role> GetRoles(int branchid)
        {
            var result = new List<Role>();
            

            var query = @"select role_id, role_name, creation_date, created_by, p.is_active, p.app_id, firstname, surname, p.branch_id from roles p 
                            left join aspnetusers o on o.id = p.created_by where p.is_active = 1 and (p.branch_id = :branch_id or p.branch_id is null) order by role_name";

            using (var dset = new DataSet())
            {
                OracleParameter[] param = { new OracleParameter("branch_id", branchid) };
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var role = new Role()
                    {
                        Id = int.Parse(row["role_id"].ToString()),
                        Name = row["role_name"].ToString(),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = "(" + (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper() + ")"
                    };

                    if (!string.IsNullOrEmpty(row["app_id"].ToString())) role.ApplicationId = int.Parse(row["app_id"].ToString());

                    result.Add(role);
                }
            }

            return result;
        }

        public List<Role> GetRolesByType(int type)
        {
            var result = new List<Role>();
            

            var query = @"select role_id, role_name, creation_date, created_by, p.is_active, p.app_id, firstname, surname, t.role_type from roles p
                            left join list_role_type t on t.role_type_id = p.role_type 
                            left join aspnetusers o on o.id = p.created_by where p.is_active = 1 and p.role_type < :role_type order by role_name";

            using (var dset = new DataSet())
            {
                OracleParameter[] param = { new OracleParameter("role_type", type) };
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var role = new Role()
                    {
                        Id = int.Parse(row["role_id"].ToString()),
                        Name = row["role_name"].ToString(),
                        RoleType = new RoleType(){ IsApplicationLinked = 0, RoleTypeId = 1, Name = row["role_type"].ToString() },
                        IsActive = int.Parse(row["is_active"].ToString()),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = "(" + (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper() + ")"
                    };

                    if (!string.IsNullOrEmpty(row["app_id"].ToString())) role.ApplicationId = int.Parse(row["app_id"].ToString());
                    
                    result.Add(role);
                }
            }

            return result;
        }

        public Role GetRoleDetails(Role role)
        {
            

            using (var dset = new DataSet())
            {
                var query = @"select role_id, app_id, role_name, creation_date, created_by, p.is_active, firstname, surname, is_linked_to_app, l.role_type_id, l.role_type from roles p 
                            left join aspnetusers o on o.id = p.created_by left join list_role_type l on l.role_type_id = p.role_type where role_id = :role_id order by role_name";

                OracleParameter[] param = { new OracleParameter("role_id", role.Id) };

                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var newrole = new Role()
                    {
                        Id = int.Parse(row["role_id"].ToString()),
                        RoleType = new RoleType() { IsApplicationLinked = int.Parse(row["is_linked_to_app"].ToString()), RoleTypeId = int.Parse(row["role_type_id"].ToString()), Name = row["role_type"].ToString() },
                        Name = row["role_name"].ToString(),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper()
                    };

                    if (!string.IsNullOrEmpty(row["app_id"].ToString())) newrole.ApplicationId = int.Parse(row["app_id"].ToString());


                    // Add role groups
                    newrole.SubRoles = new List<Role>();
                    GetRoleGroups(newrole);

                    // Get privilege details of each group
                    for (var i = 0; i < newrole.SubRoles.Count; i++)
                    {
                        var details = GetRoleDetails(newrole.SubRoles[i]);
                        newrole.SubRoles[i].Privileges = details.Privileges;
                    }

                    // Add privileges
                    GetRolePrivileges(newrole);

                    // Merge group privileges with role privileges
                    foreach (var grp in newrole.SubRoles)
                    {
                        newrole.Privileges = newrole.Privileges.Union(grp.Privileges).ToList();

                        // Also add subgroups
                        if (grp.SubRoles != null)
                        {
                            foreach (var subgrp in grp.SubRoles)
                            {
                                newrole.Privileges = newrole.Privileges.Union(subgrp.Privileges).ToList();
                            }
                        }
                    }

                    if (!role.HideUnselected)
                    {
                        if (newrole.RoleType.IsApplicationLinked == 1)
                        {
                            // Populate unselected privileges
                            var allprivis = GetApplicationPrivileges(role.ApplicationId);
                            var allprivigrps = GetApplicationRoles(new Application() { Id = role.ApplicationId, RoleType = newrole.RoleType.RoleTypeId }).Where(x => x.RoleType.IsApplicationLinked == 1).ToList();

                            newrole.UnselectedPrivileges = allprivis.Where(p => newrole.Privileges.All(p2 => p2.Id != p.Id)).ToList();

                            // Also dont forget to remove the current role as well. 
                            newrole.UnselectedRoles = allprivigrps.Where(p => newrole.SubRoles.All(p2 => p2.Id != p.Id) && p.Id != role.Id).ToList();
                        }
                        else
                        {
                            // Populate unselected privileges
                            var allprivis = GetPrivileges();
                            var allprivigrps = GetRolesByType(newrole.RoleType.RoleTypeId);

                            newrole.UnselectedPrivileges = allprivis.Where(p => newrole.Privileges.All(p2 => p2.Id != p.Id)).ToList();
                            newrole.UnselectedRoles = allprivigrps.Where(p => newrole.SubRoles.All(p2 => p2.Id != p.Id)).ToList();
                        }
                    }

                    return newrole;
                }
            }

            return null;
        }

        public Role GetRoleDetails(string id)
        {
            

            using (var dset = new DataSet())
            {
                var query = @"select role_id, app_id, role_name, creation_date, created_by, p.is_active, firstname, surname, is_linked_to_app, l.role_type_id, l.role_type from roles p 
                            left join aspnetusers o on o.id = p.created_by left join list_role_type l on l.role_type_id = p.role_type where role_id = :role_id order by role_name";

                OracleParameter[] param = { new OracleParameter("role_id", id) };

                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var newrole = new Role()
                    {
                        Id = int.Parse(row["role_id"].ToString()),
                        RoleType = new RoleType() { IsApplicationLinked = int.Parse(row["is_linked_to_app"].ToString()), RoleTypeId = int.Parse(row["role_type_id"].ToString()), Name = row["role_type"].ToString() },
                        Name = row["role_name"].ToString(),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper()
                    };

                    if (!string.IsNullOrEmpty(row["app_id"].ToString())) newrole.ApplicationId = int.Parse(row["app_id"].ToString());


                    // Add role groups
                    newrole.SubRoles = new List<Role>();
                    GetRoleGroups(newrole);

                    // Get privilege details of each group
                    for (var i = 0; i < newrole.SubRoles.Count; i++)
                    {
                        var details = GetRoleDetails(newrole.SubRoles[i]);
                        newrole.SubRoles[i].Privileges = details.Privileges;
                    }

                    // Add privileges
                    GetRolePrivileges(newrole);

                    // Merge group privileges with role privileges
                    foreach (var grp in newrole.SubRoles)
                    {
                        newrole.Privileges = newrole.Privileges.Union(grp.Privileges).ToList();

                        // Also add subgroups
                        if (grp.SubRoles != null)
                        {
                            foreach (var subgrp in grp.SubRoles)
                            {
                                newrole.Privileges = newrole.Privileges.Union(subgrp.Privileges).ToList();
                            }
                        }
                    }

                    return newrole;
                }
            }

            return null;
        }       

        public int AddRole(Role role)
        {
            
            
            // Check if role with same name already exists
            var query = @"select * from roles where lower(role_name) = :role_name and app_id = :app_id and role_type = :role_type";

            var param = new List<OracleParameter>
            {
                new OracleParameter("role_name", role.Name.ToLower()),
                new OracleParameter("role_type", role.RoleType.RoleTypeId),
                role.ApplicationId == 0
                    ? new OracleParameter("app_id", DBNull.Value)
                    : new OracleParameter("app_id", role.ApplicationId)
            };


            using (var dset = new DataSet())
            {
                RunProcedure(query, param.ToArray(), dset);

                if (dset.Tables[0].Rows.Count == 0)
                {
                    // Carry on
                    query = @"insert into roles (role_name, created_by, role_type, app_id) values
                                     (:role_name, :created_by, :role_type, :app_id)";

                    var param2 = new List<OracleParameter>
                    {
                        new OracleParameter("role_name", role.Name),
                        new OracleParameter("created_by", role.OwnerId),
                        new OracleParameter("role_type", role.RoleType.RoleTypeId),
                        role.ApplicationId == 0
                            ? new OracleParameter("app_id", DBNull.Value)
                            : new OracleParameter("app_id", role.ApplicationId)
                    };


                    ExecuteQuery(query, param2.ToArray());

                    // Add audit
                    AddAudit(new Audit { EventType = (int)AuditEnum.CreateRole, UserId = role.OwnerId, TimeStamp = DateTime.Now });

                    return 1;
                }
                else
                {
                    // Check if it is inactive and update otherwise return
                    var row = dset.Tables[0].Rows[0];
                    if (row["is_active"].ToString() == "0")
                    {
                        query = @"update roles set is_active = 1, created_by = :created_by, app_id = :app_id where role_id = :role_id";

                        OracleParameter[] param2 = 
                        {
                            new OracleParameter("created_by", role.OwnerId),        
                            new OracleParameter("app_id", role.ApplicationId),
                            new OracleParameter("role_id", row["role_id"].ToString())
                        };

                        ExecuteQuery(query, param2);

                        // Add audit
                        AddAudit(new Audit { EventType = (int)AuditEnum.CreateRole, UserId = role.OwnerId, TimeStamp = DateTime.Now });
                        return 1;
                    }
                    else
                        return 2;
                }
            }
        }

        public int AddRolesToRole(List<Role> grps)
        {
            

            foreach (var grp in grps)
            {
                // First check if it has already been inserted in which case it will be simply an update
                var query = @"select role_id_1, role_id_2 from roles_link where role_id_1 = :role_id_1 and role_id_2 = :role_id_2";

                OracleParameter[] paramx = 
                {
                    new OracleParameter("role_id_1", grp.Id),        
                    new OracleParameter("role_id_2", grp.AddToRole)
                };

                using (var dset = new DataSet())
                {
                    RunProcedure(query, paramx, dset);

                    if (dset.Tables[0].Rows.Count == 0)
                    {
                        // Insert the link
                        query = @"insert into roles_link (role_id_1, role_id_2) values
                             (:role_id_1, :role_id_2)";

                        OracleParameter[] param = 
                        {
                            new OracleParameter("role_id_1", grp.Id),        
                            new OracleParameter("role_id_2", grp.AddToRole)
                        };

                        ExecuteQuery(query, param);
                    }
                    else
                    {
                        // Update the row instead
                        query = "update roles_link set is_active = 1 where role_id_1 = :role_id_1 and role_id_2 = :role_id_2";
                        OracleParameter[] param = 
                        {
                            new OracleParameter("role_id_1", grp.Id),        
                            new OracleParameter("role_id_2", grp.AddToRole)
                        };

                        ExecuteQuery(query, param);
                    }
                }

                // Add audit
                AddAudit(new Audit { EventType = (int)AuditEnum.AssignGrpPrivilege, UserId = grp.EditBy, TimeStamp = DateTime.Now });
            }

            return 1;
        }

        public int AddRolesToOperator(List<Role> roles)
        {
            

            foreach (var role in roles)
            {
                // First check if role has been added before
                var query = "select * from user_role_link where role_id = :role_id and userid = :userid";

                using (var dset = new DataSet())
                {
                    OracleParameter[] param = 
                    {
                        new OracleParameter("role_id", role.Id),        
                        new OracleParameter("userid", role.OwnerId)
                    };

                    RunProcedure(query, param, dset);

                    if (dset.Tables[0].Rows.Count == 0)
                    {
                        query = @"insert into user_role_link (role_id, userid) values
                                (:role_id, :userid)";

                        OracleParameter[] param2 = 
                        {
                            new OracleParameter("role_id", role.Id),        
                            new OracleParameter("userid", role.OwnerId)
                        };

                        ExecuteQuery(query, param2);
                    }
                    else
                    {
                        query = @"update user_role_link set is_active = 1 where role_id = :role_id and userid = :userid";

                        OracleParameter[] param2 = 
                        {
                            new OracleParameter("role_id", role.Id),        
                            new OracleParameter("userid", role.OwnerId)
                        };

                        ExecuteQuery(query, param2);
                    }
                }

                // Add audit
                AddAudit(new Audit { EventType = (int)AuditEnum.AssignRole, UserId = role.EditBy, TimeStamp = DateTime.Now, Description = "Added to " + role.OwnerId });
            }

            return 1;
        }

        public int RemoveRolesFromOperator(List<Role> roles)
        {
            

            foreach (var role in roles)
            {
                var query = @"update user_role_link set is_active = 0 where role_id = :role_id and userid = :userid";

                OracleParameter[] param = 
                {
                    new OracleParameter("role_id", role.Id),        
                    new OracleParameter("userid", role.OwnerId)
                };

                ExecuteQuery(query, param);

                // Add audit
                AddAudit(new Audit { EventType = (int)AuditEnum.RemoveRole, UserId = role.OwnerId, TimeStamp = DateTime.Now });
            }

            return 1;
        }

        public int UpdateRole(Role role)
        {
            

            // First ensure the user has access to the role
            var query = "select created_by from roles where role_id = :role_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("role_id", role.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["created_by"].Equals(role.OwnerId))
                        return 2;
                }
                else
                    return 0;
            }

            // Ensure the name is also still unique
            query = "select role_name from roles where lower(role_name) = :role_name and app_id = :app_id";
            OracleParameter[] param2 = 
            {
                new OracleParameter("role_name", role.Name.ToLower()),
                new OracleParameter("app_id", role.ApplicationId)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param2, dset);

                if (dset.Tables[0].Rows.Count != 0)
                    return 3;
            }

            // Do update
            query = @"update roles set role_name = :role_name where role_id = :role_id";

            OracleParameter[] param = 
            {
                new OracleParameter("role_name", role.Name),        
                new OracleParameter("role_id", role.Id)
            };

            ExecuteQuery(query, param);

            // Add audit
            AddAudit(new Audit { EventType = (int)AuditEnum.EditRole, UserId = role.EditBy, TimeStamp = DateTime.Now });

            return 1;
        }

        public int DeleteRole(Role role)
        {
            

            // First ensure the user has access to the role
            var query = "select created_by from roles where role_id = :role_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("role_id", role.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["created_by"].Equals(role.OwnerId))
                        return 2;
                }
                else
                    return 0;
            }

            query = @"update roles set is_active = 0 where role_id = :role_id";

            OracleParameter[] param = 
            {
                new OracleParameter("role_id", role.Id)
            };

            ExecuteQuery(query, param);

            // Also remove all links to role
            query = @"update ROLES_PRIVI_LINK set is_active = 0 where role_id = :role_id";

            OracleParameter[] param2 = 
            {
                new OracleParameter("role_id", role.Id)
            };

            ExecuteQuery(query, param2);

            query = @"update ROLES_LINK set is_active = 0 where ROLE_ID_1 = :role_id OR ROLE_ID_2 = :role_id";

            OracleParameter[] param3 = 
            {
                new OracleParameter("role_id", role.Id)
            };

            ExecuteQuery(query, param3);

            // Add audit
            AddAudit(new Audit { EventType = (int)AuditEnum.DeleteRole, UserId = role.EditBy, TimeStamp = DateTime.Now });

            return 1;
        }

        public void GetRolePrivileges(Role role)
        {
            
            role.Privileges = new List<Privilege>();

            var query = @"select p.privi_id, p.privi_name, p.creation_date, p.created_by, p.app_id from roles_privi_link link 
                             left join privi p on link.privi_id = p.privi_id
                             where link.role_id = :role_id and link.is_active = 1 and p.is_active = 1";
            OracleParameter[] param = 
            { 
                new OracleParameter("role_id", role.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var privi = new Privilege()
                    {
                        IsActive = 1,
                        Id = int.Parse(row["privi_id"].ToString()),
                        Name = row["privi_name"].ToString(),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        ApplicationId = int.Parse(row["app_id"].ToString()),
                    };

                    // Get privilege actions
                    privi.Actions = GetPrivilegeActions(new Action() { PrivilegeId = privi.Id }).Actions;

                    role.Privileges.Add(privi);
                }
            }
        }

        public List<Privilege> GetRolePrivileges(int roleId)
        {
            
            var result = new List<Privilege>();

            var query = @"select p.privi_id, p.privi_name, p.creation_date, p.created_by, p.app_id from roles_privi_link link 
                             left join privi p on link.privi_id = p.privi_id
                             where link.role_id = :role_id and link.is_active = 1 and p.is_active = 1";
            OracleParameter[] param = 
            { 
                new OracleParameter("role_id", roleId)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var privi = new Privilege()
                    {
                        IsActive = 1,
                        Id = int.Parse(row["privi_id"].ToString()),
                        Name = row["privi_name"].ToString(),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        ApplicationId = int.Parse(row["app_id"].ToString()),
                    };

                    // Get privilege actions
                    privi.Actions = GetPrivilegeActions(new Action() { PrivilegeId = privi.Id }).Actions;

                    result.Add(privi);
                }
            }

            return result;
        }

        private void GetRoleGroups(Role role)
        {
            
            role.SubRoles = new List<Role>();

            var query = @"select r.role_id, role_name, created_by, creation_date, r.is_active, app_id, role_type 
                            from roles_link l left join roles r on r.role_id = l.ROLE_ID_1 where l.ROLE_ID_2 = :role_id and l.is_active = 1";
            OracleParameter[] param = 
            { 
                new OracleParameter("role_id", role.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var grp = new Role()
                    {
                        Id = int.Parse(row["role_id"].ToString()),
                        Name = row["role_name"].ToString(),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString())
                    };

                    if (!string.IsNullOrEmpty(row["app_id"].ToString())) grp.ApplicationId = int.Parse(row["app_id"].ToString());

                    role.SubRoles.Add(grp);
                }
            }
        }

        private List<Role> GetOperatorRoleDetails(Operator user)
        {
            var result = new List<Role>();
            

            var query = @"select r.role_id, r.role_name from user_role_link l left join roles r on r.role_id = l.role_id 
                            where l.is_active = 1 and l.userid = :userid";

            using (var dset = new DataSet())
            {
                OracleParameter[] param = { new OracleParameter("userid", user.OperatorId) };
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var role = new Role()
                    {
                        Id = int.Parse(row["role_id"].ToString()),
                        Name = row["role_name"].ToString(),
                        HideUnselected = true
                    };

                    // Get Role info
                    role = GetRoleDetails(role);

                    result.Add(role);
                }
            }

            return result;
        }


        #endregion

        #region UMA Privileges

        public List<Privilege> GetPrivileges()
        {
            var result = new List<Privilege>();
            

            var query = @"select privi_id, privi_name, creation_date, created_by, p.is_active, p.app_id, firstname, surname from privi p 
                            left join aspnetusers o on o.id = p.created_by where p.is_active = 1 order by privi_name";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var app = new Privilege()
                    {
                        Id = int.Parse(row["privi_id"].ToString()),
                        Name = row["privi_name"].ToString(),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        ApplicationId = int.Parse(row["app_id"].ToString()),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper()
                    };

                    result.Add(app);
                }
            }

            return result;
        }

        public List<Privilege> GetApplicationPrivileges(int appid)
        {
            var result = new List<Privilege>();
            

            var query = @"select privi_id, privi_name, creation_date, created_by, p.is_active, p.app_id, firstname, surname from privi p 
                            left join aspnetusers o on o.id = p.created_by where p.app_id = :app_id and p.is_active = 1 order by privi_name";

            OracleParameter[] param = 
            {
                new OracleParameter("app_id", appid)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var app = new Privilege()
                    {
                        Id = int.Parse(row["privi_id"].ToString()),
                        Name = row["privi_name"].ToString(),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        ApplicationId = int.Parse(row["app_id"].ToString()),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper()
                    };

                    result.Add(app);
                }
            }

            return result;
        }

        public int AddPrivilege(Privilege privilege)
        {
            

            // Check if privilege with same name already exists
            var query = @"select privi_id, is_active from privi where lower(privi_name) = :privi_name  and app_id = :app_id";

            OracleParameter[] param = 
            {
                new OracleParameter("privi_name", privilege.Name.ToLower()),
                new OracleParameter("app_id", privilege.ApplicationId)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                if (dset.Tables[0].Rows.Count == 0)
                {
                    // Carry on
                    query = @"insert into privi (privi_name, created_by, app_id) values
                             (:privi_name, :created_by, :app_id)";

                    OracleParameter[] param2 = 
                    {
                        new OracleParameter("privi_name", privilege.Name),        
                        new OracleParameter("created_by", privilege.OwnerId),
                        new OracleParameter("app_id", privilege.ApplicationId)
                    };

                    ExecuteQuery(query, param2);

                    // Add audit
                    AddAudit(new Audit { EventType = (int)AuditEnum.CreatePrivilege, UserId = privilege.OwnerId, TimeStamp = DateTime.Now });

                    return 1;
                }
                else
                {
                    if (dset.Tables[0].Rows[0]["is_active"].ToString() == "1")
                        return 2;
                    else
                    {
                        // Just set active to 1
                        query = "update privi set is_active = 1 where privi_id = :privi_id";

                        OracleParameter[] param2 = 
                        {
                            new OracleParameter("privi_id", dset.Tables[0].Rows[0]["privi_id"].ToString())
                        };

                        ExecuteQuery(query, param2);

                        // Add audit
                        AddAudit(new Audit { EventType = (int)AuditEnum.CreatePrivilege, UserId = privilege.OwnerId, TimeStamp = DateTime.Now });

                        return 1;
                    }
                }
            }
        }

        public int AddPrivilegesToRole(List<Privilege> privileges)
        {
            

            foreach (var privilege in privileges)
            {
                // First check if it has already been inserted in which case it will be simply an update
                var query = @"select privi_id, role_id from roles_privi_link where privi_id = :privi_id and role_id = :role_id";

                OracleParameter[] paramx = 
                {
                    new OracleParameter("privi_id", privilege.Id),        
                    new OracleParameter("role_id", privilege.RoleId)
                };

                using (var dset = new DataSet())
                {
                    RunProcedure(query, paramx, dset);

                    if (dset.Tables[0].Rows.Count == 0)
                    {
                        // Insert the link
                        query = @"insert into roles_privi_link (privi_id, role_id) values
                             (:privi_id, :role_id)";

                        OracleParameter[] param = 
                        {
                            new OracleParameter("privi_id", privilege.Id),        
                            new OracleParameter("role_id", privilege.RoleId)
                        };

                        ExecuteQuery(query, param);
                    }
                    else
                    {
                        // Update the row instead
                        query = "update roles_privi_link set is_active = 1 where privi_id = :privi_id and role_id = :role_id";
                        OracleParameter[] param = 
                        {
                            new OracleParameter("privi_id", privilege.Id),        
                            new OracleParameter("role_id", privilege.RoleId)
                        };

                        ExecuteQuery(query, param);
                    }
                }

                // Add audit
                AddAudit(new Audit { EventType = (int)AuditEnum.AssignPrivilege, UserId = privilege.EditBy, TimeStamp = DateTime.Now });
            }

            return 1;
        }
        
        // Depreciated
        public int AddPrivilegesToGroup(Role grp)
        {
            

            foreach (var privilege in grp.Privileges)
            {
                // First check if it has already been inserted in which case it will be simply an update
                var query = @"select privi_id, role_id from roles_privi_link where privi_id = :privi_id and role_id = :role_id";

                OracleParameter[] paramx = 
                {
                    new OracleParameter("privi_id", privilege.Id),        
                    new OracleParameter("role_id", grp.Id)
                };

                using (var dset = new DataSet())
                {
                    RunProcedure(query, paramx, dset);

                    if (dset.Tables[0].Rows.Count == 0)
                    {
                        // Insert the link
                        query = @"insert into roles_privi_link (role_id, privi_id) values
                             (:role_id, :privi_id)";

                        OracleParameter[] param = 
                        {
                            new OracleParameter("role_id", grp.Id),
                            new OracleParameter("privi_id", privilege.Id)        
                        };

                        ExecuteQuery(query, param);
                    }
                    else
                    {
                        // Update the row instead
                        query = "update roles_privi_link set is_active = 1 where privi_id = :privi_id and role_id = :role_id";
                        OracleParameter[] param = 
                        {
                            new OracleParameter("privi_id", privilege.Id),        
                            new OracleParameter("role_id", grp.Id)
                        };

                        ExecuteQuery(query, param);
                    }
                }

                // Add audit
                AddAudit(new Audit { EventType = (int)AuditEnum.AssignGrpPrivilege, UserId = grp.EditBy, TimeStamp = DateTime.Now });
            }

            return 1;
        }

        public int RemovePrivilegesFromRole(List<Privilege> privileges)
        {
            

            foreach (var privilege in privileges)
            {
                var query = @"update roles_privi_link set is_active = 0 where privi_id = :privi_id and role_id = :role_id";

                OracleParameter[] param = 
                {
                    new OracleParameter("privi_id", privilege.Id),        
                    new OracleParameter("role_id", privilege.RoleId)
                };

                ExecuteQuery(query, param);

                // Add audit
                AddAudit(new Audit { EventType = (int)AuditEnum.UnassignPrivilege, UserId = privilege.EditBy, TimeStamp = DateTime.Now });
            }

            return 1;
        }

        public int RemoveRolesFromRole(List<Role> grps)
        {
            

            foreach (var grp in grps)
            {
                // Update the row instead
                var query = "update roles_link set is_active = 0 where role_id_1 = :role_id_1 and role_id_2 = :role_id_2";
                OracleParameter[] param = 
                        {
                            new OracleParameter("role_id_1", grp.Id),        
                            new OracleParameter("role_id_2", grp.AddToRole)
                        };

                ExecuteQuery(query, param);

                // Add audit
                AddAudit(new Audit { EventType = (int)AuditEnum.UnassignGrpPrivilege, UserId = grp.EditBy, TimeStamp = DateTime.Now });
            }

            return 1;
        }

        // Depreciated
        public int RemovePrivilegesFromGroup(Role grp)
        {
            

            foreach (var privilege in grp.Privileges)
            {
                var query = @"update roles_privi_link set is_active = 0 where privi_id = :privi_id and role_id = :role_id";

                OracleParameter[] param = 
                {
                    new OracleParameter("privi_id", privilege.Id),        
                    new OracleParameter("role_id", grp.Id)
                };

                ExecuteQuery(query, param);

                // Add audit
                AddAudit(new Audit { EventType = (int)AuditEnum.UnassignGrpPrivilege, UserId = privilege.EditBy, TimeStamp = DateTime.Now });
            }

            return 1;
        }

        public int UpdatePrivilege(Privilege privilege)
        {
            

            // First ensure the user has access to the privilege
            var query = "select created_by from privi where privi_id = :privi_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("privi_id", privilege.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["created_by"].Equals(privilege.OwnerId))
                        return 2;
                }
                else
                    return 0;
            }

            // Ensure the name is also still unique
            query = "select privi_name from privi where lower(privi_name) = :privi_name and app_id = :app_id";
            OracleParameter[] param2 = 
            {
                new OracleParameter("privi_name", privilege.Name.ToLower()),
                new OracleParameter("app_id", privilege.ApplicationId)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param2, dset);

                if (dset.Tables[0].Rows.Count != 0)
                    return 3;
            }

            // Do update
            query = @"update privi set privi_name = :privi_name where privi_id = :privi_id";

            OracleParameter[] param = 
            {
                new OracleParameter("privi_name", privilege.Name),        
                new OracleParameter("privi_id", privilege.Id)
            };

            ExecuteQuery(query, param);

            // Add audit
            AddAudit(new Audit { EventType = (int)AuditEnum.EditPrivilege, UserId = privilege.EditBy, TimeStamp = DateTime.Now });

            return 1;
        }

        public int DeletePrivilege(Privilege privilege)
        {
            

            // First ensure the user has access to the privilege
            var query = "select created_by from privi where privi_id = :privi_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("privi_id", privilege.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["created_by"].Equals(privilege.OwnerId))
                        return 2;
                }
                else
                    return 0;
            }

            query = @"update privi set is_active = 0 where privi_id = :privi_id";

            OracleParameter[] param = 
            {
                new OracleParameter("privi_id", privilege.Id)
            };

            ExecuteQuery(query, param);

            // Add audit
            AddAudit(new Audit { EventType = (int)AuditEnum.DeletePrivilege, UserId = privilege.EditBy, TimeStamp = DateTime.Now });

            return 1;
        }

        #endregion

        #region UMA Internal Actions

        public int AddAction(Action action)
        {
            

            // Check if action with same name already exists
            var query = @"select internal_action_id, is_active from internal_action where lower(action) = :action and lower(controller) = :controller and app_id = :app_id";

            OracleParameter[] param = 
            {
                new OracleParameter("action", action.Name.ToLower()),
                new OracleParameter("controller", action.Controller.ToLower()),
                new OracleParameter("app_id", action.ApplicationId)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                if (dset.Tables[0].Rows.Count == 0)
                {
                    // Carry on
                    query = @"insert into internal_action (action, controller, app_id, created_by) values
                                     (:action, :controller, :app_id, :created_by)";

                    OracleParameter[] param2 = 
                    {
                        new OracleParameter("action", action.Name),       
                        new OracleParameter("controller", action.Controller),        
                        new OracleParameter("app_id", action.ApplicationId),        
                        new OracleParameter("created_by", action.OwnerId)
                    };

                    ExecuteQuery(query, param2);

                    // Add audit
                    AddAudit(new Audit { EventType = (int)AuditEnum.CreateAction, UserId = action.OwnerId, TimeStamp = DateTime.Now });

                    return 1;
                }
                else
                {
                    // Check if it is inactive and update otherwise return
                    var row = dset.Tables[0].Rows[0];
                    if (row["is_active"].ToString() == "0")
                    {
                        query = @"update internal_action set is_active = 1, created_by = :created_by where internal_action_id = :internal_action_id";

                        OracleParameter[] param2 = 
                        {
                            new OracleParameter("created_by", action.OwnerId),        
                            new OracleParameter("internal_action_id", row["internal_action_id"].ToString())
                        };

                        ExecuteQuery(query, param2);

                        // Add audit
                        AddAudit(new Audit { EventType = (int)AuditEnum.CreateAction, UserId = action.OwnerId, TimeStamp = DateTime.Now });
                        return 1;
                    }
                    else
                        return 2;
                }
            }
        }

        public int DeleteAction(Action action)
        {
            

            // First ensure the user has access to the action
            var query = "select created_by from internal_action where internal_action_id = :internal_action_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("created_by", action.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["created_by"].Equals(action.OwnerId))
                        return 2;
                }
                else
                    return 0;
            }

            query = @"update internal_action set is_active = 0 where internal_action_id = :internal_action_id";

            OracleParameter[] param = 
            {
                new OracleParameter("internal_action_id", action.Id)
            };

            ExecuteQuery(query, param);

            // Add audit
            AddAudit(new Audit { EventType = (int)AuditEnum.DeleteAction, UserId = action.EditBy, TimeStamp = DateTime.Now });

            return 1;
        }

        public int UpdateAction(Action action)
        {
            

            // First ensure the user has access to the application
            var query = "select created_by from internal_action where internal_action_id = :internal_action_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("created_by", action.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["created_by"].Equals(action.OwnerId))
                        return 2;
                }
                else
                    return 0;
            }

            // Ensure the name is also still unique
            query = "select action from internal_action where lower(action) = :action and lower(controller) = :controller and app_id = :app_id";
            OracleParameter[] param2 = 
            {
                new OracleParameter("action", action.Name.ToLower()),
                new OracleParameter("controller", action.Controller.ToLower()),
                new OracleParameter("app_id", action.ApplicationId)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param2, dset);

                if (dset.Tables[0].Rows.Count != 0)
                    return 3;
            }

            // Do update
            query = @"update internal_action set action = :action, controller = :controller where internal_action_id = :internal_action_id";

            OracleParameter[] param = 
            {
                new OracleParameter("action", action.Name), 
                new OracleParameter("controller", action.Controller), 
                new OracleParameter("internal_action_id", action.Id)
            };

            ExecuteQuery(query, param);

            // Add audit
            AddAudit(new Audit { EventType = (int)AuditEnum.EditAction, UserId = action.EditBy, TimeStamp = DateTime.Now });

            return 1;
        }

        public Privilege GetPrivilegeActions(Action action)
        {

            var result = new Privilege
            {
                Actions = new List<Action>(),
                UnselectedActions = new List<Action>()
            };

            var query = @"select a.internal_action_id, app_id, action, controller, a.creation_date, a.created_by, u.firstname, 
                            u.surname from action_privi_link link
                             left join internal_action a on a.internal_action_id = link.internal_action_id
                             left join aspnetusers u on u.id = a.created_by
                             where link.privi_id = :privi_id and link.is_active = 1 and a.is_active = 1 order by controller";
            OracleParameter[] param = 
            { 
                new OracleParameter("privi_id", action.PrivilegeId)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var act = new Action()
                    {
                        IsActive = 1,
                        ApplicationId = int.Parse(row["app_id"].ToString()),
                        Id = int.Parse(row["internal_action_id"].ToString()),
                        Controller = row["controller"].ToString(),
                        Name = row["action"].ToString(),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = row["firstname"].ToString().ToFirstLetterUpper() + " " +  row["surname"].ToString().ToFirstLetterUpper()
                    };

                    result.Actions.Add(act);
                }
            }

            if (action.ApplicationId != 0)
            {
                // Populate unselected actions
                var allactions = GetApplicationActions(action.ApplicationId);
                result.UnselectedActions = allactions.Where(p => result.Actions.All(p2 => p2.Id != p.Id)).ToList();
            }

            result.Id = action.PrivilegeId;
            return result;
        }

        public List<Action> GetApplicationActions(int appid)
        {
            
            var result = new List<Action>();

            var query = @"select internal_action_id, app_id, action, controller, creation_date, created_by, firstname, surname from internal_action a
                             left join aspnetusers u on u.id = a.created_by
                             where app_id = :app_id and a.is_active = 1 order by controller";
            OracleParameter[] param = 
            { 
                new OracleParameter("app_id", appid)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var action = new Action()
                    {
                        IsActive = 1,
                        ApplicationId = int.Parse(row["app_id"].ToString()),
                        Id = int.Parse(row["internal_action_id"].ToString()),
                        Controller = row["controller"].ToString(),
                        Name = row["action"].ToString(),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = row["firstname"].ToString().ToFirstLetterUpper() + " " + row["surname"].ToString().ToFirstLetterUpper()
                    };

                    result.Add(action);
                }
            }

            return result;
        }

        public int AddActionsToPrivilege(List<Action> actions)
        {
            foreach (var action in actions)
            {
                // First check if action has been added before
                var query = "select * from action_privi_link where privi_id = :privi_id and internal_action_id = :internal_action_id";

                using (var dset = new DataSet())
                {
                    OracleParameter[] param = 
                    {
                        new OracleParameter("privi_id", action.PrivilegeId),        
                        new OracleParameter("internal_action_id", action.Id)
                    };

                    RunProcedure(query, param, dset);

                    if (dset.Tables[0].Rows.Count == 0)
                    {
                        query = @"insert into action_privi_link (internal_action_id, privi_id) values
                                (:internal_action_id, :privi_id)";

                        OracleParameter[] param2 = 
                        {
                            new OracleParameter("internal_action_id", action.Id),        
                            new OracleParameter("privi_id", action.PrivilegeId)
                        };

                        ExecuteQuery(query, param2);
                    }
                    else
                    {
                        query = @"update action_privi_link set is_active = 1 where internal_action_id = :internal_action_id and privi_id = :privi_id";

                        OracleParameter[] param2 = 
                        {
                            new OracleParameter("internal_action_id", action.Id),        
                            new OracleParameter("privi_id", action.PrivilegeId)
                        };

                        ExecuteQuery(query, param2);
                    }
                }

                // Add audit
                AddAudit(new Audit { EventType = (int)AuditEnum.AssignAction, UserId = action.OwnerId, TimeStamp = DateTime.Now });
            }

            return 1;
        }

        public int RemoveActionsFromPrivilege(List<Action> actions)
        {
            foreach (var action in actions)
            {
                var query = @"update action_privi_link set is_active = 0 where internal_action_id = :internal_action_id and privi_id = :privi_id";

                OracleParameter[] param = 
                {
                    new OracleParameter("internal_action_id", action.Id),        
                    new OracleParameter("privi_id", action.PrivilegeId)
                };

                ExecuteQuery(query, param);

                // Add audit
                AddAudit(new Audit { EventType = (int)AuditEnum.UnAssignAction, UserId = action.OwnerId, TimeStamp = DateTime.Now });
            }

            return 1;
        }

        #endregion

        #region Misc

        public List<Branch> GetBranches()
        {
            var result = new List<Branch>();
            

            var query = @"select branch_id, branch_name from branch where is_active = 1 order by branch_name";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var app = new Branch()
                    {
                        BranchId = int.Parse(row["branch_id"].ToString()),
                        BranchName = row["branch_name"].ToString()
                    };

                    result.Add(app);
                }
            }

            return result;
        }

        public string GetBranchName(int branchid)
        {
            var query = @"select branch_name from branch where branch_id = :branch_id";
            OracleParameter[] param = { new OracleParameter("branch_id", branchid) };
            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                if (dset.Tables[0].Rows.Count == 0)
                    return string.Empty;

                return dset.Tables[0].Rows[0][0].ToString();
            }
        }

        public bool TestConnection()
        {
            return true;
        }

        public void SaveError(Error error)
        {
            
            var query = "insert into error_logs (message, stacktrace, innerex_msg, innerex_source, innerex_trace, time_logged) values (:message, :stacktrace, :innerex_msg, :innerex_source, :innerex_trace, :time_logged)";
            OracleParameter[] param =
                    {   
                        new OracleParameter("message",  error.Message),
                        new OracleParameter("stacktrace",  error.StackTrace),
                        new OracleParameter("innerex_msg",  error.InnerExMsg),
                        new OracleParameter("innerex_source",  error.InnerExSrc),
                        new OracleParameter("innerex_trace",  error.InnerExStkTrace),
                        new OracleParameter("time_logged",  DateTime.Now)
                    };

            ExecuteQuery(query, param);
        }

        private void SendMail(MailMessage msg)
        {
            try
            {
                var smtp = new SmtpClient();
                smtp.SendCompleted += SendCompletedCallback;
                smtp.SendAsync(msg, msg);
            }
            catch (Exception ex)
            {
                SaveError(Helper.CreateError(ex));
            }
        }

        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            if (e.Cancelled)
            {

            }
            if (e.Error != null)
            {

            }
        }

        #endregion

        #region Audit

        public List<Audit> GetAuditTrail(DateTime start, DateTime end)
        {
            var dset = new DataSet();
            var query = @"SELECT audit_id, event_type_name, event_type, userid, time_stamp, doc_id, description from audits a 
                            left join list_event_types l on l.event_type_id = a.event_type
                            where time_stamp > TO_DATE('" + start.AddDays(-1).ToString("yyyy-MM-dd") + @"', 'YYYY-MM-DD')
                            and time_stamp < TO_DATE('" + end.AddDays(1).AddDays(1).ToString("yyyy-MM-dd") + @"', 'YYYY-MM-DD')";

            
            RunProcedure(query, null, dset);

            var audits = new List<Audit>();

            foreach (DataRow row in dset.Tables[0].Rows)
            {
                var audit = new Audit()
                {
                    EventType = int.Parse(row["event_type"].ToString()),
                    EventTypeName = row["event_type_name"].ToString(),
                    Description = row["description"].ToString(),
                    TimeStamp = DateTime.Parse(row["time_stamp"].ToString()),
                    User = string.IsNullOrEmpty(row["id"].ToString()) ? null : GetUserFromId(int.Parse(row["id"].ToString()))
                };

                // spaces for austerity sake
                audit.EventTypeName = string.Concat(audit.EventTypeName.Select(x => Char.IsUpper(x) ? " " + x : x.ToString())).TrimStart(' ');
                audits.Add(audit);
            }

            return audits;
        }

        public void AddAudit(Audit audit)
        {
            
            var query = "insert into audits (event_type, user_id, time_stamp, description) values (:event_type, :user_id, :time_stamp, :description)";
            OracleParameter[] param =
                    {   
                        new OracleParameter("event_type",  audit.EventType),
                        new OracleParameter("user_id",  audit.UserId == 0 ? null : audit.UserId),
                        new OracleParameter("time_stamp",  DateTime.Now),
                        new OracleParameter("description",  audit.Description)
                    };

            ExecuteQuery(query, param);
        }

        #endregion

        #region Restful

        //public int Verify

        #endregion
    }
}
