﻿using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Data;
using UMA_DataManager.Misc;
using UMAentities;

namespace UMA_DataManager
{
    public class BranchData : OracleDataProv
    {
        public List<Branch> GetBranches()
        {
            var result = new List<Branch>();

            var query = @"select branch_id, branch_name from branch where is_active = 1 order by branch_name";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var app = new Branch
                    {
                        BranchId = int.Parse(row["branch_id"].ToString()),
                        BranchName = row["branch_name"].ToString()
                    };

                    result.Add(app);
                }
            }

            return result;
        }

        public string GetBranchName(int branchid)
        {
            var query = @"select branch_name from branch where branch_id = :branch_id";
            OracleParameter[] param = { new OracleParameter("branch_id", branchid) };
            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                if (dset.Tables[0].Rows.Count == 0)
                    return string.Empty;

                return dset.Tables[0].Rows[0][0].ToString();
            }
        }

    }
}
