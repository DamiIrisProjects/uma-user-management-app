﻿using Oracle.DataAccess.Client;
using System;
using System.Configuration;
using System.Data;
using System.Net.Mail;
using System.Text;
using UMA_DataManager.Misc;
using UMAdata;
using UMAentities;

namespace UMA_DataManager
{
    public class RegistrationData : OracleDataProv
    {
        private const string Buttonstyle = "background-color: #336699; font-family: arial; text-align:center; width:180px; height:30px; " +
                                   "color: White; margin: 15px 0px 20px 0px; border: none; cursor: pointer; line-height: 30px; " +
                                   "font-size: 15px; border-radius: 2px 2px 2px 2px; -moz-border-radius: 2px 2px 2px 2px; " +
                                   "-webkit-border-radius: 2px 2px 2px 2px; -khtml-border-radius:  2px 2px 2px 2px;";

        private const string Iris = "NoReply:irissmart.com.ng";
        private const string FriendlyName = "Iris Smart Technologies";
        public readonly string Server = ConfigurationManager.AppSettings["serverurl"] ?? "http://10.10.10.50:451";

        public int VerifyUsernameDoesNotExists(string email)
        {
            var dset = new DataSet();

            var query = "select email, is_active, registration_guid from operator where lower(email) = :email";

            OracleParameter[] param =
            {   
                new OracleParameter("email",  email.ToLower()),
            };

            RunProcedure(query, param, dset);
            if (dset.Tables[0].Rows.Count != 0)
            {
                // User already exists. Now if the user hasn't been activated already, send them an activation email
                if (dset.Tables[0].Rows[0]["is_active"].ToString() == "0")
                {
                    //SendUserActivationMail(email, dset.Tables[0].Rows[0]["registration_guid"].ToString(), 1);
                    //return 2;
                    return 3;
                }

                return 3;

            }

            return 0;
        }

        public int RegisterOperator(Operator opr)
        {
            // First verify user doesnt already exist
            var res = VerifyUsernameDoesNotExists(opr.EmailAddress);

            if (res == 0)
            {
                // Hash password
                var sh = SaltedHash.Create(opr.Password);

                var salt = sh.Salt;
                var hash = sh.Hash;
                var regGuid = Guid.NewGuid().ToString();

                var query = @"insert into aspnetuser (firstname, surname, email, password_hash, password_salt, registration_date, registration_guid, branch_id) values (:firstname, :surname, :email, :operator_type, :password_hash, :password_salt, :registration_date, :registration_guid, :branch_id)";
                OracleParameter[] param =
                {   
                    new OracleParameter("firstname",  opr.FirstName.ToUpper()),
                    new OracleParameter("surname",  opr.Surname.ToUpper()),
                    new OracleParameter("email",  opr.EmailAddress.ToUpper()),
                    new OracleParameter("operator_type",  opr.OperatorType),
                    new OracleParameter("password_hash",  hash),
                    new OracleParameter("password_salt",  salt),
                    new OracleParameter("registration_date",  DateTime.Now),
                    new OracleParameter("registration_guid",  regGuid),
                    new OracleParameter("branch_id",  opr.BranchId == 0 ? (object)DBNull.Value : opr.BranchId)
                };

                var result = ExecuteQuery(query, param);

                if (result != 0)
                {
                    new AuditData().AddAudit(new Audit() { UserId = 0, Description = "Registration email sent for " + opr.EmailAddress, EventType = (int)AuditEnum.RegistrationEmailSent });
                    //SendUserActivationMail(enrollee.EmailAddress, regGuid, 1);
                }

                return result;
            }

            return res;
        }

        public int ActivateUser(string guid)
        {
            
            var query = "update operator set is_active = 1 where registration_guid = :registration_guid";
            OracleParameter[] param =
            {   
                new OracleParameter("registration_guid",  guid)
            };

            return ExecuteQuery(query, param);
        }

        public int DeactivateUser(Operator opr)
        {
            
            var query = "update aspnetusers set is_active = :iscancel where id = :id";
            OracleParameter[] param =
                {   
                    new OracleParameter("iscancel",  opr.IsCancel),
                    new OracleParameter("id",  opr.OperatorId),
                };

            ExecuteQuery(query, param);

            //Audit
            var audit = new Audit()
            {
                EventType = (int)AuditEnum.DeactivateUser,
                UserId = opr.OwnerId,
                Description = opr.IsCancel == 2 ? "Cancel invite" : "Deactivate"
            };

            new AuditData().AddAudit(audit);

            return 1;
        }

        public Operator LoginUser(Operator opr)
        {
            // Get hash and salt from database
            
            var query = @"select id, operator_type, firstname, surname, email, registration_date, password_salt, password_hash, o.is_active, 
                        o.emailconfirmed, last_activity, first_time_log, b.branch_id, b.branch_name from aspnetusers o
                        left join branch b on b.branch_id = o.branch 
                        where lower(email) = :email and o.is_active = 1";

            OracleParameter[] param = { new OracleParameter("email", opr.EmailAddress.ToLower()) };
            var dset = new DataSet();

            RunProcedure(query, param, dset);

            if (dset.Tables[0].Rows.Count != 0)
            {
                var row = dset.Tables[0].Rows[0];

                // Normal operators must say what application they are logging onto
                var oprType = int.Parse(row["operator_type"].ToString());

                // Otherwise carry on
                var salt = row["password_salt"].ToString();
                var hash = row["password_hash"].ToString();

                // Verify
                var sh = SaltedHash.Create(salt, hash);
                var value = sh.Verify(opr.Password);

                // Add audit trail
                var audit = new Audit
                {
                    TimeStamp = DateTime.Now,
                    UserId = int.Parse(row["id"].ToString())
                };

                if (value)
                {
                    //set details
                    var user = new Operator
                    {
                        OperatorId = int.Parse(row["id"].ToString()),
                        OperatorType = oprType,
                        FirstTimeLog = int.Parse(row["first_time_log"].ToString()),
                        FirstName = row["firstname"].ToString(),
                        Surname = row["surname"].ToString()
                    };

                    if (!string.IsNullOrEmpty(row["branch"].ToString()))
                        user.BranchId = int.Parse(row["branch"].ToString());
                    user.BranchName = row["branch_name"].ToString();
                    user.EmailAddress = row["email"].ToString();
                    if (!string.IsNullOrEmpty(row["registration_date"].ToString()))
                        user.DateOfRegistration = DateTime.Parse(row["registration_date"].ToString());
                    if (!string.IsNullOrEmpty(row["last_activity"].ToString()))
                        user.LastActivityDate = DateTime.Parse(row["last_activity"].ToString());
                    user.IsActive = row["is_active"].ToString() == "1";
                    user.IsActivated = row["emailconfirmed"].ToString() == "1";
                    user.Salt = row["password_salt"].ToString();
                    user.Hash = row["password_hash"].ToString();

                    // If normal operator get roles
                    if (user.OperatorType == 3 || user.OperatorType == 2)
                    {
                        user.Roles = new RoleData().GetOperatorRoleDetails(user);
                    }

                    //Audit
                    audit.EventType = (int)AuditEnum.LoginSuccess;
                    audit.Description = user.EmailAddress;
                    new AuditData().AddAudit(audit);

                    // If its first time, update it so user doesn't have to see message again
                    if (user.FirstTimeLog == 1)
                    {
                        query = "update operator set first_time_log = 0 where userid = :userid";
                        OracleParameter[] param2 = { new OracleParameter("userid", user.OperatorId) };
                        ExecuteQuery(query, param2);
                    }

                    return user;
                }

                //Audit
                audit.EventType = (int)AuditEnum.LoginFailed;
                audit.Description = opr.EmailAddress;
                new AuditData().AddAudit(audit);
                return null;
            }
            else
            {
                //Audit
                var audit = new Audit()
                {
                    EventType = (int)AuditEnum.LoginFailed,
                    Description = opr.EmailAddress,
                    TimeStamp = DateTime.Now
                };

                new AuditData().AddAudit(audit);
                return null;
            }
        }

        public int ResetUserPassword(string email)
        {
            var res = VerifyUsernameDoesNotExists(email);

            // If the user exists
            if (res == 2)
            {
                SendPasswordByMail(email);
                return 0;
            }

            return 2;
        }

        public int VerifyPassword(string email, string password)
        {
            // Get hash and salt from database
            
            var query = @"select email, password_salt, password_hash from operator where email = :email";

            OracleParameter[] param = { new OracleParameter("email", email) };
            var dset = new DataSet();

            RunProcedure(query, param, dset);

            if (dset.Tables[0].Rows.Count != 0)
            {
                var row = dset.Tables[0].Rows[0];
                var salt = row["password_salt"].ToString();
                var hash = row["password_hash"].ToString();

                // Verify
                var sh = SaltedHash.Create(salt, hash);
                var value = sh.Verify(password);

                if (value) return 1;

                return 2;
            }

            return 0;
        }

        public int ChangePassword(string password, int userid)
        {
            var query = "update operator set password_hash = :password_hash, password_salt = :password_salt, last_edit = :last_edit where userid = :userid";

            // Hash password

            var sh = SaltedHash.Create(password);
            var salt = sh.Salt;
            var hash = sh.Hash;

            OracleParameter[] param =
            {   
                new OracleParameter("userid",  userid),
                new OracleParameter("password_hash",  hash),
                new OracleParameter("password_salt",  salt),
                new OracleParameter("last_edit",  DateTime.Now)
            };

            return ExecuteQuery(query, param);
        }

        public int UpdateOperator(Operator opr)
        {
            // Add audit
            new AuditData().AddAudit(new Audit
            {
                EventType = (int)AuditEnum.EditLoginDetails,
                UserId = opr.OperatorId,
                TimeStamp = DateTime.Now,
                Description = "Updated Account"
            });

            var query = "update aspnetusers set firstname = :firstname, surname = :surname, emailconfirmed = 1 where id = :userid";

            OracleParameter[] param =
            {   
                new OracleParameter("firstname",  opr.FirstName.ToUpper()),
                new OracleParameter("surname",  opr.Surname.ToUpper()),
                new OracleParameter("userid",  opr.OperatorId),
            };

            //Audit
            var audit = new Audit
            {
                EventType = (int) AuditEnum.EditLoginDetails,
                Description = opr.EmailAddress
            };

            new AuditData().AddAudit(audit);

            return ExecuteQuery(query, param);
        }

        public int VerifyPasswordChange(string email, string guid)
        {
            var query = "select is_changing_pw, new_key, last_pwchange_expiry from operator where email = :email";
            
            var dset = new DataSet();

            OracleParameter[] param =
            {   
                new OracleParameter("email",  email)
            };

            RunProcedure(query, param, dset);

            // Verify if its correct
            if (dset.Tables[0].Rows.Count != 0)
            {
                var pwstatus = int.Parse(dset.Tables[0].Rows[0]["is_changing_pw"].ToString());
                var userkey = Guid.Parse(dset.Tables[0].Rows[0]["new_key"].ToString());
                var expiry = DateTime.Parse((dset.Tables[0].Rows[0]["last_pwchange_expiry"].ToString()));

                // Check password status is set to changing
                if (pwstatus == 1)
                {
                    // Check Guid correct
                    var result = userkey.CompareTo(Guid.Parse(guid));

                    if (result != 0)
                        return 1;
                    // Check Expiration
                    return DateTime.Now.CompareTo(expiry) == 1 ? 2 : 0;
                }
            }

            return 1;
        }

        public int UpdateForgottenPassword(string email, string password)
        {
            // Hash password
            var sh = SaltedHash.Create(password);

            var salt = sh.Salt;
            var hash = sh.Hash;

            // Update Password
            var query = "update operator set password_hash = :password_hash, password_salt = :password_salt where email = :email";

            OracleParameter[] param =
            {   
                new OracleParameter("password_hash", hash),
                new OracleParameter("email",  email.ToLower()),
                new OracleParameter("password_salt",  salt)
            };

            ExecuteQuery(query, param);

            // Update member is_changingpw
            query = "update operator set is_changing_pw = 0 where email = :email";

            OracleParameter[] param2 =
            {   
                new OracleParameter("email",  email)
            };

            ExecuteQuery(query, param2);

            return 0;
        }

        public string SendPasswordByMail(string email)
        {
            var newKey = Guid.NewGuid();

            //Set guid and pw status
            var query = "update operator set is_changing_pw = 1, new_key = :new_key, last_pwchange_expiry = :last_pwchange_expiry " +
                        "where email = :email";

            OracleParameter[] param2 =
            {   
                new OracleParameter("new_key",  newKey.ToString()),
                new OracleParameter("email",  email),
                new OracleParameter("last_pwchange_expiry",  DateTime.Now.AddHours(1))
            };
            ExecuteQuery(query, param2);

            SendPasswordMail(email, newKey.ToString());
            return string.Empty;

        }

        private void SendPasswordMail(string email, string guid)
        {
            var mail = new MailMessage();
            var button = "<a style='text-decoration: none' href=\"" + Server + "/Home?guid=" + guid + "&usr=" + email + 
                "\"><div Style='" + Buttonstyle + "'>Reset My Password</div></a>";

            var sb = new StringBuilder();
            sb.Append("<div style='color:#333'>Hello, </div><br/>");
            sb.Append("<div style='color:#333'>Following your request on the Iris website, " +
                      "here is a link from which you can change your password:</div><br/>");
            sb.Append(button);
            sb.Append("<div style='color:#333; margin: 0px 0px 15px 0px'><b>Note:</b> You must be on the Iris network for this to work</div><br/>");
            sb.Append("<div style='color:#333; margin: 0px 0px 10px 0px'>Sincerly,</div><br/>");
            sb.Append("<div style='color:#333'>Iris Software Team</div>");

            mail.From = new MailAddress(Iris, FriendlyName);
            mail.To.Add(email);

            //set the content
            mail.Subject = "Password Recovery";
            mail.Body = sb.ToString();
            mail.IsBodyHtml = true;

            MiscData.SendMail(mail);
        }

        public void SendUserActivationMail(string email, string guid, int type)
        {
            var mail = new MailMessage();
            
            var button = "<a href='" + Server + "/Activation?Email=" + email + "&ID=" + guid + 
                "'><div Style='" + Buttonstyle + "'>Activate</div></a>";

            var sb = new StringBuilder();
            sb.Append("<div style='color:#333'>Hello, </div><br/>");
            sb.Append("<div style='color:#333'>To complete your registration, please click on the button below:</div><br/>");
            sb.Append(button);
            sb.Append("<div style='color:#333'>Sincerly,</div><br/>");
            sb.Append("<div style='color:#333'>Iris Software Team</div>");

            mail.From = new MailAddress(Iris, FriendlyName);
            mail.To.Add(email);

            //set the content
            mail.Subject = "Iris Registration Completion";
            mail.Body = sb.ToString();
            mail.IsBodyHtml = true;

            MiscData.SendMail(mail);
        }
    }
}
