﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using UMA_DataManager.Misc;
using UMAentities;

namespace UMA_DataManager
{
    public class ApplicationData : OracleDataProv
    {
        public List<Application> GetApplications()
        {
            var result = new List<Application>();

            var query = @"select u.app_id, u.port, u.app_name, u.registration_date, u.owner_id, u.is_active, u.icon, o.firstname, o.surname from apps u 
                            left join aspnetusers o on o.id = u.owner_id where u.is_active = 1 order by u.app_id";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var app = new Application()
                    {
                        Id = int.Parse(row["app_id"].ToString()),
                        Name = row["app_name"].ToString(),
                        Port = row["port"].ToString(),
                        Icon = row["icon"].ToString(),
                        DateCreated = DateTime.Parse(row["registration_date"].ToString()),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        OwnerId = int.Parse(row["owner_id"].ToString()),
                        OwnerName = (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper()
                    };

                    // Add people shared in this application
                    query = @"select userid from APP_SHARE where is_active = 1 and app_id = :app_id";

                    using (var dset2 = new DataSet())
                    {
                        OracleParameter[] param = { new OracleParameter("app_id", app.Id) };
                        RunProcedure(query, param, dset2);
                        app.SharedWith = new List<string>();

                        foreach (DataRow row2 in dset2.Tables[0].Rows)
                        {
                            app.SharedWith.Add(row2["userid"].ToString());
                        }
                    }

                    result.Add(app);
                }
            }

            return result;
        }

        public Application GetApplication(int id)
        {
            

            var query = "select app_name, registration_date, owner_id, is_active, port, icon from apps where app_id = :app_id";
            OracleParameter[] param = { new OracleParameter("app_id", id) };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var app = new Application()
                    {
                        Id = id,
                        Name = row["app_name"].ToString(),
                        Port = row["port"].ToString(),
                        Icon = row["icon"].ToString(),
                        DateCreated = DateTime.Parse(row["registration_date"].ToString()),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        OwnerId = int.Parse(row["owner_id"].ToString())
                    };

                    return app;
                }
            }

            return null;
        }

        public Application GetApplicationByName(string name)
        {
            

            var query = "select app_id, app_name, registration_date, owner_id, is_active, icon from apps where lower(app_name) = :app_name";
            OracleParameter[] param = { new OracleParameter("app_name", name.ToLower()) };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var app = new Application()
                    {
                        Id = int.Parse(row["app_id"].ToString()),
                        Name = row["app_name"].ToString(),
                        Icon = row["icon"].ToString(),
                        DateCreated = DateTime.Parse(row["registration_date"].ToString()),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        OwnerId = int.Parse(row["owner_id"].ToString())
                    };

                    return app;
                }
            }

            return null;
        }

        public List<Operator> GetSharedAppUsers(Application app)
        {
            var result = new List<Operator>();
            

            var query = @"select o.id, firstname, surname from aspnetusers o 
                            left join app_share s on s.userid = o.id
                            where s.app_id = :app_id and o.is_active = 1 and s.is_active = 1";
            OracleParameter[] param = { new OracleParameter("app_id", app.Id) };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var op = new Operator()
                    {
                        FirstName = row["firstname"].ToString(),
                        Surname = row["surname"].ToString(),
                        OperatorId = int.Parse(row["id"].ToString())
                    };

                    result.Add(op);
                }
            }

            return result;
        }

        public List<Operator> GetUmaUsers(Application app)
        {
            var result = new List<Operator>();
            

            var query = @"select id, firstname, surname from aspnetusers o left join aspnetuserroles ur on ur.userid = o.id where ur.roleid = 1 and o.is_active = 1 and id != :id and id not in
                            (select userid from app_share where app_id = :app_id and is_active = 1)";

            OracleParameter[] param = 
            { 
                new OracleParameter("id", app.OwnerId), 
                new OracleParameter("app_id", app.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var op = new Operator()
                    {
                        FirstName = row["firstname"].ToString(),
                        Surname = row["surname"].ToString(),
                        OperatorId = int.Parse(row["id"].ToString())
                    };

                    result.Add(op);
                }
            }

            return result;
        }

        public List<Operator> SaveSharedAppUsers(Application app)
        {
            // First ensure this is the owner of the application
            var query = "select owner_id from application where app_id = :app_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("app_id", app.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["owner_id"].Equals(app.OwnerId))
                        return null;
                }
            }

            // Check if it has already been shared
            foreach (var user in app.SelectedUsers.Split(','))
            {
                query = "select app_id, userid from app_share where app_id = :app_id and userid = :userid";
                OracleParameter[] param = { new OracleParameter("app_id", app.Id), new OracleParameter("userid", user) };

                using (var dset = new DataSet())
                {
                    RunProcedure(query, param, dset);

                    // insert
                    if (dset.Tables[0].Rows.Count == 0)
                    {
                        query = @"insert into app_share (app_id, userid, date_shared) values (:app_id, :userid, :date_shared)";
                        OracleParameter[] param2 = 
                        { 
                            new OracleParameter("app_id", app.Id), 
                            new OracleParameter("userid", user),
                            new OracleParameter("date_shared", DateTime.Now)
                        };

                        ExecuteQuery(query, param2);
                    }

                    // otherwise update
                    else
                    {
                        query = @"update app_share set is_active = 1, date_shared = systimestamp where app_id = :app_id and userid = :userid";
                        OracleParameter[] param3 = 
                        { 
                            new OracleParameter("app_id", app.Id), 
                            new OracleParameter("userid", user)
                        };

                        ExecuteQuery(query, param3);
                    }
                }
            }

            // Return new list
            return GetSharedAppUsers(app);
        }

        public List<Operator> RemoveShareApp(Application app)
        {
            var query = "update app_share set is_active = 0 where app_id = :app_id and userid = :userid";
            OracleParameter[] param = { new OracleParameter("app_id", app.Id), new OracleParameter("userid", app.EditBy) };

            ExecuteQuery(query, param);

            // Return new list
            return GetSharedAppUsers(app);
        }

        public List<Operator> GetBranchAdmins(Application app)
        {
            var result = new List<Operator>();
            

            var query = @"select id, firstname, surname, email, o.branch, branch_name, emailconfirmed from aspnetusers o left join branch b on b.branch_id = o.branch
                            left join aspnetuserroles ur on ur.userid = id 
                            where roleid = 2 and o.is_active = 1 and o.branch " + (app.BranchId == 0 ? "is null" : "= " + app.BranchId);

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    // Don't add the operator who requested this
                    if (int.Parse(row["id"].ToString()) != app.OwnerId)
                    {
                        var op = new Operator()
                        {
                            FirstName = row["firstname"].ToString(),
                            Surname = row["surname"].ToString(),
                            EmailAddress = row["email"].ToString().ToLower(),
                            BranchId = !string.IsNullOrEmpty(row["branch"].ToString()) ? int.Parse(row["branch"].ToString()) : 0,
                            BranchName = !string.IsNullOrEmpty(row["branch_name"].ToString()) ? row["branch_name"].ToString() : "General",
                            OperatorId = int.Parse(row["id"].ToString()),
                            IsActivated = row["emailconfirmed"].ToString() == "1",
                        };

                        result.Add(op);
                    }
                }
            }

            return result;
        }

        public List<Operator> GetBranchUsers(Application app)
        {
            var result = new List<Operator>();
            

            var query = @"select id, firstname, surname, email, o.branch, branch_name, emailconfirmed from aspnetusers o 
                            left join branch b on b.branch_id = o.branch
                            left join aspnetuserroles ar on ar.USERID = o.id
                            where ar.roleid = 3 and o.is_active = 1 and o.branch = :branch_id order by firstname";

            using (var dset = new DataSet())
            {
                OracleParameter[] param = 
                {
                    new OracleParameter("branch_id", app.BranchId)
                };

                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    // Don't add the operator who requested this
                    if (int.Parse(row["id"].ToString()) != app.OwnerId)
                    {
                        var op = new Operator()
                        {
                            FirstName = row["firstname"].ToString(),
                            Surname = row["surname"].ToString(),
                            EmailAddress = row["email"].ToString().ToLower(),
                            BranchId = !string.IsNullOrEmpty(row["branch"].ToString()) ? int.Parse(row["branch"].ToString()) : 0,
                            BranchName = !string.IsNullOrEmpty(row["branch_name"].ToString()) ? row["branch_name"].ToString() : "General",
                            OperatorId = int.Parse(row["id"].ToString()),
                            IsActivated = row["emailconfirmed"].ToString() == "1",
                        };

                        result.Add(op);
                    }
                }
            }

            return result;
        }

        public List<Operator> GetSuperUsers()
        {
            var result = new List<Operator>();
            

            var query = @"select id, firstname, surname, email, o.branch, branch_name, emailconfirmed from aspnetusers o 
                            left join branch b on b.branch_id = o.branch
                            left join aspnetuserroles ar on ar.USERID = o.id
                            where ar.roleid = 4 and o.is_active = 1 order by firstname";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var op = new Operator()
                    {
                        FirstName = row["firstname"].ToString(),
                        Surname = row["surname"].ToString(),
                        EmailAddress = row["email"].ToString().ToLower(),
                        BranchId = !string.IsNullOrEmpty(row["branch"].ToString()) ? int.Parse(row["branch"].ToString()) : 0,
                        BranchName = !string.IsNullOrEmpty(row["branch_name"].ToString()) ? row["branch_name"].ToString() : "General",
                        OperatorId = int.Parse(row["id"].ToString()),
                        IsActivated = row["emailconfirmed"].ToString() == "1",
                    };

                    result.Add(op);
                }
            }

            return result;
        }

        public int AddApplication(Application app)
        {
            

            // Ensure there is no other application with the same name
            var query = @"select app_id from apps where lower(app_name) = :app_name and is_active = 1";
            OracleParameter[] param = 
            {
                new OracleParameter("app_name", app.Name.ToLower())
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);
                if (dset.Tables[0].Rows.Count != 0)
                    return 2;
            }

            // Add new application
            query = @"insert into apps (app_name, registration_date, owner_id) values
                             (:app_name, :registration_date, :owner_id)";

            OracleParameter[] param2 = 
            {
                new OracleParameter("app_name", app.Name),        
                new OracleParameter("registration_date", DateTime.Now),
                new OracleParameter("owner_id", app.OwnerId),
            };

            ExecuteQuery(query, param2);

            // Add audit
            new AuditData().AddAudit(new Audit { EventType = (int)AuditEnum.CreateApplication, UserId = app.OwnerId, TimeStamp = DateTime.Now });

            return 1;
        }

        public int UpdateApplication(Application app)
        {
            

            // First ensure the user has access to the application
            var query = "select owner_id from apps where app_id = :app_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("app_id", app.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["owner_id"].Equals(app.OwnerId))
                        return 2;
                }
                else
                    return 0;
            }

            // Ensure the name is also still unique
            query = "select app_name from apps where app_name = :app_name";
            OracleParameter[] param2 = 
            {
                new OracleParameter("app_name", app.Name)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param2, dset);

                if (dset.Tables[0].Rows.Count != 0)
                    return 3;
            }

            // Do update
            query = @"update apps set app_name = :app_name where app_id = :app_id";

            OracleParameter[] param = 
            {
                new OracleParameter("app_name", app.Name),        
                new OracleParameter("app_id", app.Id)
            };

            ExecuteQuery(query, param);

            // Add audit
            new AuditData().AddAudit(new Audit { EventType = (int)AuditEnum.EditApplication, UserId = app.EditBy, TimeStamp = DateTime.Now });

            return 1;
        }

        public int DeleteApplication(Application app)
        {
            

            // First ensure the user has access to the application
            var query = "select owner_id from apps where app_id = :app_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("app_id", app.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["owner_id"].Equals(app.OwnerId))
                        return 2;
                }
                else
                    return 0;
            }

            query = @"update apps set is_active = 0 where app_id = :app_id";

            OracleParameter[] param = 
            {
                new OracleParameter("app_id", app.Id)
            };

            ExecuteQuery(query, param);

            // Add audit
            new AuditData().AddAudit(new Audit { EventType = (int)AuditEnum.EditApplication, UserId = app.EditBy, TimeStamp = DateTime.Now });

            return 1;
        }
    }
}
