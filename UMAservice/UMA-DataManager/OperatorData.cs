﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using UMA_DataManager.Misc;
using UMAdata;
using UMAentities;

namespace UMA_DataManager
{
    public class OperatorData : OracleDataProv
    {
        public List<Operator> GetAllUsers()
        {
            var result = new List<Operator>();

            var query = @"select id, firstname, surname, branch from aspnetusers";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var op = new Operator
                    {
                        FirstName = row["firstname"].ToString(),
                        Surname = row["surname"].ToString(),
                        OperatorId = int.Parse(row["id"].ToString())
                    };

                    result.Add(op);
                }
            }

            return result;
        }

        public Operator GetOperatorDetails(int oprid)
        {
            var result = new Operator();
            

            var query = @"select id, firstname, surname, email, phonenumber, datecreated, branch, ar.id as roleid, ar.name as rolename 
                            from aspnetusers o
                            left join aspnetuserroles r on r.userid = o.id left join aspnetroles ar on ar.id = r.roleid where o.id = :id";

            OracleParameter[] param = 
            { 
                new OracleParameter("id", oprid)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    result = new Operator
                    {
                        FirstName = row["firstname"].ToString(),
                        Surname = row["surname"].ToString(),
                        EmailAddress = row["email"].ToString(),
                        OperatorId = int.Parse(row["id"].ToString()),
                        DateOfRegistration = DateTime.Parse(row["datecreated"].ToString()),
                        OperatorType = int.Parse(row["roleid"].ToString()),
                        OperatorTypeName = row["rolename"].ToString()
                    };

                    if (!string.IsNullOrEmpty(row["branch"].ToString()))
                        result.BranchId = int.Parse(row["branch"].ToString());

                    // Add roles
                    result.Roles = new RoleData().GetOperatorRoleDetails(result);
                }
            }

            return result;
        }

        public int RegisterOperator(Operator opr)
        {
            // First verify user doesnt already exist
            var res = VerifyUsernameDoesNotExists(opr.EmailAddress);

            if (res == 0)
            {
                
                // Hash password
                var sh = SaltedHash.Create(opr.Password);

                var salt = sh.Salt;
                var hash = sh.Hash;
                var regGuid = Guid.NewGuid().ToString();

                var query = @"insert into aspnetuser (id, firstname, surname, email, password_hash, password_salt, 
                                registration_date, registration_guid, branch_id) values (aspnetusers_seq.nextval, :firstname, :surname, 
                                :email, :operator_type, :password_hash, :password_salt, :registration_date, :registration_guid, :branch_id)";
                OracleParameter[] param =
                {   
                    new OracleParameter("firstname",  opr.FirstName.ToUpper()),
                    new OracleParameter("surname",  opr.Surname.ToUpper()),
                    new OracleParameter("email",  opr.EmailAddress.ToUpper()),
                    new OracleParameter("operator_type",  opr.OperatorType),
                    new OracleParameter("password_hash",  hash),
                    new OracleParameter("password_salt",  salt),
                    new OracleParameter("registration_date",  DateTime.Now),
                    new OracleParameter("registration_guid",  regGuid),
                    new OracleParameter("branch_id",  opr.BranchId == 0 ? (object)DBNull.Value : opr.BranchId)
                };

                var result = ExecuteQuery(query, param);

                if (result != 0)
                {
                    new AuditData().AddAudit(new Audit
                    {
                        UserId = 0,
                        Description = "Registration email sent for " + opr.EmailAddress,
                        EventType = (int)AuditEnum.RegistrationEmailSent
                    });
                }

                return result;
            }

            return res;
        }

        public Operator LoginUser(Operator opr)
        {
            // Get hash and salt from database
            
            var query = @"select id, operator_type, firstname, surname, email, registration_date, password_salt, password_hash, 
                            o.is_active, o.emailconfirmed, last_activity, first_time_log, b.branch_id, b.branch_name from aspnetusers o
                            left join branch b on b.branch_id = o.branch 
                            where lower(email) = :email and o.is_active = 1";

            OracleParameter[] param = { new OracleParameter("email", opr.EmailAddress.ToLower()) };
            var dset = new DataSet();

            RunProcedure(query, param, dset);

            if (dset.Tables[0].Rows.Count != 0)
            {
                var row = dset.Tables[0].Rows[0];

                // Normal operators must say what application they are logging onto
                var oprType = int.Parse(row["operator_type"].ToString());

                // Otherwise carry on
                var salt = row["password_salt"].ToString();
                var hash = row["password_hash"].ToString();

                // Verify
                var sh = SaltedHash.Create(salt, hash);
                var value = sh.Verify(opr.Password);

                // Add audit trail
                var audit = new Audit
                {
                    TimeStamp = DateTime.Now,
                    UserId = int.Parse(row["id"].ToString())
                };

                if (value)
                {
                    //set details
                    var user = new Operator
                    {
                        OperatorId = int.Parse(row["id"].ToString()),
                        OperatorType = oprType,
                        FirstTimeLog = int.Parse(row["first_time_log"].ToString()),
                        FirstName = row["firstname"].ToString(),
                        Surname = row["surname"].ToString()
                    };

                    if (!string.IsNullOrEmpty(row["branch"].ToString()))
                        user.BranchId = int.Parse(row["branch"].ToString());
                    user.BranchName = row["branch_name"].ToString();
                    user.EmailAddress = row["email"].ToString();
                    if (!string.IsNullOrEmpty(row["registration_date"].ToString()))
                        user.DateOfRegistration = DateTime.Parse(row["registration_date"].ToString());
                    if (!string.IsNullOrEmpty(row["last_activity"].ToString()))
                        user.LastActivityDate = DateTime.Parse(row["last_activity"].ToString());
                    user.IsActive = row["is_active"].ToString() == "1";
                    user.IsActivated = row["emailconfirmed"].ToString() == "1";
                    user.Salt = row["password_salt"].ToString();
                    user.Hash = row["password_hash"].ToString();

                    // If normal operator get roles
                    if (user.OperatorType == 3 || user.OperatorType == 2)
                    {
                        user.Roles = new RoleData().GetOperatorRoleDetails(user);
                    }

                    //Audit
                    audit.EventType = (int)AuditEnum.LoginSuccess;
                    audit.Description = user.EmailAddress;
                    new AuditData().AddAudit(audit);

                    // If its first time, update it so user doesn't have to see message again
                    if (user.FirstTimeLog == 1)
                    {
                        query = "update operator set first_time_log = 0 where userid = :userid";
                        OracleParameter[] param2 = { new OracleParameter("userid", user.OperatorId) };
                        ExecuteQuery(query, param2);
                    }

                    return user;
                }

                //Audit
                audit.EventType = (int)AuditEnum.LoginFailed;
                audit.Description = opr.EmailAddress;
                new AuditData().AddAudit(audit);
                return null;
            }
            else
            {
                //Audit
                var audit = new Audit()
                {
                    EventType = (int)AuditEnum.LoginFailed,
                    Description = opr.EmailAddress,
                    TimeStamp = DateTime.Now
                };

                new AuditData().AddAudit(audit);
                return null;
            }
        }

        public int UpdateOperator(Operator opr)
        {
            // Add audit
            new AuditData().AddAudit(new Audit
            {
                EventType = (int)AuditEnum.EditLoginDetails,
                UserId = opr.OperatorId,
                TimeStamp = DateTime.Now,
                Description = "Updated Account"
            });

            string query = "update aspnetusers set firstname = :firstname, surname = :surname, emailconfirmed = 1 where id = :userid";

            OracleParameter[] param =
            {   
                new OracleParameter("firstname",  opr.FirstName.ToUpper()),
                new OracleParameter("surname",  opr.Surname.ToUpper()),
                new OracleParameter("userid",  opr.OperatorId),
            };

            //Audit
            var audit = new Audit
            {
                EventType = (int) AuditEnum.EditLoginDetails,
                Description = opr.EmailAddress
            };

            new AuditData().AddAudit(audit);

            return ExecuteQuery(query, param);
        }

        public int DeactivateUser(Operator opr)
        {
            
            var query = "update aspnetusers set is_active = :iscancel where id = :id";
            OracleParameter[] param =
                {   
                    new OracleParameter("iscancel",  opr.IsCancel),
                    new OracleParameter("id",  opr.OperatorId),
                };

            ExecuteQuery(query, param);

            //Audit
            var audit = new Audit()
            {
                EventType = (int)AuditEnum.DeactivateUser,
                UserId = opr.OwnerId,
                Description = opr.IsCancel == 2 ? "Cancel invite" : "Deactivate"
            };

            new AuditData().AddAudit(audit);

            return 1;
        }

        public List<Role> GetOperatorRoles(Operator opr)
        {
            var result = new List<Role>();
            

            if (string.IsNullOrEmpty(opr.AppId))
            {
                var query = @"select r.role_id, role_name, r.app_id, r.created_by, r.creation_date, u.id, u.firstname, u.surname from roles r
                            left join user_role_link l on r.role_id = l.role_id
                            left join aspnetusers u on u.ID = r.CREATED_BY 
                            where l.userid = :userid and l.is_active = 1 and r.is_active = 1 
                            order by role_name";

                using (var dset = new DataSet())
                {
                    OracleParameter[] param = { new OracleParameter("userid", opr.OperatorId) };
                    RunProcedure(query, param, dset);

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        var role = new Role()
                        {
                            Id = int.Parse(row["role_id"].ToString()),
                            Name = row["role_name"].ToString(),
                            ApplicationId = int.Parse(row["app_id"].ToString()),
                            DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                            OwnerName = row["firstname"].ToString().ToFirstLetterUpper() + " " + row["surname"].ToString().ToFirstLetterUpper(),
                            OwnerId = int.Parse(row["id"].ToString())
                        };

                        // Add privileges of role
                        role.Privileges =  GetOperatorRolePrivileges(role.Id);

                        result.Add(role);
                    }
                }
            }
            else
            {
                var query = @"select r.role_id, role_name from roles r
                            left join user_role_link l on r.role_id = l.role_id 
                            where l.userid = :userid and r.app_id = :app_id and l.is_active = 1 and r.is_active = 1 
                            order by role_name";

                using (var dset = new DataSet())
                {
                    OracleParameter[] param = { new OracleParameter("userid", opr.OperatorId), new OracleParameter("app_id", opr.AppId) };
                    RunProcedure(query, param, dset);

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        var role = new Role()
                        {
                            Id = int.Parse(row["role_id"].ToString()),
                            Name = row["role_name"].ToString()
                        };

                        result.Add(role);
                    }
                }
            }

            return result;
        }

        public List<Privilege> GetOperatorRolePrivileges(int roleid)
        {
            var result = new List<Privilege>();
            

                var query = @"select p.privi_id, p.privi_name from privi p 
                                left join roles_privi_link l on l.PRIVI_ID = p.PRIVI_ID
                                left join roles r on r.ROLE_ID = l.ROLE_ID where l.role_id = :role_id and p.is_active = 1 order by privi_name";

            OracleParameter[] param = 
            {
                new OracleParameter("role_id", roleid)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var app = new Privilege
                    {
                        Id = int.Parse(row["privi_id"].ToString()),
                        Name = row["privi_name"].ToString()
                    };

                    result.Add(app);
                }
            }

            return result;
        }

        public int VerifyUsernameDoesNotExists(string email)
        {
            
            var dset = new DataSet();

            var query = "select email, is_active, registration_guid from operator where lower(email) = :email";

            OracleParameter[] param =
                    {   
                        new OracleParameter("email",  email.ToLower()),
                    };

            RunProcedure(query, param, dset);
            if (dset.Tables[0].Rows.Count != 0)
            {
                // User already exists. Now if the user hasn't been activated already, send them an activation email
                if (dset.Tables[0].Rows[0]["is_active"].ToString() == "0")
                {
                    return 3;
                }

                return 3;
            }

            return 0;
        }
    }
}
