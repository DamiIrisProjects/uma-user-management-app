﻿using Oracle.DataAccess.Client;
using System;
using System.ComponentModel;
using System.Data;
using System.Net.Mail;
using UMAentities;

namespace UMA_DataManager.Misc
{
    public static class MiscData
    {
        public static bool TestConnection()
        {
            return true;
        }

        public static void SaveError(Error error)
        {
            var query = "insert into error_logs (error_id, message, stacktrace, innerex_msg, innerex_source, innerex_trace, time_logged) values (error_logs_seq.nextval, :message, :stacktrace, :innerex_msg, :innerex_source, :innerex_trace, :time_logged)";
            OracleParameter[] param =
                    {   
                        new OracleParameter("message",  error.Message),
                        new OracleParameter("stacktrace",  error.StackTrace),
                        new OracleParameter("innerex_msg",  error.InnerExMsg),
                        new OracleParameter("innerex_source",  error.InnerExSrc),
                        new OracleParameter("innerex_trace",  error.InnerExStkTrace),
                        new OracleParameter("time_logged",  DateTime.Now)
                    };

            new OracleDataProv().ExecuteQuery(query, param);
        }

        public static int GetPreviousSequenceValue(OracleCommand cmd)
        {
            
            return new OracleDataProv().GetIdNumber(cmd);
        }

        public static int GetOraclePreviousSequenceValue(OracleCommand cmd, string table)
        {
            if (cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();

            cmd.CommandText = "SELECT " + table.ToUpper() + "_SEQ.CURRVAL FROM DUAL";
            return Convert.ToInt32(cmd.ExecuteScalar());
        }

        public static int GetOracleNextSequenceValue(OracleCommand cmd, string table)
        {
            if (cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();

            cmd.CommandText = "SELECT " + table.ToUpper() + "_SEQ.NEXTVAL FROM DUAL";
            return Convert.ToInt32(cmd.ExecuteScalar());
        }

        public static void SendMail(MailMessage msg)
        {
            try
            {
                var smtp = new SmtpClient();
                smtp.SendCompleted += SendCompletedCallback;
                smtp.SendAsync(msg, msg);
            }
            catch (Exception ex)
            {
                SaveError(Helper.CreateError(ex));
            }
        }

        public static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            if (e.Cancelled)
            {

            }
            if (e.Error != null)
            {

            }
        }
    }
}
