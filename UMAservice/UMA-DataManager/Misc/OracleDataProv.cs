﻿using System;
using System.Configuration;
using System.Data;
using Oracle.DataAccess.Client;

namespace UMA_DataManager.Misc
{
    public class OracleDataProv
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["OracleConnectionStringXE"].ToString();
        private OracleConnection _connection;
        public OracleCommand Command;

        public OracleDataProv()
        {

        }

        public OracleDataProv(string conn)
        {
            _connectionString = ConfigurationManager.ConnectionStrings[conn].ToString();
        }

        public void CreateConnection()
        {
            if (_connection == null)
            {
                _connection = new OracleConnection(_connectionString);
            }
        }

        public void CreateTransactionCommand()
        {
            if (Command != null && Command.Transaction != null && Command.Transaction.Connection != null && Command.Transaction.Connection.State == ConnectionState.Open)
                Command.Transaction.Connection.Close();

            if (_connection == null)
                CreateConnection();

            if (_connection.State == ConnectionState.Closed)
                _connection.Open();

            var transaction = _connection.BeginTransaction();

            Command = _connection.CreateCommand();
            Command.Transaction = transaction;
        }

        public int ExecuteQuery(OracleParameter[] parameters)
        {
            var cmd = Command;

            if (cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();

            if (parameters != null && parameters.Length > 0)
            {
                cmd.Parameters.Clear();

                foreach (var p in parameters)
                {
                    if (p != null)
                        cmd.Parameters.Add(p);
                }
            }

            var iRowsAffected = cmd.ExecuteNonQuery();

            return iRowsAffected;
        }

        public int ExecuteQuery(string strQuery, OracleParameter[] parameters)
        {
            int iRowsAffected;

            try
            {
                if (_connection == null)
                {
                    CreateConnection();
                }

                if (_connection.State != ConnectionState.Open)
                    _connection.Open();

                using (var cmd = _connection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (var i = 0; i < parameters.Length; i++)
                        {
                            if (parameters[i].Value == null)
                                parameters[i].Value = DBNull.Value;

                            cmd.Parameters.Add(parameters[i]);
                        }
                    }

                    cmd.Connection = _connection;
                    iRowsAffected = cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                _connection?.Close();
            }

            return iRowsAffected;
        }

        public void RunProcedure(string strQuery, OracleParameter[] parameters, DataSet dataSet)
        {
            try
            {
                // Make sure there is a connection before running this
                if (_connection == null)
                {
                    CreateConnection();
                }

                if (_connection.State != ConnectionState.Open)
                    _connection.Open();

                using (var cmd = _connection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (var i = 0; i < parameters.Length; i++)
                        {
                            cmd.Parameters.Add(i);
                        }
                    }

                    var oracleDa = new OracleDataAdapter {SelectCommand = cmd};
                    oracleDa.Fill(dataSet);
                    _connection.Close();
                }
            }
            finally
            {
                if (_connection != null && _connection.State == ConnectionState.Open)
                {
                    _connection.Close();
                }
            }
        }

        public int GetIdNumber(OracleCommand cmd)
        {
            if (cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();

            cmd.CommandText = "select last_insert_id();";
            return Convert.ToInt32(cmd.ExecuteScalar());
        }

        public void CloseQuery()
        {
            _connection.Close();
        }
    }
}
