﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using UMA_DataManager.Misc;
using UMAentities;
using Action = UMAentities.Action;

namespace UMA_DataManager
{
    public class RoleData : OracleDataProv
    {
        public List<Role> GetApplicationRoles(Application app)
        {
            var result = new List<Role>();

            var query = @"select role_id, app_id, role_name, creation_date, created_by, p.is_active, firstname, surname, is_linked_to_app, l.role_type_id, l.role_type from roles p 
                            left join aspnetusers o on o.id = p.created_by left join list_role_type l on l.role_type_id = p.role_type where p.is_active = 1 and p.app_id = :app_id order by role_name";

            using (var dset = new DataSet())
            {
                OracleParameter[] param = { new OracleParameter("app_id", app.Id) };
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var rtype = int.Parse(row["role_type_id"].ToString());

                    // If the role type is set, only get those that are of the same role type or below it
                    if (app.RoleType == 0 || app.RoleType != 0 && (rtype <= app.RoleType && !app.ExcludeLesserRoles || 
                        rtype == app.RoleType && app.ExcludeLesserRoles))
                    {
                        var role = new Role
                        {
                            Id = int.Parse(row["role_id"].ToString()),
                            RoleType = new RoleType
                            {
                                IsApplicationLinked = int.Parse(row["is_linked_to_app"].ToString()),
                                RoleTypeId = rtype,
                                Name = row["role_type"].ToString()
                            },
                            ApplicationId = int.Parse(row["app_id"].ToString()),
                            Name = row["role_name"].ToString(),
                            IsActive = int.Parse(row["is_active"].ToString()),
                            DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                            OwnerId = int.Parse(row["created_by"].ToString()),
                            OwnerName = "(" + (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper() + ")"
                        };

                        result.Add(role);
                    }
                }
            }

            return result;
        }

        public List<Role> GetSystemRoles()
        {
            var result = new List<Role>();
            var query = @"select role_id, app_id, role_name, creation_date, created_by, p.is_active, firstname, surname, is_linked_to_app, 
                        l.role_type_id, l.role_type from roles p 
                        left join aspnetusers o on o.id = p.created_by left join list_role_type l on l.role_type_id = p.role_type 
                        where p.is_active = 1 and l.is_linked_to_app = 0 order by role_name";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var rtype = int.Parse(row["role_type_id"].ToString());

                    // If the role type is set, only get those that are of the same role type or below it
                    var role = new Role()
                    {
                        Id = int.Parse(row["role_id"].ToString()),
                        RoleType = new RoleType
                        {
                            IsApplicationLinked = int.Parse(row["is_linked_to_app"].ToString()),
                            RoleTypeId = rtype,
                            Name = row["role_type"].ToString()
                        },
                        Name = row["role_name"].ToString(),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = "(" + (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper() + ")"
                    };

                    if (!string.IsNullOrEmpty(row["app_id"].ToString())) role.ApplicationId = int.Parse(row["app_id"].ToString());

                    result.Add(role);
                }
            }

            return result;
        }

        public List<Role> GetOperatorRoles(Operator opr)
        {
            var result = new List<Role>();
            
            if (string.IsNullOrEmpty(opr.AppId))
            {
                var query = @"select r.role_id, role_name, r.app_id, r.created_by, r.creation_date, u.id, u.firstname, u.surname from roles r
                            left join user_role_link l on r.role_id = l.role_id
                            left join aspnetusers u on u.ID = r.CREATED_BY 
                            where l.userid = :userid and l.is_active = 1 and r.is_active = 1 
                            order by role_name";

                using (var dset = new DataSet())
                {
                    OracleParameter[] param = { new OracleParameter("userid", opr.OperatorId) };
                    RunProcedure(query, param, dset);

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        var role = new Role
                        {
                            Id = int.Parse(row["role_id"].ToString()),
                            Name = row["role_name"].ToString(),
                            ApplicationId = int.Parse(row["app_id"].ToString()),
                            DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                            OwnerName = row["firstname"].ToString().ToFirstLetterUpper() + " " + row["surname"].ToString().ToFirstLetterUpper(),
                            OwnerId = int.Parse(row["id"].ToString())
                        };

                        result.Add(role);
                    }
                }
            }
            else
            {
                var query = @"select r.role_id, role_name from roles r
                            left join user_role_link l on r.role_id = l.role_id 
                            where l.userid = :userid and r.app_id = :app_id and l.is_active = 1 and r.is_active = 1 
                            order by role_name";

                using (var dset = new DataSet())
                {
                    OracleParameter[] param =
                    {
                        new OracleParameter("userid", opr.OperatorId),
                        new OracleParameter("app_id", opr.AppId)
                    };
                    RunProcedure(query, param, dset);

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        var role = new Role
                        {
                            Id = int.Parse(row["role_id"].ToString()),
                            Name = row["role_name"].ToString()
                        };

                        result.Add(role);
                    }
                }
            }

            return result;
        }

        public List<Role> GetRoles()
        {
            var result = new List<Role>();
            
            var query = @"select firstname, app_id, surname, role_id, grp_name, created_by, creation_date from roles p 
                            left join aspnetusers o on o.id = p.created_by where p.is_active = 1 order by grp_name";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var role = new Role()
                    {
                        Id = int.Parse(row["role_id"].ToString()),
                        Name = row["grp_name"].ToString(),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper()
                    };

                    if (!string.IsNullOrEmpty(row["app_id"].ToString())) role.ApplicationId = int.Parse(row["app_id"].ToString());

                    result.Add(role);
                }
            }

            return result;
        }

        public List<RoleType> GetRoleTypes()
        {
            var result = new List<RoleType>();
            
            var query = @"select role_type_id, role_type, is_linked_to_app from list_role_type";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var type = new RoleType()
                    {
                        RoleTypeId = int.Parse(row["role_type_id"].ToString()),
                        IsApplicationLinked = int.Parse(row["is_linked_to_app"].ToString()),
                        Name = row["role_type"].ToString().ToFirstLetterUpper()
                    };

                    result.Add(type);
                }
            }

            return result;
        }

        public List<Role> GetRoles(int branchid)
        {
            var result = new List<Role>();
            

            var query = @"select role_id, role_name, creation_date, created_by, p.is_active, p.app_id, firstname, surname, p.branch_id from roles p 
                        left join aspnetusers o on o.id = p.created_by where p.is_active = 1 and 
                        (p.branch_id = :branch_id or p.branch_id is null) order by role_name";

            using (var dset = new DataSet())
            {
                OracleParameter[] param = { new OracleParameter("branch_id", branchid) };
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var role = new Role
                    {
                        Id = int.Parse(row["role_id"].ToString()),
                        Name = row["role_name"].ToString(),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = "(" + (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper() + ")"
                    };

                    if (!string.IsNullOrEmpty(row["app_id"].ToString())) role.ApplicationId = int.Parse(row["app_id"].ToString());

                    result.Add(role);
                }
            }

            return result;
        }

        public List<Role> GetRolesByType(int type)
        {
            var result = new List<Role>();
            
            var query = @"select role_id, role_name, creation_date, created_by, p.is_active, p.app_id, firstname, surname, t.role_type from roles p
                            left join list_role_type t on t.role_type_id = p.role_type 
                            left join aspnetusers o on o.id = p.created_by where p.is_active = 1 and p.role_type < :role_type order by role_name";

            using (var dset = new DataSet())
            {
                OracleParameter[] param = { new OracleParameter("role_type", type) };
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var role = new Role
                    {
                        Id = int.Parse(row["role_id"].ToString()),
                        Name = row["role_name"].ToString(),
                        RoleType = new RoleType() { IsApplicationLinked = 0, RoleTypeId = 1, Name = row["role_type"].ToString() },
                        IsActive = int.Parse(row["is_active"].ToString()),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = "(" + (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper() + ")"
                    };

                    if (!string.IsNullOrEmpty(row["app_id"].ToString())) role.ApplicationId = int.Parse(row["app_id"].ToString());

                    result.Add(role);
                }
            }

            return result;
        }

        public Role GetRoleDetails(Role role)
        {
            using (var dset = new DataSet())
            {
                var query = @"select role_id, app_id, role_name, creation_date, created_by, p.is_active, firstname, surname, 
                            is_linked_to_app, l.role_type_id, l.role_type from roles p 
                            left join aspnetusers o on o.id = p.created_by left join list_role_type l on l.role_type_id = p.role_type 
                            where role_id = :role_id order by role_name";

                OracleParameter[] param = { new OracleParameter("role_id", role.Id) };

                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var newrole = new Role
                    {
                        Id = int.Parse(row["role_id"].ToString()),
                        RoleType = new RoleType
                        {
                            IsApplicationLinked = int.Parse(row["is_linked_to_app"].ToString()),
                            RoleTypeId = int.Parse(row["role_type_id"].ToString()),
                            Name = row["role_type"].ToString()
                        },
                        Name = row["role_name"].ToString(),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper()
                    };

                    if (!string.IsNullOrEmpty(row["app_id"].ToString())) newrole.ApplicationId = int.Parse(row["app_id"].ToString());


                    // Add role groups
                    newrole.SubRoles = new List<Role>();
                    GetRoleGroups(newrole);

                    // Get privilege details of each group
                    for (var i = 0; i < newrole.SubRoles.Count; i++)
                    {
                        var details = GetRoleDetails(newrole.SubRoles[i]);
                        newrole.SubRoles[i].Privileges = details.Privileges;
                    }

                    // Add privileges
                    GetRolePrivileges(newrole);

                    // Merge group privileges with role privileges
                    foreach (var grp in newrole.SubRoles)
                    {
                        newrole.Privileges = newrole.Privileges.Union(grp.Privileges).ToList();

                        // Also add subgroups
                        if (grp.SubRoles != null)
                        {
                            foreach (var subgrp in grp.SubRoles)
                            {
                                newrole.Privileges = newrole.Privileges.Union(subgrp.Privileges).ToList();
                            }
                        }
                    }

                    if (!role.HideUnselected)
                    {
                        if (newrole.RoleType.IsApplicationLinked == 1)
                        {
                            // Populate unselected privileges
                            var allprivis = new PrivilegeData().GetApplicationPrivileges(role.ApplicationId);
                            var allprivigrps = GetApplicationRoles(new Application()
                            {
                                Id = role.ApplicationId,
                                RoleType = newrole.RoleType.RoleTypeId
                            }).Where(x => x.RoleType.IsApplicationLinked == 1).ToList();

                            newrole.UnselectedPrivileges = allprivis.Where(p => newrole.Privileges.All(p2 => p2.Id != p.Id)).ToList();

                            // Also dont forget to remove the current role as well. 
                            newrole.UnselectedRoles = allprivigrps.Where(p => newrole.SubRoles.All(p2 => p2.Id != p.Id) && p.Id != role.Id).ToList();
                        }
                        else
                        {
                            // Populate unselected privileges
                            var allprivis = new PrivilegeData().GetPrivileges();
                            var allprivigrps = GetRolesByType(newrole.RoleType.RoleTypeId);

                            newrole.UnselectedPrivileges = allprivis.Where(p => newrole.Privileges.All(p2 => p2.Id != p.Id)).ToList();
                            newrole.UnselectedRoles = allprivigrps.Where(p => newrole.SubRoles.All(p2 => p2.Id != p.Id)).ToList();
                        }
                    }

                    return newrole;
                }
            }

            return null;
        }

        public Role GetRoleDetailsById(int id)
        {
            using (var dset = new DataSet())
            {
                var query = @"select role_id, app_id, role_name, creation_date, created_by, p.is_active, firstname, surname, is_linked_to_app, 
                            l.role_type_id, l.role_type from roles p 
                            left join aspnetusers o on o.id = p.created_by left join list_role_type l on l.role_type_id = p.role_type 
                            where role_id = :role_id order by role_name";

                OracleParameter[] param = { new OracleParameter("role_id", id) };

                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var newrole = new Role
                    {
                        Id = int.Parse(row["role_id"].ToString()),
                        RoleType = new RoleType
                        {
                            IsApplicationLinked = int.Parse(row["is_linked_to_app"].ToString()),
                            RoleTypeId = int.Parse(row["role_type_id"].ToString()),
                            Name = row["role_type"].ToString()
                        },
                        Name = row["role_name"].ToString(),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper()
                    };

                    if (!string.IsNullOrEmpty(row["app_id"].ToString())) newrole.ApplicationId = int.Parse(row["app_id"].ToString());


                    // Add role groups
                    newrole.SubRoles = new List<Role>();
                    GetRoleGroups(newrole);

                    // Get privilege details of each group
                    for (var i = 0; i < newrole.SubRoles.Count; i++)
                    {
                        var details = GetRoleDetails(newrole.SubRoles[i]);
                        newrole.SubRoles[i].Privileges = details.Privileges;
                    }

                    // Add privileges
                    GetRolePrivileges(newrole);

                    // Merge group privileges with role privileges
                    foreach (var grp in newrole.SubRoles)
                    {
                        newrole.Privileges = newrole.Privileges.Union(grp.Privileges).ToList();

                        // Also add subgroups
                        if (grp.SubRoles != null)
                        {
                            foreach (var subgrp in grp.SubRoles)
                            {
                                newrole.Privileges = newrole.Privileges.Union(subgrp.Privileges).ToList();
                            }
                        }
                    }

                    return newrole;
                }
            }

            return null;
        }

        public int AddRole(Role role)
        {
            // Check if role with same name already exists
            var query = @"select * from roles where lower(role_name) = :role_name and app_id = :app_id and role_type = :role_type";

            var param = new List<OracleParameter>
            {
                new OracleParameter("role_name", role.Name.ToLower()),
                new OracleParameter("role_type", role.RoleType.RoleTypeId),
                role.ApplicationId == 0
                    ? new OracleParameter("app_id", DBNull.Value)
                    : new OracleParameter("app_id", role.ApplicationId)
            };


            using (var dset = new DataSet())
            {
                RunProcedure(query, param.ToArray(), dset);

                if (dset.Tables[0].Rows.Count == 0)
                {
                    // Carry on
                    query = @"insert into roles (role_id, role_name, created_by, role_type, app_id) values
                                     (roles_seq.nextval, :role_name, :created_by, :role_type, :app_id)";

                    var param2 = new List<OracleParameter>
                    {
                        new OracleParameter("role_name", role.Name),
                        new OracleParameter("created_by", role.OwnerId),
                        new OracleParameter("role_type", role.RoleType.RoleTypeId),
                        role.ApplicationId == 0
                            ? new OracleParameter("app_id", DBNull.Value)
                            : new OracleParameter("app_id", role.ApplicationId)
                    };


                    try
                    {
                        ExecuteQuery(query, param2.ToArray());
                    }
                    catch (Exception)
                    {
                        return 0;
                    }

                    // Add audit
                    new AuditData().AddAudit(new Audit { EventType = (int)AuditEnum.CreateRole, UserId = role.OwnerId, TimeStamp = DateTime.Now });

                    return 1;
                }

                // Check if it is inactive and update otherwise return
                var row = dset.Tables[0].Rows[0];
                if (row["is_active"].ToString() == "0")
                {
                    query = @"update roles set is_active = 1, created_by = :created_by, app_id = :app_id where role_id = :role_id";

                    OracleParameter[] param2 = 
                    {
                        new OracleParameter("created_by", role.OwnerId),        
                        new OracleParameter("app_id", role.ApplicationId),
                        new OracleParameter("role_id", row["role_id"].ToString())
                    };

                    ExecuteQuery(query, param2);

                    // Add audit
                    new AuditData().AddAudit(new Audit { EventType = (int)AuditEnum.CreateRole, UserId = role.OwnerId, TimeStamp = DateTime.Now });
                    return 1;
                }

                return 2;
            }
        }

        public int AddRolesToRole(List<Role> grps)
        {
            foreach (var grp in grps)
            {
                // First check if it has already been inserted in which case it will be simply an update
                var query = @"select role_id_1, role_id_2 from roles_link where role_id_1 = :role_id_1 and role_id_2 = :role_id_2";

                OracleParameter[] paramx = 
                {
                    new OracleParameter("role_id_1", grp.Id),        
                    new OracleParameter("role_id_2", grp.AddToRole)
                };

                using (var dset = new DataSet())
                {
                    RunProcedure(query, paramx, dset);

                    if (dset.Tables[0].Rows.Count == 0)
                    {
                        // Insert the link
                        query = @"insert into roles_link (role_id_1, role_id_2) values
                             (:role_id_1, :role_id_2)";

                        OracleParameter[] param = 
                        {
                            new OracleParameter("role_id_1", grp.Id),        
                            new OracleParameter("role_id_2", grp.AddToRole)
                        };

                        ExecuteQuery(query, param);
                    }
                    else
                    {
                        // Update the row instead
                        query = "update roles_link set is_active = 1 where role_id_1 = :role_id_1 and role_id_2 = :role_id_2";
                        OracleParameter[] param = 
                        {
                            new OracleParameter("role_id_1", grp.Id),        
                            new OracleParameter("role_id_2", grp.AddToRole)
                        };

                        ExecuteQuery(query, param);
                    }
                }

                // Add audit
                new AuditData().AddAudit(new Audit { EventType = (int)AuditEnum.AssignGrpPrivilege, UserId = grp.EditBy, TimeStamp = DateTime.Now });
            }

            return 1;
        }

        public int AddRolesToOperator(List<Role> roles)
        {
            foreach (var role in roles)
            {
                // First check if role has been added before
                var query = "select * from user_role_link where role_id = :role_id and userid = :userid";

                using (var dset = new DataSet())
                {
                    OracleParameter[] param = 
                    {
                        new OracleParameter("role_id", role.Id),        
                        new OracleParameter("userid", role.OwnerId)
                    };

                    RunProcedure(query, param, dset);

                    if (dset.Tables[0].Rows.Count == 0)
                    {
                        query = @"insert into user_role_link (role_id, userid) values
                                (:role_id, :userid)";

                        OracleParameter[] param2 = 
                        {
                            new OracleParameter("role_id", role.Id),        
                            new OracleParameter("userid", role.OwnerId)
                        };

                        ExecuteQuery(query, param2);
                    }
                    else
                    {
                        query = @"update user_role_link set is_active = 1 where role_id = :role_id and userid = :userid";

                        OracleParameter[] param2 = 
                        {
                            new OracleParameter("role_id", role.Id),        
                            new OracleParameter("userid", role.OwnerId)
                        };

                        ExecuteQuery(query, param2);
                    }
                }

                // Add audit
                new AuditData().AddAudit(new Audit
                {
                    EventType = (int)AuditEnum.AssignRole,
                    UserId = role.EditBy,
                    TimeStamp = DateTime.Now,
                    Description = "Added to " + role.OwnerId
                });
            }

            return 1;
        }

        public int RemoveRolesFromRole(List<Role> grps)
        {
            foreach (var grp in grps)
            {
                // Update the row instead
                var query = "update roles_link set is_active = 0 where role_id_1 = :role_id_1 and role_id_2 = :role_id_2";
                OracleParameter[] param = 
                {
                    new OracleParameter("role_id_1", grp.Id),        
                    new OracleParameter("role_id_2", grp.AddToRole)
                };

                ExecuteQuery(query, param);

                // Add audit
                new AuditData().AddAudit(new Audit
                {
                    EventType = (int)AuditEnum.UnassignGrpPrivilege,
                    UserId = grp.EditBy,
                    TimeStamp = DateTime.Now
                });
            }

            return 1;
        }

        public int RemoveRolesFromOperator(List<Role> roles)
        {
            foreach (var role in roles)
            {
                var query = @"update user_role_link set is_active = 0 where role_id = :role_id and userid = :userid";

                OracleParameter[] param = 
                {
                    new OracleParameter("role_id", role.Id),        
                    new OracleParameter("userid", role.OwnerId)
                };

                ExecuteQuery(query, param);

                // Add audit
                new AuditData().AddAudit(new Audit
                {
                    EventType = (int)AuditEnum.RemoveRole,
                    UserId = role.OwnerId,
                    TimeStamp = DateTime.Now
                });
            }

            return 1;
        }

        public int UpdateRole(Role role)
        {
            // First ensure the user has access to the role
            var query = "select created_by from roles where role_id = :role_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("role_id", role.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["created_by"].Equals(role.OwnerId))
                        return 2;
                }
                else
                    return 0;
            }

            // Ensure the name is also still unique
            query = "select role_name from roles where lower(role_name) = :role_name and app_id = :app_id";
            OracleParameter[] param2 = 
            {
                new OracleParameter("role_name", role.Name.ToLower()),
                new OracleParameter("app_id", role.ApplicationId)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param2, dset);

                if (dset.Tables[0].Rows.Count != 0)
                    return 3;
            }

            // Do update
            query = @"update roles set role_name = :role_name where role_id = :role_id";

            OracleParameter[] param = 
            {
                new OracleParameter("role_name", role.Name),        
                new OracleParameter("role_id", role.Id)
            };

            ExecuteQuery(query, param);

            // Add audit
            new AuditData().AddAudit(new Audit
            {
                EventType = (int)AuditEnum.EditRole,
                UserId = role.EditBy,
                TimeStamp = DateTime.Now
            });

            return 1;
        }

        public int DeleteRole(Role role)
        {
            // First ensure the user has access to the role
            var query = "select created_by from roles where role_id = :role_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("role_id", role.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["created_by"].Equals(role.OwnerId))
                        return 2;
                }
                else
                    return 0;
            }

            query = @"update roles set is_active = 0 where role_id = :role_id";

            OracleParameter[] param = 
            {
                new OracleParameter("role_id", role.Id)
            };

            ExecuteQuery(query, param);

            // Also remove all links to role
            query = @"update ROLES_PRIVI_LINK set is_active = 0 where role_id = :role_id";

            OracleParameter[] param2 = 
            {
                new OracleParameter("role_id", role.Id)
            };

            ExecuteQuery(query, param2);

            query = @"update ROLES_LINK set is_active = 0 where ROLE_ID_1 = :role_id OR ROLE_ID_2 = :role_id";

            OracleParameter[] param3 = 
            {
                new OracleParameter("role_id", role.Id)
            };

            ExecuteQuery(query, param3);

            // Add audit
            new AuditData().AddAudit(new Audit
            {
                EventType = (int)AuditEnum.DeleteRole,
                UserId = role.EditBy,
                TimeStamp = DateTime.Now
            });

            return 1;
        }

        public void GetRolePrivileges(Role role)
        {
            role.Privileges = new List<Privilege>();

            var query = @"select p.privi_id, p.privi_name, p.creation_date, p.created_by, p.app_id from roles_privi_link link 
                             left join privi p on link.privi_id = p.privi_id
                             where link.role_id = :role_id and link.is_active = 1 and p.is_active = 1";
            OracleParameter[] param = 
            { 
                new OracleParameter("role_id", role.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var privi = new Privilege()
                    {
                        IsActive = 1,
                        Id = int.Parse(row["privi_id"].ToString()),
                        Name = row["privi_name"].ToString(),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        ApplicationId = int.Parse(row["app_id"].ToString()),
                    };

                    // Get privilege actions
                    privi.Actions = new InternalActionData().GetPrivilegeActions(new Action { PrivilegeId = privi.Id }).Actions;

                    role.Privileges.Add(privi);
                }
            }
        }

        public List<Privilege> GetRolePrivileges(int roleId)
        {
            
            var result = new List<Privilege>();

            var query = @"select p.privi_id, p.privi_name, p.creation_date, p.created_by, p.app_id from roles_privi_link link 
                             left join privi p on link.privi_id = p.privi_id
                             where link.role_id = :role_id and link.is_active = 1 and p.is_active = 1";
            OracleParameter[] param = 
            { 
                new OracleParameter("role_id", roleId)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var privi = new Privilege()
                    {
                        IsActive = 1,
                        Id = int.Parse(row["privi_id"].ToString()),
                        Name = row["privi_name"].ToString(),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        ApplicationId = int.Parse(row["app_id"].ToString()),
                    };

                    // Get privilege actions
                    privi.Actions = new InternalActionData().GetPrivilegeActions(new Action { PrivilegeId = privi.Id }).Actions;

                    result.Add(privi);
                }
            }

            return result;
        }

        public void GetRoleGroups(Role role)
        {
            role.SubRoles = new List<Role>();

            var query = @"select r.role_id, role_name, created_by, creation_date, r.is_active, app_id, role_type 
                            from roles_link l left join roles r on r.role_id = l.ROLE_ID_1 where l.ROLE_ID_2 = :role_id and l.is_active = 1";
            OracleParameter[] param = 
            { 
                new OracleParameter("role_id", role.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var grp = new Role()
                    {
                        Id = int.Parse(row["role_id"].ToString()),
                        Name = row["role_name"].ToString(),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString())
                    };

                    if (!string.IsNullOrEmpty(row["app_id"].ToString())) grp.ApplicationId = int.Parse(row["app_id"].ToString());

                    role.SubRoles.Add(grp);
                }
            }
        }

        public List<Role> GetOperatorRoleDetails(Operator user)
        {
            var result = new List<Role>();
            
            var query = @"select r.role_id, r.role_name from user_role_link l left join roles r on r.role_id = l.role_id 
                            where l.is_active = 1 and l.userid = :userid";

            using (var dset = new DataSet())
            {
                OracleParameter[] param = { new OracleParameter("userid", user.OperatorId) };
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var role = new Role()
                    {
                        Id = int.Parse(row["role_id"].ToString()),
                        Name = row["role_name"].ToString(),
                        HideUnselected = true
                    };

                    // Get Role info
                    role = GetRoleDetails(role);

                    result.Add(role);
                }
            }

            return result;
        }
    }
}
