﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using UMA_DataManager.Misc;
using UMAentities;

namespace UMA_DataManager
{
    public class PrivilegeData : OracleDataProv
    {
        public List<Privilege> GetPrivileges()
        {
            var result = new List<Privilege>();

            var query = @"select privi_id, privi_name, creation_date, created_by, p.is_active, p.app_id, firstname, surname from privi p 
                            left join aspnetusers o on o.id = p.created_by where p.is_active = 1 order by privi_name";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var app = new Privilege
                    {
                        Id = int.Parse(row["privi_id"].ToString()),
                        Name = row["privi_name"].ToString(),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        ApplicationId = int.Parse(row["app_id"].ToString()),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper()
                    };

                    result.Add(app);
                }
            }

            return result;
        }

        public List<Privilege> GetApplicationPrivileges(int appid)
        {
            var result = new List<Privilege>();

            var query = @"select privi_id, privi_name, creation_date, created_by, p.is_active, p.app_id, firstname, surname from privi p 
                            left join aspnetusers o on o.id = p.created_by where p.app_id = :app_id and p.is_active = 1 order by privi_name";

            OracleParameter[] param = 
            {
                new OracleParameter("app_id", appid)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var app = new Privilege()
                    {
                        Id = int.Parse(row["privi_id"].ToString()),
                        Name = row["privi_name"].ToString(),
                        IsActive = int.Parse(row["is_active"].ToString()),
                        ApplicationId = int.Parse(row["app_id"].ToString()),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = (row["firstname"] + " " + row["surname"]).ToFirstLetterUpper()
                    };

                    result.Add(app);
                }
            }

            return result;
        }

        public int AddPrivilege(Privilege privilege)
        {
            // Check if privilege with same name already exists
            var query = @"select privi_id, is_active from privi where lower(privi_name) = :privi_name  and app_id = :app_id";

            OracleParameter[] param = 
            {
                new OracleParameter("privi_name", privilege.Name.ToLower()),
                new OracleParameter("app_id", privilege.ApplicationId)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                if (dset.Tables[0].Rows.Count == 0)
                {
                    // Carry on
                    query = @"insert into privi (privi_id, privi_name, created_by, app_id) values
                             (privi_seq.nextval, :privi_name, :created_by, :app_id)";

                    OracleParameter[] param2 = 
                    {
                        new OracleParameter("privi_name", privilege.Name),        
                        new OracleParameter("created_by", privilege.OwnerId),
                        new OracleParameter("app_id", privilege.ApplicationId)
                    };

                    ExecuteQuery(query, param2);

                    // Add audit
                    new AuditData().AddAudit(new Audit
                    {
                        EventType = (int)AuditEnum.CreatePrivilege,
                        UserId = privilege.OwnerId,
                        TimeStamp = DateTime.Now
                    });

                    return 1;
                }

                if (dset.Tables[0].Rows[0]["is_active"].ToString() == "1")
                    return 2;

                // Just set active to 1
                query = "update privi set is_active = 1 where privi_id = :privi_id";

                OracleParameter[] param3 = 
                {
                    new OracleParameter("privi_id", dset.Tables[0].Rows[0]["privi_id"].ToString())
                };

                ExecuteQuery(query, param3);

                // Add audit
                new AuditData().AddAudit(new Audit { EventType = (int)AuditEnum.CreatePrivilege, UserId = privilege.OwnerId, TimeStamp = DateTime.Now });

                return 1;
            }
        }

        public int AddPrivilegesToRole(List<Privilege> privileges)
        {
            foreach (var privilege in privileges)
            {
                // First check if it has already been inserted in which case it will be simply an update
                var query = @"select privi_id, role_id from roles_privi_link where privi_id = :privi_id and role_id = :role_id";

                OracleParameter[] paramx = 
                {
                    new OracleParameter("privi_id", privilege.Id),        
                    new OracleParameter("role_id", privilege.RoleId)
                };

                using (var dset = new DataSet())
                {
                    RunProcedure(query, paramx, dset);

                    if (dset.Tables[0].Rows.Count == 0)
                    {
                        // Insert the link
                        query = @"insert into roles_privi_link (privi_id, role_id) values
                             (:privi_id, :role_id)";

                        OracleParameter[] param = 
                        {
                            new OracleParameter("privi_id", privilege.Id),        
                            new OracleParameter("role_id", privilege.RoleId)
                        };

                        ExecuteQuery(query, param);
                    }
                    else
                    {
                        // Update the row instead
                        query = "update roles_privi_link set is_active = 1 where privi_id = :privi_id and role_id = :role_id";
                        OracleParameter[] param = 
                        {
                            new OracleParameter("privi_id", privilege.Id),        
                            new OracleParameter("role_id", privilege.RoleId)
                        };

                        ExecuteQuery(query, param);
                    }
                }

                // Add audit
                new AuditData().AddAudit(new Audit
                {
                    EventType = (int)AuditEnum.AssignPrivilege,
                    UserId = privilege.EditBy,
                    TimeStamp = DateTime.Now
                });
            }

            return 1;
        }

        public int AddPrivilegesToGroup(Role grp)
        {
            foreach (var privilege in grp.Privileges)
            {
                // First check if it has already been inserted in which case it will be simply an update
                var query = @"select privi_id, role_id from roles_privi_link where privi_id = :privi_id and role_id = :role_id";

                OracleParameter[] paramx = 
                {
                    new OracleParameter("privi_id", privilege.Id),        
                    new OracleParameter("role_id", grp.Id)
                };

                using (var dset = new DataSet())
                {
                    RunProcedure(query, paramx, dset);

                    if (dset.Tables[0].Rows.Count == 0)
                    {
                        // Insert the link
                        query = @"insert into roles_privi_link (role_id, privi_id) values
                             (:role_id, :privi_id)";

                        OracleParameter[] param = 
                        {
                            new OracleParameter("role_id", grp.Id),
                            new OracleParameter("privi_id", privilege.Id)        
                        };

                        ExecuteQuery(query, param);
                    }
                    else
                    {
                        // Update the row instead
                        query = "update roles_privi_link set is_active = 1 where privi_id = :privi_id and role_id = :role_id";
                        OracleParameter[] param = 
                        {
                            new OracleParameter("privi_id", privilege.Id),        
                            new OracleParameter("role_id", grp.Id)
                        };

                        ExecuteQuery(query, param);
                    }
                }

                // Add audit
                new AuditData().AddAudit(new Audit
                {
                    EventType = (int)AuditEnum.AssignGrpPrivilege,
                    UserId = grp.EditBy,
                    TimeStamp = DateTime.Now
                });
            }

            return 1;
        }

        public int RemovePrivilegesFromRole(List<Privilege> privileges)
        {
            foreach (var privilege in privileges)
            {
                var query = @"update roles_privi_link set is_active = 0 where privi_id = :privi_id and role_id = :role_id";

                OracleParameter[] param = 
                {
                    new OracleParameter("privi_id", privilege.Id),        
                    new OracleParameter("role_id", privilege.RoleId)
                };

                ExecuteQuery(query, param);

                // Add audit
                new AuditData().AddAudit(new Audit
                {
                    EventType = (int)AuditEnum.UnassignPrivilege,
                    UserId = privilege.EditBy,
                    TimeStamp = DateTime.Now
                });
            }

            return 1;
        }               

        public int RemovePrivilegesFromGroup(Role grp)
        {
            foreach (var privilege in grp.Privileges)
            {
                var query = @"update roles_privi_link set is_active = 0 where privi_id = :privi_id and role_id = :role_id";

                OracleParameter[] param = 
                {
                    new OracleParameter("privi_id", privilege.Id),        
                    new OracleParameter("role_id", grp.Id)
                };

                ExecuteQuery(query, param);

                // Add audit
                new AuditData().AddAudit(new Audit
                {
                    EventType = (int)AuditEnum.UnassignGrpPrivilege,
                    UserId = privilege.EditBy,
                    TimeStamp = DateTime.Now
                });
            }

            return 1;
        }

        public int UpdatePrivilege(Privilege privilege)
        {
            // First ensure the user has access to the privilege
            var query = "select created_by from privi where privi_id = :privi_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("privi_id", privilege.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["created_by"].Equals(privilege.OwnerId))
                        return 2;
                }
                else
                    return 0;
            }

            // Ensure the name is also still unique
            query = "select privi_name from privi where lower(privi_name) = :privi_name and app_id = :app_id";
            OracleParameter[] param2 = 
            {
                new OracleParameter("privi_name", privilege.Name.ToLower()),
                new OracleParameter("app_id", privilege.ApplicationId)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param2, dset);

                if (dset.Tables[0].Rows.Count != 0)
                    return 3;
            }

            // Do update
            query = @"update privi set privi_name = :privi_name where privi_id = :privi_id";

            OracleParameter[] param = 
            {
                new OracleParameter("privi_name", privilege.Name),        
                new OracleParameter("privi_id", privilege.Id)
            };

            ExecuteQuery(query, param);

            // Add audit
            new AuditData().AddAudit(new Audit
            {
                EventType = (int)AuditEnum.EditPrivilege,
                UserId = privilege.EditBy,
                TimeStamp = DateTime.Now
            });

            return 1;
        }

        public int DeletePrivilege(Privilege privilege)
        {
            // First ensure the user has access to the privilege
            var query = "select created_by from privi where privi_id = :privi_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("privi_id", privilege.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["created_by"].Equals(privilege.OwnerId))
                        return 2;
                }
                else
                    return 0;
            }

            query = @"update privi set is_active = 0 where privi_id = :privi_id";

            OracleParameter[] param = 
            {
                new OracleParameter("privi_id", privilege.Id)
            };

            ExecuteQuery(query, param);

            // Add audit
            new AuditData().AddAudit(new Audit
            {
                EventType = (int)AuditEnum.DeletePrivilege,
                UserId = privilege.EditBy,
                TimeStamp = DateTime.Now
            });

            return 1;
        }
    }
}