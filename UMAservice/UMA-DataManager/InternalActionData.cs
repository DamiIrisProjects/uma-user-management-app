﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using UMA_DataManager.Misc;
using UMAentities;
using Action = UMAentities.Action;

namespace UMA_DataManager
{
    public class InternalActionData : OracleDataProv
    {
        public int AddAction(Action action)
        {
            // Check if action with same name already exists
            var query = @"select internal_action_id, is_active from internal_action where lower(action) = :action and lower(controller) = :controller and app_id = :app_id";

            OracleParameter[] param = 
            {
                new OracleParameter("action", action.Name.ToLower()),
                new OracleParameter("controller", action.Controller.ToLower()),
                new OracleParameter("app_id", action.ApplicationId)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                if (dset.Tables[0].Rows.Count == 0)
                {
                    // Carry on
                    query = @"insert into internal_action (internal_action_id, action, controller, app_id, created_by) values
                                     (internal_action_seq.nextval, :action, :controller, :app_id, :created_by)";

                    OracleParameter[] param2 = 
                    {
                        new OracleParameter("action", action.Name),       
                        new OracleParameter("controller", action.Controller),        
                        new OracleParameter("app_id", action.ApplicationId),        
                        new OracleParameter("created_by", action.OwnerId)
                    };

                    ExecuteQuery(query, param2);

                    // Add audit
                    new AuditData().AddAudit(new Audit { EventType = (int)AuditEnum.CreateAction, UserId = action.OwnerId, TimeStamp = DateTime.Now });

                    return 1;
                }

                // Check if it is inactive and update otherwise return
                var row = dset.Tables[0].Rows[0];
                if (row["is_active"].ToString() == "0")
                {
                    query = @"update internal_action set is_active = 1, created_by = :created_by where internal_action_id = :internal_action_id";

                    OracleParameter[] param2 = 
                    {
                        new OracleParameter("created_by", action.OwnerId),        
                        new OracleParameter("internal_action_id", row["internal_action_id"].ToString())
                    };

                    ExecuteQuery(query, param2);

                    // Add audit
                    new AuditData().AddAudit(new Audit { EventType = (int)AuditEnum.CreateAction, UserId = action.OwnerId, TimeStamp = DateTime.Now });
                    return 1;
                }

                return 2;
            }
        }

        public int DeleteAction(Action action)
        {
            // First ensure the user has access to the action
            var query = "select created_by from internal_action where internal_action_id = :internal_action_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("created_by", action.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["created_by"].Equals(action.OwnerId))
                        return 2;
                }
                else
                    return 0;
            }

            query = @"update internal_action set is_active = 0 where internal_action_id = :internal_action_id";

            OracleParameter[] param = 
            {
                new OracleParameter("internal_action_id", action.Id)
            };

            ExecuteQuery(query, param);

            // Add audit
            new AuditData().AddAudit(new Audit { EventType = (int)AuditEnum.DeleteAction, UserId = action.EditBy, TimeStamp = DateTime.Now });

            return 1;
        }

        public int UpdateAction(Action action)
        {
            // First ensure the user has access to the application
            var query = "select created_by from internal_action where internal_action_id = :internal_action_id";
            OracleParameter[] param1 = 
            {
                new OracleParameter("created_by", action.Id)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param1, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    if (dset.Tables[0].Rows[0]["created_by"].Equals(action.OwnerId))
                        return 2;
                }
                else
                    return 0;
            }

            // Ensure the name is also still unique
            query = "select action from internal_action where lower(action) = :action and lower(controller) = :controller and app_id = :app_id";
            OracleParameter[] param2 = 
            {
                new OracleParameter("action", action.Name.ToLower()),
                new OracleParameter("controller", action.Controller.ToLower()),
                new OracleParameter("app_id", action.ApplicationId)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param2, dset);

                if (dset.Tables[0].Rows.Count != 0)
                    return 3;
            }

            // Do update
            query = @"update internal_action set action = :action, controller = :controller where internal_action_id = :internal_action_id";

            OracleParameter[] param = 
            {
                new OracleParameter("action", action.Name), 
                new OracleParameter("controller", action.Controller), 
                new OracleParameter("internal_action_id", action.Id)
            };

            ExecuteQuery(query, param);

            // Add audit
            new AuditData().AddAudit(new Audit
            {
                EventType = (int)AuditEnum.EditAction,
                UserId = action.EditBy,
                TimeStamp = DateTime.Now
            });

            return 1;
        }

        public Privilege GetPrivilegeActions(Action action)
        {

            var result = new Privilege
            {
                Actions = new List<Action>(),
                UnselectedActions = new List<Action>()
            };

            var query = @"select a.internal_action_id, app_id, action, controller, a.creation_date, a.created_by, u.firstname, u.surname 
                            from action_privi_link link
                            left join internal_action a on a.internal_action_id = link.internal_action_id
                            left join aspnetusers u on u.id = a.created_by
                            where link.privi_id = :privi_id and link.is_active = 1 and a.is_active = 1 order by controller";
            OracleParameter[] param = 
            { 
                new OracleParameter("privi_id", action.PrivilegeId)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var act = new Action
                    {
                        IsActive = 1,
                        ApplicationId = int.Parse(row["app_id"].ToString()),
                        Id = int.Parse(row["internal_action_id"].ToString()),
                        Controller = row["controller"].ToString(),
                        Name = row["action"].ToString(),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = row["firstname"].ToString().ToFirstLetterUpper() + " " + row["surname"].ToString().ToFirstLetterUpper()
                    };

                    result.Actions.Add(act);
                }
            }

            if (action.ApplicationId != 0)
            {
                // Populate unselected actions
                var allactions = GetApplicationActions(action.ApplicationId);
                result.UnselectedActions = allactions.Where(p => result.Actions.All(p2 => p2.Id != p.Id)).ToList();
            }

            result.Id = action.PrivilegeId;
            return result;
        }

        public List<Action> GetApplicationActions(int appid)
        {
            var result = new List<Action>();

            var query = @"select internal_action_id, app_id, action, controller, creation_date, created_by, firstname, surname from internal_action a
                             left join aspnetusers u on u.id = a.created_by
                             where app_id = :app_id and a.is_active = 1 order by controller";
            OracleParameter[] param = 
            { 
                new OracleParameter("app_id", appid)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var action = new Action()
                    {
                        IsActive = 1,
                        ApplicationId = int.Parse(row["app_id"].ToString()),
                        Id = int.Parse(row["internal_action_id"].ToString()),
                        Controller = row["controller"].ToString(),
                        Name = row["action"].ToString(),
                        DateCreated = DateTime.Parse(row["creation_date"].ToString()),
                        OwnerId = int.Parse(row["created_by"].ToString()),
                        OwnerName = row["firstname"].ToString().ToFirstLetterUpper() + " " + row["surname"].ToString().ToFirstLetterUpper()
                    };

                    result.Add(action);
                }
            }

            return result;
        }

        public int AddActionsToPrivilege(List<Action> actions)
        {
            foreach (var action in actions)
            {
                // First check if action has been added before
                var query = "select * from action_privi_link where privi_id = :privi_id and internal_action_id = :internal_action_id";

                using (var dset = new DataSet())
                {
                    OracleParameter[] param = 
                    {
                        new OracleParameter("privi_id", action.PrivilegeId),        
                        new OracleParameter("internal_action_id", action.Id)
                    };

                    RunProcedure(query, param, dset);

                    if (dset.Tables[0].Rows.Count == 0)
                    {
                        query = @"insert into action_privi_link (internal_action_id, privi_id) values
                                (:internal_action_id, :privi_id)";

                        OracleParameter[] param2 = 
                        {
                            new OracleParameter("internal_action_id", action.Id),        
                            new OracleParameter("privi_id", action.PrivilegeId)
                        };

                        ExecuteQuery(query, param2);
                    }
                    else
                    {
                        query = @"update action_privi_link set is_active = 1 where internal_action_id = :internal_action_id and privi_id = :privi_id";

                        OracleParameter[] param2 = 
                        {
                            new OracleParameter("internal_action_id", action.Id),        
                            new OracleParameter("privi_id", action.PrivilegeId)
                        };

                        ExecuteQuery(query, param2);
                    }
                }

                // Add audit
                new AuditData().AddAudit(new Audit { EventType = (int)AuditEnum.AssignAction, UserId = action.OwnerId, TimeStamp = DateTime.Now });
            }

            return 1;
        }

        public int RemoveActionsFromPrivilege(List<Action> actions)
        {
            foreach (var action in actions)
            {
                var query = @"update action_privi_link set is_active = 0 where internal_action_id = :internal_action_id and privi_id = :privi_id";

                OracleParameter[] param = 
                {
                    new OracleParameter("internal_action_id", action.Id),        
                    new OracleParameter("privi_id", action.PrivilegeId)
                };

                ExecuteQuery(query, param);

                // Add audit
                new AuditData().AddAudit(new Audit { EventType = (int)AuditEnum.UnAssignAction, UserId = action.OwnerId, TimeStamp = DateTime.Now });
            }

            return 1;
        }
    }
}
