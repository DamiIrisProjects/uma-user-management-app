﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using UMA_DataManager.Misc;
using UMAentities;

namespace UMA_DataManager
{
    public class AuditData : OracleDataProv
    {
        public void AddAudit(Audit audit)
        {
            var query = "insert into audits (audit_id, event_type, user_id, time_stamp, description) values (audit_seq.nextval, :event_type, :user_id, :time_stamp, :description)";
            OracleParameter[] param =
                    {   
                        new OracleParameter("event_type",  audit.EventType),
                        new OracleParameter("user_id",  audit.UserId == 0 ? null : audit.UserId),
                        new OracleParameter("time_stamp",  DateTime.Now),
                        new OracleParameter("description",  audit.Description)
                    };

            ExecuteQuery(query, param);
        }

        public List<Audit> GetAuditTrail(DateTime start, DateTime end)
        {
            var dset = new DataSet();
            var query = @"SELECT audit_id, event_type_name, event_type, userid, time_stamp, doc_id, description from audits a 
                            left join list_event_types l on l.event_type_id = a.event_type
                            where time_stamp > TO_DATE('" + start.AddDays(-1).ToString("yyyy-MM-dd") + @"', 'YYYY-MM-DD')
                            and time_stamp < TO_DATE('" + end.AddDays(1).AddDays(1).ToString("yyyy-MM-dd") + @"', 'YYYY-MM-DD')";

            
            RunProcedure(query, null, dset);

            var audits = new List<Audit>();

            foreach (DataRow row in dset.Tables[0].Rows)
            {
                var audit = new Audit
                {
                    EventType = int.Parse(row["event_type"].ToString()),
                    EventTypeName = row["event_type_name"].ToString(),
                    Description = row["description"].ToString(),
                    TimeStamp = DateTime.Parse(row["time_stamp"].ToString()),
                    User = string.IsNullOrEmpty(row["id"].ToString()) ? null : GetUserFromId(int.Parse(row["id"].ToString()))
                };

                // spaces for austerity sake
                audit.EventTypeName = string.Concat(audit.EventTypeName.Select(x => Char.IsUpper(x) ? " " + x : x.ToString())).TrimStart(' ');
                audits.Add(audit);
            }

            return audits;
        }

        private Person GetUserFromId(int userid)
        {
            var dset = new DataSet();
            Person user = null;

            var query = @"select id, firstname, surname, email, registration_date, userid, password_salt, password_hash, is_active, last_edit
                            from operator where userid = :userid";
            OracleParameter[] param =
                {   
                    new OracleParameter("userid",  userid)
                };

            RunProcedure(query, param, dset);

            if (dset.Tables[0].Rows.Count != 0)
            {
                var row = dset.Tables[0].Rows[0];
                user = new Person
                {
                    PersonId = int.Parse(row["id"].ToString()),
                    FirstName = row["firstname"].ToString(),
                    Surname = row["surname"].ToString(),
                    EmailAddress = row["email"].ToString()
                };

                if (!string.IsNullOrEmpty(row["registration_date"].ToString()))
                    user.DateOfRegistration = DateTime.Parse(row["registration_date"].ToString());
                if (!string.IsNullOrEmpty(row["last_edit"].ToString()))
                    user.LastActivityDate = DateTime.Parse(row["last_edit"].ToString());
                user.IsActive = row["is_active"].ToString() == "1";
                user.Salt = row["password_salt"].ToString();
                user.Hash = row["password_hash"].ToString();
            }

            return user;
        }
    }
}
