﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Script.Serialization;
using UMAdata;
using UMAentities;

namespace UMA_Contract
{
    public class UMAservice : UMAcontract
    {
        #region UMA Application

        #region Operator

        public int RegisterOperator(Operator opr)
        {
            try
            {
                return new DataManager().RegisterOperator(opr);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public Operator LoginOperator(Operator opr)
        {
            try
            {
                return new DataManager().LoginUser(opr);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public int UpdateOperator(Operator opr)
        {
            try
            {
                return new DataManager().UpdateOperator(opr);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Role> GetOperatorRoles(int oprid)
        {
            try
            {
                return new DataManager().GetOperatorRoles(oprid);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        #endregion

        #region Applications

        public int AddApplication(Application app)
        {
            try
            {
                return new DataManager().AddApplication(app);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int DeleteApplication(Application app)
        {
            try
            {
                return new DataManager().DeleteApplication(app);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int UpdateApplication(Application app)
        {
            try
            {
                return new DataManager().UpdateApplication(app);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public List<Operator> GetSharedAppUsers(Application app)
        {
            try
            {
                return new DataManager().GetSharedAppUsers(app);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Operator> GetUMAUsers(Application app)
        {
            try
            {
                return new DataManager().GetUMAUsers(app);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Operator> SaveSharedAppUsers(Application app)
        {
            try
            {
                return new DataManager().SaveSharedAppUsers(app);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Operator> RemoveShareApp(Application app)
        {
            try
            {
                return new DataManager().RemoveShareApp(app);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Operator> GetBranchAdmins(Application app)
        {
            try
            {
                return new DataManager().GetBranchAdmins(app);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Operator> GetBranchUsers(Application app)
        {
            try
            {
                return new DataManager().GetBranchUsers(app);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Branch> GetBranches()
        {
            try
            {
                return new DataManager().GetBranches();
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        #endregion

        #region Privileges

        public List<Privilege> GetPrivileges()
        {
            try
            {
                return new DataManager().GetPrivileges();
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<PrivilegeGroup> GetPrivilegeGroups()
        {
            try
            {
                return new DataManager().GetPrivilegeGroups();
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Privilege> GetApplicationPrivileges(int appid)
        {
            try
            {
                return new DataManager().GetApplicationPrivileges(appid);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public int AddPrivilege(Privilege privi)
        {
            try
            {
                return new DataManager().AddPrivilege(privi);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int AddPrivilegeGroup(PrivilegeGroup grp)
        {
            try
            {
                return new DataManager().AddPrivilegeGroup(grp);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int AddPrivilegesToGroup(PrivilegeGroup grp)
        {
            try
            {
                return new DataManager().AddPrivilegesToGroup(grp);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int DeletePrivilege(Privilege privi)
        {
            try
            {
                return new DataManager().DeletePrivilege(privi);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int DeletePrivilegeGroup(PrivilegeGroup grp)
        {
            try
            {
                return new DataManager().DeletePrivilegeGroup(grp);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int RemovePrivilegesFromGroup(PrivilegeGroup grp)
        {
            try
            {
                return new DataManager().RemovePrivilegesFromGroup(grp);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public PrivilegeGroup GetPrivilegeGroupDetails(int grpid)
        {
            try
            {
                return new DataManager().GetPrivilegeGroupDetails(grpid);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return null;
            }
        }

        public int UpdatePrivilege(Privilege privi)
        {
            try
            {
                return new DataManager().UpdatePrivilege(privi);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        #endregion

        #region Roles

        public List<Role> GetRoles(int branchid)
        {
            try
            {
                return new DataManager().GetRoles(branchid);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Role> GetApplicationRoles(int appid)
        {
            try
            {
                return new DataManager().GetApplicationRoles(appid);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public Role GetRoleDetails(Role role)
        {
            try
            {
                return new DataManager().GetRoleDetails(role);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public int AddRole(Role role)
        {
            try
            {
                return new DataManager().AddRole(role);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int AddRolesToOperator(List<Role> roles)
        {
            try
            {
                return new DataManager().AddRolesToOperator(roles);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int RemoveRolesFromOperator(List<Role> roles)
        {
            try
            {
                return new DataManager().RemoveRolesFromOperator(roles);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int AddPrivilegesToRole(List<Privilege> privileges)
        {
            try
            {
                return new DataManager().AddPrivilegesToRole(privileges);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int RemovePrivilegesFromRole(List<Privilege> privileges)
        {
            try
            {
                return new DataManager().RemovePrivilegesFromRole(privileges);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int DeleteRole(Role role)
        {
            try
            {
                return new DataManager().DeleteRole(role);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int UpdateRole(Role role)
        {
            try
            {
                return new DataManager().UpdateRole(role);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        #endregion

        #endregion

        #region REST service

        public List<Application> GetApplications()
        {
            try
            {
                return new DataManager().GetApplications();
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public Application GetApplicationById(string id)
        {
            try
            {
                int Id = 0;
                if (int.TryParse(id, out Id))
                    return new DataManager().GetApplication(int.Parse(id));
                else
                    return null;
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        private Person DecryptPassword(string password)
        {
            Person user = new Person();

            //temp
            user.Person_Id = 1;

            return user;
        }

        // Json
        public string GetApplicationsJson()
        {
            try
            {
                return new JavaScriptSerializer().Serialize(new DataManager().GetApplications());
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public string OperatorLogin(string username, string password)
        {
            try
            {
                Operator op = new Operator()
                {
                    EmailAddress = username,
                    Password = password
                };

                Operator result = new DataManager().LoginUser(op);
                RestfulResponse response = new RestfulResponse();
                if (result != null)
                {
                    response.Token = Helper.ToFirstLetterUpper(result.FirstName) + "-Token";
                    response.Roles = result.FirstName.ToLower() == "brian" ? new List<string>()
                    {
                       "Test Role", "Another test"
                    } : new List<string>()
                    {
                       "Biometrics officer"
                    };
                }
                else
                {
                    response.Error = "Incorrect username or password";
                }

                return new JavaScriptSerializer().Serialize(response);
            }
            catch (Exception ex)
            {
                new DataManager().SaveError(Helper.CreateError(ex));
                return new JavaScriptSerializer().Serialize(new RestfulResponse() { Error = "Invalid request" });
            }
        }

        #endregion
    }
}
