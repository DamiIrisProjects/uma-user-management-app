﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using UMA_Contract;

namespace UMA_Proxy
{
    public class ApplicationProxy
    {
        public IApplication ApplicationChannel { get; set; }

        public string ServiceBaseAddress { get; set; }

        public ApplicationProxy()
        {
            string serviceUrl = ConfigurationManager.AppSettings["ServiceUrl"];
            string serviceName = "ApplicationService.svc";

            if (serviceUrl.EndsWith("/"))
            {
                ServiceBaseAddress = serviceUrl + serviceName;
            }
            else
            {
                ServiceBaseAddress = serviceUrl + "/" + serviceName;
            }

            var binding = new WSHttpBinding
            {
                MaxReceivedMessageSize = 4194304,
                Security =
                {
                    Mode = SecurityMode.Transport,
                    Transport = {ClientCredentialType = HttpClientCredentialType.None}
                },
                SendTimeout = new TimeSpan(0, 10, 0),
                ReceiveTimeout = new TimeSpan(0, 10, 0),
                ReaderQuotas = {MaxArrayLength = 4194304}
            };
            //4 mb


            //Always accept as this is on the local network and would need to run on a self signed certificate
            //if (Common.Utilities.GeneralUtilities.IsDeveloper)
            //{
            ServicePointManager.ServerCertificateValidationCallback = delegate
            {
                return true;
            };
            //}

            var cf = new ChannelFactory<IApplication>(binding, ServiceBaseAddress);

            ApplicationChannel = cf.CreateChannel();
        }
    }
}
