﻿using System;
using System.Collections.Generic;
using UMA_DataManager;
using UMA_DataManager.Misc;
using UMAentities;

namespace UMA_Contract
{
    public class PrivilegeService : IPrivilege
    {
        public List<Privilege> GetPrivileges()
        {
            try
            {
                return new PrivilegeData().GetPrivileges();
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Privilege> GetApplicationPrivileges(int appid)
        {
            try
            {
                return new PrivilegeData().GetApplicationPrivileges(appid);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public int AddPrivilege(Privilege privi)
        {
            try
            {
                return new PrivilegeData().AddPrivilege(privi);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int AddPrivilegesToRole(List<Privilege> privileges)
        {
            try
            {
                return new PrivilegeData().AddPrivilegesToRole(privileges);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int RemovePrivilegesFromRole(List<Privilege> privileges)
        {
            try
            {
                return new PrivilegeData().RemovePrivilegesFromRole(privileges);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int AddPrivilegesToGroup(Role grp)
        {
            try
            {
                return new PrivilegeData().AddPrivilegesToGroup(grp);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int DeletePrivilege(Privilege privi)
        {
            try
            {
                return new PrivilegeData().DeletePrivilege(privi);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int RemovePrivilegesFromGroup(Role grp)
        {
            try
            {
                return new PrivilegeData().RemovePrivilegesFromGroup(grp);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int UpdatePrivilege(Privilege privi)
        {
            try
            {
                return new PrivilegeData().UpdatePrivilege(privi);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }
    }
}
