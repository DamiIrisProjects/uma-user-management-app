﻿using System;
using System.Collections.Generic;
using UMA_DataManager;
using UMA_DataManager.Misc;
using UMAentities;
using Action = UMAentities.Action;

namespace UMA_Contract
{
    public class InternalActionService : IInternalAction
    {
        public int AddAction(Action action)
        {
            try
            {
                return new InternalActionData().AddAction(action);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public int DeleteAction(Action action)
        {
            try
            {
                return new InternalActionData().DeleteAction(action);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public int UpdateAction(Action action)
        {
            try
            {
                return new InternalActionData().UpdateAction(action);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public Privilege GetPrivilegeActions(Action action)
        {
            try
            {
                return new InternalActionData().GetPrivilegeActions(action);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Action> GetApplicationActions(int appid)
        {
            try
            {
                return new InternalActionData().GetApplicationActions(appid);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public int AddActionsToPrivilege(List<Action> actions)
        {
            try
            {
                return new InternalActionData().AddActionsToPrivilege(actions);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public int RemoveActionsFromPrivilege(List<Action> Actions)
        {
            try
            {
                return new InternalActionData().RemoveActionsFromPrivilege(Actions);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }
    }
}
