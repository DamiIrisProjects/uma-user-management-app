﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Script.Serialization;
using UMA_DataManager;
using UMA_DataManager.Misc;
using UMAentities;
using Action = UMAentities.Action;

namespace UMA_Contract
{
    public class OperatorService : IOperator
    {
        public int RegisterOperator(Operator opr)
        {
            try
            {
                return new OperatorData().RegisterOperator(opr);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public Operator LoginOperator(Operator opr)
        {
            try
            {
                return new OperatorData().LoginUser(opr);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public Operator GetOperatorDetails(int oprid)
        {
            try
            {
                return new OperatorData().GetOperatorDetails(oprid);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Operator> GetAllUsers()
        {
            try
            {
                return new OperatorData().GetAllUsers();
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public int UpdateOperator(Operator opr)
        {
            try
            {
                return new OperatorData().UpdateOperator(opr);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Role> GetOperatorRoles(Operator opr)
        {
            try
            {
                return new OperatorData().GetOperatorRoles(opr);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public int DeactivateUser(Operator opr)
        {
            try
            {
                return new OperatorData().DeactivateUser(opr);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public string OperatorLogin(string username, string password)
        {
            try
            {
                Operator op = new Operator()
                {
                    EmailAddress = username,
                    Password = password
                };

                Operator result = new OperatorData().LoginUser(op);
                ApiResult response = new ApiResult();
                if (result != null)
                {
                    response.Id = result.OperatorId;
                    response.Firstname = result.FirstName.ToFirstLetterUpper();
                    response.Surname = result.Surname.ToFirstLetterUpper();
                    response.Roles = new List<JsonRole>();
                    response.Privileges = new List<JsonPrivilege>();
                    response.Actions = new List<JsonAction>();

                    foreach (Role role in result.Roles)
                    {
                        var jrole = new JsonRole
                        {
                            Id = role.Id,
                            Name = role.Name
                        };

                        if (role.Privileges != null)
                        {
                            foreach (Privilege privi in role.Privileges)
                            {
                                var jprivi = new JsonPrivilege
                                {
                                    Id = privi.Id,
                                    Name = privi.Name
                                };

                                if (privi.Actions != null)
                                {
                                    foreach (Action action in privi.Actions)
                                    {
                                        var jaction = new JsonAction
                                        {
                                            Controller = action.Controller,
                                            Action = action.Name
                                        };

                                        response.Actions.Add(jaction);
                                    }
                                }

                                response.Privileges.Add(jprivi);
                            }
                        }

                        response.Roles.Add(jrole);
                    }
                }
                else
                {
                    response.Error = "Incorrect username or password";
                }

                var serializer = new JavaScriptSerializer();
                serializer.RegisterConverters(new JavaScriptConverter[] { new NullPropertiesConverter() });
                return serializer.Serialize(response);

                //return new JavaScriptSerializer().Serialize(response);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return new JavaScriptSerializer().Serialize(new ApiResult() { Error = "Invalid request" });
            }
        }

        private class NullPropertiesConverter : JavaScriptConverter
        {
            public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
            {
                throw new NotImplementedException();
            }

            public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
            {
                var jsonExample = new Dictionary<string, object>();
                foreach (var prop in obj.GetType().GetProperties())
                {
                    //check if decorated with ScriptIgnore attribute
                    bool ignoreProp = prop.IsDefined(typeof(ScriptIgnoreAttribute), true);

                    var value = prop.GetValue(obj, BindingFlags.Public, null, null, null);
                    if (value != null && !ignoreProp)
                        jsonExample.Add(prop.Name, value);
                }

                return jsonExample;
            }

            public override IEnumerable<Type> SupportedTypes => GetType().Assembly.GetTypes();
        }
    }
}
