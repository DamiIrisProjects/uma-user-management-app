﻿using System;
using System.Collections.Generic;
using UMA_DataManager;
using UMA_DataManager.Misc;
using UMAentities;

namespace UMA_Contract
{
    public class RoleService : IRole
    {
        public List<Role> GetRoles()
        {
            try
            {
                return new RoleData().GetRoles();
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Role> GetRolesByType(int id)
        {
            try
            {
                return new RoleData().GetRolesByType(id);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<RoleType> GetRoleTypes()
        {
            try
            {
                return new RoleData().GetRoleTypes();
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Role> GetApplicationRoles(Application app)
        {
            try
            {
                return new RoleData().GetApplicationRoles(app);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Role> GetSystemRoles()
        {
            try
            {
                return new RoleData().GetSystemRoles();
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public Role GetRoleDetails(Role role)
        {
            try
            {
                return new RoleData().GetRoleDetails(role);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public Role GetRoleDetailsById(int roleid)
        {
            try
            {
                return new RoleData().GetRoleDetailsById(roleid);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Privilege> GetRolePrivileges(int id)
        {
            try
            {
                return new RoleData().GetRolePrivileges(id);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public int AddRole(Role role)
        {
            try
            {
                return new RoleData().AddRole(role);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int AddRolesToRole(List<Role> grps)
        {
            try
            {
                return new RoleData().AddRolesToRole(grps);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int AddRolesToOperator(List<Role> roles)
        {
            try
            {
                return new RoleData().AddRolesToOperator(roles);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int RemoveRolesFromOperator(List<Role> roles)
        {
            try
            {
                return new RoleData().RemoveRolesFromOperator(roles);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int AddPrivilegesToRole(List<Privilege> privileges)
        {
            try
            {
                return new PrivilegeData().AddPrivilegesToRole(privileges);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int RemovePrivilegesFromRole(List<Privilege> privileges)
        {
            try
            {
                return new PrivilegeData().RemovePrivilegesFromRole(privileges);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int RemoveRolesFromRole(List<Role> grps)
        {
            try
            {
                return new RoleData().RemoveRolesFromRole(grps);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int DeleteRole(Role role)
        {
            try
            {
                return new RoleData().DeleteRole(role);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int UpdateRole(Role role)
        {
            try
            {
                return new RoleData().UpdateRole(role);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }
    }
}
