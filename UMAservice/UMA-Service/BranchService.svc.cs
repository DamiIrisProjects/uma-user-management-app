﻿using System;
using System.Collections.Generic;
using UMAentities;
using UMA_DataManager;
using UMA_DataManager.Misc;

namespace UMA_Contract
{
    public class BranchService : IBranch
    {
        public List<Branch> GetBranches()
        {
            try
            {
                return new BranchData().GetBranches();
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public string GetBranchName(int branchid)
        {
            try
            {
                return new BranchData().GetBranchName(branchid);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }
    }
}
