﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using UMA_DataManager;
using UMAentities;
using UMA_DataManager.Misc;

namespace UMA_Contract
{
    public class ApplicationService : IApplication
    {
        public string GetApplicationsJson()
        {
            try
            {
                return new JavaScriptSerializer().Serialize(new ApplicationData().GetApplications());
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Application> GetApplications()
        {
            try
            {
                return new ApplicationData().GetApplications();
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public Application GetApplicationById(string id)
        {
            try
            {
                int outId;
                if (int.TryParse(id, out outId))
                    return new ApplicationData().GetApplication(int.Parse(id));
                else
                    return null;
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public Application GetApplication(int appid)
        {
            try
            {
                return new ApplicationData().GetApplication(appid);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return null;
            }
        }

        public Application GetApplicationByName(string id)
        {
            try
            {
                return new ApplicationData().GetApplicationByName(id);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return null;
            }
        }

        public int AddApplication(Application app)
        {
            try
            {
                return new ApplicationData().AddApplication(app);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int DeleteApplication(Application app)
        {
            try
            {
                return new ApplicationData().DeleteApplication(app);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public int UpdateApplication(Application app)
        {
            try
            {
                return new ApplicationData().UpdateApplication(app);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                return -1;
            }
        }

        public List<Operator> GetSharedAppUsers(Application app)
        {
            try
            {
                return new ApplicationData().GetSharedAppUsers(app);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Operator> GetUmaUsers(Application app)
        {
            try
            {
                return new ApplicationData().GetUmaUsers(app);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Operator> SaveSharedAppUsers(Application app)
        {
            try
            {
                return new ApplicationData().SaveSharedAppUsers(app);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Operator> RemoveShareApp(Application app)
        {
            try
            {
                return new ApplicationData().RemoveShareApp(app);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Operator> GetBranchAdmins(Application app)
        {
            try
            {
                return new ApplicationData().GetBranchAdmins(app);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Operator> GetSuperUsers()
        {
            try
            {
                return new ApplicationData().GetSuperUsers();
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Operator> GetBranchUsers(Application app)
        {
            try
            {
                return new ApplicationData().GetBranchUsers(app);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public List<Branch> GetBranches()
        {
            try
            {
                return new BranchData().GetBranches();
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }

        public string GetBranchName(int branchid)
        {
            try
            {
                return new BranchData().GetBranchName(branchid);
            }
            catch (Exception ex)
            {
                MiscData.SaveError(Helper.CreateError(ex));
                throw;
            }
        }
    }
}
