﻿using System.Collections.Generic;

namespace UMAentities
{
    public class ApiResult
    {
        public string Error { get; set; }

        public int Id { get; set; }

        public string Firstname { get; set; }

        public string Surname { get; set; }

        public List<JsonRole> Roles { get; set; }

        public List<JsonPrivilege> Privileges { get; set; }

        public List<JsonAction> Actions { get; set; }
    }

    public class JsonRole
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class JsonPrivilege
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class JsonAction
    {
        public string Controller { get; set; }

        public string Action { get; set; }
    }
}
