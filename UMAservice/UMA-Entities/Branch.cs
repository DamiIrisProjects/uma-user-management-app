﻿using System.Runtime.Serialization;

namespace UMAentities
{
    [DataContract]
    public class Branch
    {
        [DataMember]
        public int BranchId { get; set; }

        [DataMember]
        public string BranchName { get; set; }
    }
}
