﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace UMAentities
{
    [DataContract]
    public class Role
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int OwnerId { get; set; }

        [DataMember]
        public int AddToRole { get; set; }

        [DataMember]
        public int RoleTypeId { get; set; }

        [DataMember]
        public RoleType RoleType { get; set; }

        [DataMember]
        public bool HideUnselected { get; set; }
        
        [DataMember]
        public int EditBy { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string OwnerName { get; set; }

        [DataMember]
        public DateTime DateCreated { get; set; }

        [DataMember]
        public int ApplicationId { get; set; }

        [DataMember]
        public int IsActive { get; set; }

        [DataMember]
        public List<Privilege> Privileges { get; set; }

        [DataMember]
        public List<Role> SubRoles { get; set; }

        [DataMember]
        public List<PrivilegeGroup> PrivilegeGrps { get; set; }

        [DataMember]
        public List<Privilege> UnselectedPrivileges { get; set; }

        [DataMember]
        public List<Role> UnselectedRoles { get; set; }

        [DataMember]
        public List<PrivilegeGroup> UnselectedPrivilegeGroups { get; set; }

        // Other properties used for jquery and such
        public string AddPrivileges { get; set; }

        public string AddPrivilegeGroups { get; set; }

        public string RemovePrivileges { get; set; }

        public string RemovePrivilegeGroups { get; set; }

        [DataMember]
        public string AddRoles { get; set; }

        [DataMember]
        public string RemoveRoles { get; set; }
    }
}
