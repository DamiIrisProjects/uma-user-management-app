﻿using System;
using System.Runtime.Serialization;

namespace UMAentities
{
    [DataContract]
    public class Action
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int OwnerId { get; set; }

        [DataMember]
        public int EditBy { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Controller { get; set; }

        [DataMember]
        public string OwnerName { get; set; }

        [DataMember]
        public DateTime DateCreated { get; set; }

        [DataMember]
        public int PrivilegeId { get; set; }

        [DataMember]
        public int ApplicationId { get; set; }

        [DataMember]
        public int IsActive { get; set; }

        // Other properties used for jquery and such
        public string AddActions { get; set; }

        public string RemoveActions { get; set; }
    }
}
