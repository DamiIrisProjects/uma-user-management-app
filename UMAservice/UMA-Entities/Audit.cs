﻿using System;
using System.Runtime.Serialization;

namespace UMAentities
{
    [DataContract]
    public class Audit
    {
        [DataMember]
        public int EventType { get; set; }

        [DataMember]
        public string EventTypeName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? UserId { get; set; }

        [DataMember]
        public DateTime TimeStamp { get; set; }

        [DataMember]
        public int? DocId { get; set; }

        [DataMember]
        public Person User { get; set; }
    }

    [DataContract]
    public enum AuditEnum
    {
        LoginSuccess = 1,
        LogOut = 2,
        CreateApplication = 3,
        EditApplication = 4,
        CreateRole = 5,
        EditRole = 6,
        DeleteRole = 7,
        CreatePrivilege = 8,
        EditPrivilege = 9,
        DeletePrivilege = 10,
        LoginFailed = 11,
        EditLoginDetails = 12,
        RegistrationEmailSent = 13,
        RegistrationActivated = 14,
        ForgotPassword = 15,
        AssignPrivilege = 16,
        UnassignPrivilege = 17,
        AssignRole = 18,
        RemoveRole = 19,
        CreateGrpPrivilege = 20,
        EditGrpPrivilege = 21,
        DeleteGrpPrivilege = 22,
        AssignGrpPrivilege = 23,
        UnassignGrpPrivilege = 24,
        CreateAction = 25,
        EditAction = 26,
        DeleteAction = 27,
        AssignAction = 28,
        UnAssignAction = 29,
        DeactivateUser = 30
    }
}
