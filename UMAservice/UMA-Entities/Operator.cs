﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace UMAentities
{
    [DataContract]
    public class Operator
    {
        [DataMember]
        public int OperatorId { get; set; }
                
        [DataMember]
        public int OperatorType { get; set; }

        [DataMember]
        public int OwnerId { get; set; }

        [DataMember]
        public int EditBy { get; set; }

        [DataMember]
        public bool IsActivated { get; set; }

        [DataMember]
        public int IsCancel { get; set; }

        [DataMember]
        public int FirstTimeLog { get; set; }

        [DataMember]
        public DateTime LastActivityDate { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public List<Role> UnselectedRoles { get; set; }

        [DataMember]
        public List<Role> SelectedRoles { get; set; }

        [DataMember]
        public string AppId { get; set; }

        [DataMember]
        public string Hash { get; set; }

        [DataMember]
        public string Salt { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public bool IsDisabled { get; set; }

        [DataMember]
        public bool IsSelected { get; set; }

        [DataMember]
        public string BranchName { get; set; }

        [DataMember]
        public int BranchId { get; set; }

        [DataMember]
        public string NewPassword { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string PasswordConfirm { get; set; }

        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public DateTime DateOfRegistration { get; set; }

        [DataMember]
        public string OperatorTypeName { get; set; }

        [DataMember]
        public string RolesJson { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string Surname { get; set; }

        public string FullName => $"{UppercaseFirst(FirstName)} {UppercaseFirst(Surname)}".Trim();

        public string FirstNameUpper => UppercaseFirst(FirstName);

        public string SurnameUpper => UppercaseFirst(Surname);

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public List<Role> Roles { get; set; }

        #region Operations

        public string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToLower().ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        #endregion
    }
}
