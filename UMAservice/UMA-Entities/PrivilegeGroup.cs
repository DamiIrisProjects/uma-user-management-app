﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace UMAentities
{
    [DataContract]
    public class PrivilegeGroup
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int AppId { get; set; }

        [DataMember]
        public int RoleId { get; set; }

        [DataMember]
        public int OwnerId { get; set; }

        [DataMember]
        public DateTime DateCreated { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string OwnerName { get; set; }

        [DataMember]
        public string AddPrivileges { get; set; }

        [DataMember]
        public string RemovePrivileges { get; set; }

        [DataMember]
        public List<Privilege> UnselectedPrivileges { get; set; }

        [DataMember]
        public List<Privilege> Privileges { get; set; }

        [DataMember]
        public int EditBy { get; set; }

    }
}
