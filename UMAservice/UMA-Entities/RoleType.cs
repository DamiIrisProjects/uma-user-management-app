﻿using System.Runtime.Serialization;

namespace UMAentities
{
    [DataContract]
    public class RoleType
    {
        [DataMember]
        public int RoleTypeId { get; set; }

        [DataMember]
        public int IsApplicationLinked { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
