﻿namespace UMAentities
{
    public class ApiVerification
    {
        public bool IsExpired { get; set; }

        public Operator Operator { get; set; }
    }
}
