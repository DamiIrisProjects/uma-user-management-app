﻿using System;
using System.Runtime.Serialization;

namespace UMAentities
{
    [DataContract]
    public class Person
    {
        [DataMember]
        public int PersonId { get; set; }

        [DataMember]
        public int GroupId { get; set; }

        [DataMember]
        public DateTime LastActivityDate { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Hash { get; set; }

        [DataMember]
        public string Salt { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public bool IsDisabled { get; set; }

        [DataMember]
        public bool IsSelected { get; set; }

        [DataMember]
        public string NewPassword { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string PasswordConfirm { get; set; }

        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public DateTime DateOfRegistration { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string Surname { get; set; }

        public string FullName => $"{UppercaseFirst(FirstName)} {UppercaseFirst(Surname)}".Trim();

        public string FirstNameUpper => UppercaseFirst(FirstName);

        public string SurnameUpper => UppercaseFirst(Surname);

        [DataMember]
        public string EmailAddress { get; set; }

        #region Operations

        public string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToLower().ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        #endregion
    }
}
