﻿using System;

namespace UMAentities
{
    public static class Helper
    {
        public static Error CreateError(Exception ex)
        {
            var error = new Error
            {
                Message = ex.Message,
                StackTrace = ex.StackTrace,
                TargetSite = ex.TargetSite.Name,
                Timestamp = DateTime.Now
            };

            if (ex.InnerException != null)
            {
                error.InnerExMsg = ex.InnerException.Message;
                error.InnerExSrc = ex.InnerException.Source;
                error.InnerExStkTrace = ex.InnerException.StackTrace;
                error.InnerExTargetSite = ex.InnerException.TargetSite.Name;
            }

            return error;
        }

        public static string ToFirstLetterUpper(this string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var words = input.ToLower().Split(' ');
                string result = string.Empty;

                foreach (string word in words)
                {
                    if (word != string.Empty && word != " ")
                    {
                        char[] a = word.ToCharArray();
                        a[0] = char.ToUpper(a[0]);

                        if (result == string.Empty)
                            result = result + new string(a);
                        else
                            result = result + " " + new string(a);
                    }
                }

                return result;
            }

            return string.Empty;
        }
    }
}
