﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace UMAentities
{
    [DataContract]
    public class Privilege : IEquatable<Privilege>
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int ApplicationId { get; set; }

        [DataMember]
        public int OwnerId { get; set; }

        [DataMember]
        public int EditBy { get; set; }

        [DataMember]
        public int RoleId { get; set; }

        [DataMember]
        public int GroupId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DateTime DateCreated { get; set; }

        [DataMember]
        public string OwnerName { get; set; }

        [DataMember]
        public List<Action> Actions { get; set; }

        [DataMember]
        public List<Action> UnselectedActions { get; set; }

        [DataMember]
        public int IsActive { get; set; }

        public bool Equals(Privilege other)
        {
            return other != null && Id == other.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
