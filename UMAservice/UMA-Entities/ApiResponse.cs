﻿namespace UMAentities
{
    public class ApiResponse
    {
        public string Error { get; set; }

        public string ResponseVal { get; set; }

        public string Message { get; set; }
    }
}
