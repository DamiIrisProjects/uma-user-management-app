﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace UMAentities
{
    [DataContract]
    public class Application
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Icon { get; set; }

        [DataMember]
        public string Port { get; set; }

        [DataMember]
        public bool HasAccess { get; set; }

        [DataMember]
        public DateTime DateCreated { get; set; }

        [DataMember]
        public int OwnerId { get; set; }

        [DataMember]
        public int EditBy { get; set; }

        [DataMember]
        public int BranchId { get; set; }

        [DataMember]
        public string OwnerName { get; set; }

        [DataMember]
        public List<string> SharedWith { get; set; }

        [DataMember]
        public string SelectedUsers { get; set; }

        [DataMember]
        public int IsActive { get; set; }

        [DataMember]
        public int RoleType { get; set; }

        [DataMember]
        public bool ExcludeLesserRoles { get; set; }
    }


}
